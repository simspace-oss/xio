import { not } from "fp-ts/Predicate"

export interface Interruptible {
  readonly _tag: "Interruptible"
}

export interface Uninterruptible {
  readonly _tag: "Uninterruptible"
}

export type InterruptStatus = Interruptible | Uninterruptible

export const Interruptible: InterruptStatus = {
  _tag: "Interruptible",
}

export const Uninterruptible: InterruptStatus = {
  _tag: "Uninterruptible",
}

export const isInterruptible = (self: InterruptStatus): boolean => {
  switch (self._tag) {
    case "Interruptible":
      return true
    case "Uninterruptible":
      return false
  }
}

export const isUninterruptible: (self: InterruptStatus) => boolean = not(isInterruptible)

/**
 * `true`  -> `Interruptible`
 *
 * `false` -> `Uninterruptible`
 */
export const fromBoolean = (b: boolean): InterruptStatus => (b ? Interruptible : Uninterruptible)
