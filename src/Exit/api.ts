import type { Cause } from "../Cause"
import type { FiberId } from "../FiberId"
import type { XIO } from "../XIO"
import type { Exit } from "./definition"

import * as V from "@simspace/collections/Vector"
import * as E from "fp-ts/Either"
import { flow, identity, pipe } from "fp-ts/function"
import * as O from "fp-ts/Option"

import * as C from "../Cause"
import { Failure, Success } from "./definition"

/**
 * Constructs a new `Exit` that represents a success with the specified value
 */
export const succeed = <E = never, A = never>(value: A): Exit<E, A> => new Success(value)

/**
 * Constructs a new `Exit` that represents a failure with the specified `Cause`
 */
export const failCause = <E = never, A = never>(cause: C.Cause<E>): Exit<E, A> => new Failure(cause)

/**
 * Constructs a new `Exit` that represents a failure with the specified value
 */
export const fail = <E = never, A = never>(value: E): Exit<E, A> => failCause(C.fail(value))

/**
 * Constructs a new `Exit` that represents an unchecked failure with the specified defect
 */
export const halt = <E = never, A = never>(defect: unknown): Exit<E, A> => failCause(C.halt(defect))

/**
 * Constructs a new `Exit` that represents an interruption by the specified `FiberId`
 */
export const interrupt = <E = never, A = never>(fiberId: FiberId): Exit<E, A> =>
  failCause(C.interrupt(fiberId))

export const isFailure = <E, A>(exit: Exit<E, A>): exit is Failure<E> => exit._tag === "Failure"

export const isSuccess = <E, A>(exit: Exit<E, A>): exit is Success<A> => exit._tag === "Success"

/**
 * Constructs a new `Exit` from an `Either`, failing with the `Left` value,
 * or succeeding with the `Right` value
 */
export const fromEither: <E, A>(either: E.Either<E, A>) => Exit<E, A> = E.matchW(fail, succeed)

export const match =
  <E, A, B, C>(onFailure: (e: C.Cause<E>) => B, onSuccess: (a: A) => C) =>
  (exit: Exit<E, A>): B | C => {
    switch (exit._tag) {
      case "Failure":
        return onFailure(exit.cause)
      case "Success":
        return onSuccess(exit.value)
    }
  }

export const matchXIO: <E, A, R1, E1, B, R2, E2, C>(
  onFailure: (e: C.Cause<E>) => XIO<R1, E2, B>,
  onSuccess: (a: A) => XIO<R2, E2, C>,
) => (exit: Exit<E, A>) => XIO<R1 & R2, E1 | E2, B | C> = match

/**
 * Transforms the success value contained in an `Exit`
 */
export const map = <A, B>(f: (a: A) => B): (<E>(exit: Exit<E, A>) => Exit<E, B>) =>
  match(failCause, flow(f, succeed))

/**
 * Transforms checked failures contained in an `Exit`
 */
export const mapError = <E, E1>(f: (e: E) => E1): (<A>(exit: Exit<E, A>) => Exit<E1, A>) =>
  match(flow(C.map(f), failCause), succeed)

/**
 * Transforms the success and checked failures contained in an `Exit`
 */
export const bimap = <E, A, E1, B>(
  f: (e: E) => E1,
  g: (a: A) => B,
): ((exit: Exit<E, A>) => Exit<E1, B>) => match(flow(C.map(f), failCause), flow(g, succeed))

/**
 * Transforms the `Cause` contained in an `Exit`
 */
export const mapErrorCause = <E, E1>(
  f: (cause: C.Cause<E>) => C.Cause<E1>,
): (<A>(exit: Exit<E, A>) => Exit<E1, A>) => match(flow(f, failCause), succeed)

/**
 * Transforms the success of an `Exit` into a new `Exit` with function `f`,
 * flattening the resuling `Exit`
 */
export const chain =
  <A, E1, B>(f: (a: A) => Exit<E1, B>) =>
  <E>(exit: Exit<E, A>): Exit<E | E1, B> =>
    isSuccess(exit) ? f(exit.value) : exit

/**
 * Flattens an Exit of an Exit into a single Exit value.
 */
export const flatten: <E, E1, A>(mma: Exit<E, Exit<E1, A>>) => Exit<E | E1, A> = chain(identity)

/**
 * Combines two `Exits` together, using function `f` to combine successes,
 * and function `g` to combine failures
 */
export const zipWithCause =
  <E, A, E1, B, C>(
    fb: Exit<E1, B>,
    f: (a: A, b: B) => C,
    g: (ea: Cause<E>, eb: Cause<E1>) => Cause<E | E1>,
  ) =>
  (fa: Exit<E, A>): Exit<E | E1, C> => {
    switch (fa._tag) {
      case "Failure":
        switch (fb._tag) {
          case "Success":
            return fa
          case "Failure":
            return failCause(g(fa.cause, fb.cause))
        }
      case "Success":
        switch (fb._tag) {
          case "Success":
            return succeed(f(fa.value, fb.value))
          case "Failure":
            return fb
        }
    }
  }

/**
 * Returns the optional `Cause` of the failure of an `Exit`
 */
export const causeOption: <E, A>(exit: Exit<E, A>) => O.Option<C.Cause<E>> = match(
  O.some,
  () => O.none,
)

/**
 * Determines if the `Exit` is interrupted
 */
export const isInterrupted: <E, A>(exit: Exit<E, A>) => boolean = match(
  C.isInterrupted,
  () => false,
)

/**
 * Collects the successes and failures of multiple `Exit` into a single `Exit`,
 * combining Causes with `Then`
 */
export const collectAll = <E, A>(exits: V.Vector<Exit<E, A>>): O.Option<Exit<E, V.Vector<A>>> =>
  pipe(
    V.head(exits),
    O.map((head) =>
      pipe(
        exits,
        V.drop(1),
        V.reduce(pipe(head, map(V.of)), (acc, el) =>
          pipe(
            acc,
            zipWithCause(el, (as, a) => pipe(as, V.append(a)), C.then),
          ),
        ),
      ),
    ),
  )

/**
 * Collects the successes and failures of multiple `Exit` into a single `Exit`,
 * combining Causes with `Both`
 */
export const collectAllPar = <E, A>(exits: V.Vector<Exit<E, A>>): O.Option<Exit<E, V.Vector<A>>> =>
  pipe(
    V.head(exits),
    O.map((head) =>
      pipe(
        exits,
        V.drop(1),
        V.reduce(pipe(head, map(V.of)), (acc, el) =>
          pipe(
            acc,
            zipWithCause(el, (as, a) => pipe(as, V.append(a)), C.both),
          ),
        ),
      ),
    ),
  )

/**
 * The unit `Exit`, an `Exit` that has succeeded with `void`
 */
export const unit: Exit<never, void> = succeed(undefined)
