import type { Cause } from "../Cause"

export class Success<A> {
  readonly _tag = "Success"
  constructor(readonly value: A) {}
}

export class Failure<E> {
  readonly _tag = "Failure"
  constructor(readonly cause: Cause<E>) {}
}

export type Exit<E, A> = Failure<E> | Success<A>
