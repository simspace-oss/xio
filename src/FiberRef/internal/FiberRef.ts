import type { WithPatch } from "../definition"

import { identity } from "fp-ts/function"

import { FiberRefTypeId } from "../definition"

/** @internal */
export class FiberRef<Value, Patch> implements WithPatch<Value, Patch> {
  readonly [FiberRefTypeId]: FiberRefTypeId = FiberRefTypeId
  readonly _A!: () => Value
  readonly _Patch!: Patch
  constructor(
    readonly initial: Value,
    readonly diff: (oldValue: Value, newValue: Value) => Patch,
    readonly combine: (first: Patch, second: Patch) => Patch,
    readonly patch: (patch: Patch) => (oldValue: Value) => Value,
    readonly fork: Patch,
  ) {}
}

export function concrete<Value, Patch>(
  _: WithPatch<Value, Patch>,
): asserts _ is FiberRef<Value, Patch> {
  //
}

export const unsafeMakePatch = <Value, Patch>(
  initialValue: Value,
  diff: (oldValue: Value, newValue: Value) => Patch,
  combinePatch: (first: Patch, second: Patch) => Patch,
  patch: (patch: Patch) => (value: Value) => Value,
  fork: Patch,
): WithPatch<Value, Patch> => new FiberRef(initialValue, diff, combinePatch, patch, fork)

export const unsafeMake = <A>(
  initial: A,
  fork: (a: A) => A = identity,
  join: (first: A, second: A) => A = (_, a) => a,
): WithPatch<A, (a: A) => A> =>
  unsafeMakePatch(
    initial,
    (_, newValue) => () => newValue,
    (first, second) => (value) => second(first(value)),
    (patch) => (value) => join(value, patch(value)),
    fork,
  )
