import type { _A } from "../internal/types"

export const FiberRefTypeId = Symbol.for("@simspace/xio/FiberRef")
export type FiberRefTypeId = typeof FiberRefTypeId

/**
 * A `FiberRef` is the `XIO` equivalent of a `ThreadLocal`. The value of a
 * `FiberRef` is automatically propagated to child fibers when they are forked
 * and merged back in to the value of the parent fiber after they are joined.
 */
export interface FiberRef<Value> {
  readonly [FiberRefTypeId]: FiberRefTypeId
  readonly _A: () => Value
  readonly _Patch: unknown
}

export interface WithPatch<Value, Patch> extends FiberRef<Value> {
  readonly _Patch: Patch
}
