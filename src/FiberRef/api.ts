import type { FiberRef, WithPatch } from "./definition"

import { pipe } from "fp-ts/function"

import { concrete } from "./internal/FiberRef"
import * as XIO from "./internal/XIO"

export const initial = <A>(fiberRef: FiberRef<A>): A => {
  concrete(fiberRef)
  return fiberRef.initial
}

export const diff =
  <A>(oldValue: A, newValue: A) =>
  <P>(fiberRef: WithPatch<A, P>): P => {
    concrete(fiberRef)
    return fiberRef.diff(oldValue, newValue)
  }

export const patch =
  <A, P>(patch: P, oldValue: A) =>
  (fiberRef: WithPatch<A, P>): A => {
    concrete(fiberRef)
    return fiberRef.patch(patch)(oldValue)
  }

export const fork = <A, P>(fiberRef: WithPatch<A, P>): P => {
  concrete(fiberRef)
  return fiberRef.fork
}

export const modify =
  <A, B>(f: (a: A) => [B, A]) =>
  (fiberRef: FiberRef<A>): XIO.UIO<B> =>
    new XIO.FiberRefModify(fiberRef, f)

export const get: <A>(fiberRef: FiberRef<A>) => XIO.UIO<A> = modify((a) => [a, a])

export const set = <A, B extends A>(a: B): ((fiberRef: FiberRef<A>) => XIO.UIO<void>) =>
  modify<A, void>(() => [undefined, a])

export const update = <A>(f: (a: A) => A): ((fiberRef: FiberRef<A>) => XIO.UIO<void>) =>
  modify((a) => [undefined, f(a)])

export const remove = <A>(fiberRef: FiberRef<A>): XIO.UIO<void> => new XIO.FiberRefDelete(fiberRef)

export const locally =
  <A>(fiberRef: FiberRef<A>, value: A) =>
  <R, E, B>(xio: XIO.XIO<R, E, B>) =>
    new XIO.FiberRefLocally(value, fiberRef, xio)

export const getWith =
  <A, R, E, B>(f: (a: A) => XIO.XIO<R, E, B>) =>
  (fiberRef: FiberRef<A>): XIO.XIO<R, E, B> =>
    new XIO.FiberRefWith(fiberRef, f)

export const locallyWith =
  <A>(fiberRef: FiberRef<A>, f: (a: A) => A) =>
  <R, E, B>(xio: XIO.XIO<R, E, B>): XIO.XIO<R, E, B> =>
    pipe(
      fiberRef,
      getWith((a) => pipe(xio, locally(fiberRef, f(a)))),
    )
