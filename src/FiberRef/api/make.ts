import type { Scope } from "../../Scope"
import type { Has } from "../../Tag"
import type { FiberRef, WithPatch } from "../definition"

import { identity, pipe } from "fp-ts/function"

import { remove, update } from "../api"
import { unsafeMake } from "../internal/FiberRef"
import * as XIO from "./internal/XIO"

export const make = <A>(
  initial: A,
  fork: (a: A) => A = identity,
  join: (first: A, second: A) => A = (_, a) => a,
): XIO.XIO<Has<Scope>, never, FiberRef<A>> => makeWith(unsafeMake(initial, fork, join))

function makeWith<Value, Patch>(
  ref: WithPatch<Value, Patch>,
): XIO.XIO<Has<Scope>, never, WithPatch<Value, Patch>> {
  return XIO.acquireRelease(
    pipe(
      XIO.fromIO(() => ref),
      XIO.chainFirst(update(identity)),
    ),
    remove,
  ) as XIO.XIO<Has<Scope>, never, WithPatch<Value, Patch>>
}
