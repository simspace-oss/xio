import type { FiberScope } from "../Fiber/internal/FiberScope"
import type { Scheduler } from "../internal/Scheduler"

import { identity } from "fp-ts/function"
import * as O from "fp-ts/Option"

import { defaultScheduler } from "../internal/Scheduler"
import { unsafeMake } from "./internal/FiberRef"

export const forkScopeOverride = unsafeMake<O.Option<FiberScope>>(O.none, () => O.none)

export const currentEnvironment = unsafeMake<any>(undefined, identity, (a, _) => a)

export const currentScheduler = unsafeMake<Scheduler>(defaultScheduler)
