import type { Cause } from "../Cause"
import type { Fiber } from "../Fiber"
import type { FiberDescriptor } from "../Fiber/FiberDescriptor"
import type { FiberContext } from "../Fiber/internal/FiberContext"
import type { FiberScope } from "../Fiber/internal/FiberScope"
import type * as FId from "../FiberId"
import type { FiberRef } from "../FiberRef/definition"
import type { FiberRefs } from "../FiberRefs"
import type { InterruptStatus } from "../InterruptStatus"
import type { Sync } from "../Sync"
import type { Either } from "fp-ts/Either"
import type { Option } from "fp-ts/Option"

export const XIOTypeId = Symbol.for("@simspace/xio/XIO")
export type XIOTypeId = typeof XIOTypeId

export abstract class XIO<R, E, A> {
  readonly [XIOTypeId]: XIOTypeId = XIOTypeId
  readonly _S1!: (_: unknown) => void
  readonly _S2!: () => unknown
  readonly _R!: (_: R) => void
  readonly _E!: () => E
  readonly _A!: () => A
}

export type UIO<A> = XIO<unknown, never, A>
export type URIO<R, A> = XIO<R, never, A>

/** @internal */
export const enum Tag {
  SucceedNow,
  Succeed,
  Fail,
  Defer,
  Async,
  Chain,
  Read,
  Provide,
  Match,
  Ensuring,
  Fork,
  Yield,
  SetInterruptStatus,
  GetInterruptStatus,
  Descriptor,
  FiberRefModifyAll,
  FiberRefModify,
  FiberRefLocally,
  FiberRefDelete,
  FiberRefWith,
  RaceWith,
  GetForkScope,
  OverrideForkScope,
  Sync,
}

/** @internal */
export class Succeed<A> extends XIO<unknown, never, A> {
  readonly _tag = Tag.SucceedNow
  constructor(readonly value: A) {
    super()
  }
}

/** @internal */
export class FromIO<A> extends XIO<unknown, never, A> {
  readonly _tag = Tag.Succeed
  constructor(readonly value: () => A) {
    super()
  }
}

/** @internal */
export class Fail<E> extends XIO<unknown, E, never> {
  readonly _tag = Tag.Fail
  constructor(readonly cause: () => Cause<E>) {
    super()
  }
}

/** @internal */
export class Defer<R, E, A> extends XIO<R, E, A> {
  readonly _tag = Tag.Defer
  constructor(readonly xio: () => XIO<R, E, A>) {
    super()
  }
}

/** @internal */
export class Async<R, E, A> extends XIO<R, E, A> {
  readonly _tag = Tag.Async
  constructor(
    readonly register: (
      k: (cont: XIO<R, E, A>) => void,
    ) => Either<XIO<R, never, void>, XIO<R, E, A>>,
    readonly blockingOn: FId.FiberId,
  ) {
    super()
  }
}

/** @internal */
export class Chain<R, E, A, R1, E1, B> extends XIO<R & R1, E | E1, B> {
  readonly _tag = Tag.Chain
  constructor(readonly xio: XIO<R, E, A>, readonly f: (a: A) => XIO<R1, E1, B>) {
    super()
  }
}

/** @internal */
export class Match<R, E, A, R1, E1, B, R2, E2, C> extends XIO<R & R1 & R2, E1 | E2, B | C> {
  readonly _tag = Tag.Match
  constructor(
    readonly xio: XIO<R, E, A>,
    readonly onFailure: (cause: Cause<E>) => XIO<R1, E1, B>,
    readonly onSuccess: (a: A) => XIO<R2, E2, C>,
  ) {
    super()
  }
}

/** @internal */
export class Read<R0, R, E, A> extends XIO<R0 & R, E, A> {
  readonly _tag = Tag.Read
  constructor(readonly reader: (environment: R0) => XIO<R, E, A>) {
    super()
  }
}

/** @internal */
export class Provide<R, E, A> extends XIO<unknown, E, A> {
  readonly _tag = Tag.Provide
  constructor(readonly xio: XIO<R, E, A>, readonly environment: R) {
    super()
  }
}

/** @internal */
export class Ensuring<R, E, A, R1> extends XIO<R, E, A> {
  readonly _tag = Tag.Ensuring
  constructor(readonly xio: XIO<R, E, A>, readonly finalizer: XIO<R1, never, void>) {
    super()
  }
}

/** @internal */
export class Fork<R, E, A> extends XIO<R, never, FiberContext<E, A>> {
  readonly _tag = Tag.Fork
  constructor(readonly xio: XIO<R, E, A>, readonly scope: Option<FiberScope>) {
    super()
  }
}

/** @internal */
export class Yield extends XIO<unknown, never, void> {
  readonly _tag = Tag.Yield
}

/** @internal */
export class SetInterruptStatus<R, E, A> extends XIO<R, E, A> {
  readonly _tag = Tag.SetInterruptStatus
  constructor(readonly xio: XIO<R, E, A>, readonly flag: InterruptStatus) {
    super()
  }
}

/** @internal */
export class GetInterruptStatus<R, E, A> extends XIO<R, E, A> {
  readonly _tag = Tag.GetInterruptStatus
  constructor(readonly f: (status: InterruptStatus) => XIO<R, E, A>) {
    super()
  }
}

/** @internal */
export class Descriptor<R, E, A> extends XIO<R, E, A> {
  readonly _tag = Tag.Descriptor
  constructor(readonly f: (descriptor: FiberDescriptor) => XIO<R, E, A>) {
    super()
  }
}

/** @internal */
export class FiberRefModifyAll<A> extends XIO<unknown, never, A> {
  readonly _tag = Tag.FiberRefModifyAll
  constructor(readonly f: (fiberId: FId.Runtime, fiberRefs: FiberRefs) => [A, FiberRefs]) {
    super()
  }
}

/** @internal */
export class FiberRefModify<A, B> extends XIO<unknown, never, B> {
  readonly _tag = Tag.FiberRefModify
  constructor(readonly fiberRef: FiberRef<A>, readonly f: (a: A) => [B, A]) {
    super()
  }
}

/** @internal */
export class FiberRefLocally<V, R, E, A> extends XIO<R, E, A> {
  readonly _tag = Tag.FiberRefLocally
  constructor(readonly localValue: V, readonly fiberRef: FiberRef<V>, readonly xio: XIO<R, E, A>) {
    super()
  }
}

/** @internal */
export class FiberRefDelete extends XIO<unknown, never, void> {
  readonly _tag = Tag.FiberRefDelete
  constructor(readonly fiberRef: FiberRef<unknown>) {
    super()
  }
}

/** @internal */
export class FiberRefWith<R, E, A, B> extends XIO<R, E, B> {
  readonly _tag = Tag.FiberRefWith
  constructor(readonly fiberRef: FiberRef<A>, readonly f: (a: A) => XIO<R, E, B>) {
    super()
  }
}

/** @internal */
export class RaceWith<R, E, A, R1, E1, B, R2, E2, C, R3, E3, D> extends XIO<
  R & R1 & R2 & R3,
  E2 | E3,
  C | D
> {
  readonly _tag = Tag.RaceWith
  constructor(
    readonly left: XIO<R, E, A>,
    readonly right: XIO<R1, E1, B>,
    readonly leftWins: (winner: Fiber<E, A>, loser: Fiber<E1, B>) => XIO<R2, E2, C>,
    readonly rightWins: (winner: Fiber<E1, B>, loser: Fiber<E, A>) => XIO<R3, E3, D>,
  ) {
    super()
  }
}

/** @internal */
export class GetForkScope<R, E, A> extends XIO<R, E, A> {
  readonly _tag = Tag.GetForkScope
  constructor(readonly f: (scope: FiberScope) => XIO<R, E, A>) {
    super()
  }
}

/** @internal */
export class OverrideForkScope<R, E, A> extends XIO<R, E, A> {
  readonly _tag = Tag.OverrideForkScope
  constructor(readonly xio: XIO<R, E, A>, readonly forkScope: Option<FiberScope>) {
    super()
  }
}

/** @internal */
export type Concrete =
  | Sync<unknown, unknown, unknown, unknown, unknown>
  | Succeed<unknown>
  | FromIO<unknown>
  | Fail<unknown>
  | Defer<unknown, unknown, unknown>
  | Chain<unknown, unknown, unknown, unknown, unknown, unknown>
  | Read<unknown, unknown, unknown, unknown>
  | Provide<unknown, unknown, unknown>
  | Match<unknown, unknown, unknown, unknown, unknown, unknown, unknown, unknown, unknown>
  | Async<unknown, unknown, unknown>
  | Ensuring<unknown, unknown, unknown, unknown>
  | Fork<unknown, unknown, unknown>
  | Yield
  | SetInterruptStatus<unknown, unknown, unknown>
  | GetInterruptStatus<unknown, unknown, unknown>
  | Descriptor<unknown, unknown, unknown>
  | FiberRefModifyAll<unknown>
  | FiberRefModify<unknown, unknown>
  | FiberRefLocally<unknown, unknown, unknown, unknown>
  | FiberRefDelete
  | FiberRefWith<unknown, unknown, unknown, unknown>
  | GetForkScope<unknown, unknown, unknown>
  | OverrideForkScope<unknown, unknown, unknown>
  | RaceWith<
      unknown,
      unknown,
      unknown,
      unknown,
      unknown,
      unknown,
      unknown,
      unknown,
      unknown,
      unknown,
      unknown,
      unknown
    >
