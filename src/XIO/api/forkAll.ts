import type { XIO } from "../definition"
import type { Vector } from "@simspace/collections/Vector"

import { pipe } from "fp-ts/lib/function"

import * as Fi from "../../Fiber"
import { foreach, fork, map } from "./core"

export const forkAll = <R, E, A>(
  as: Iterable<XIO<R, E, A>>,
): XIO<R, never, Fi.Fiber<E, Vector<A>>> => pipe(foreach(as, fork), map(Fi.collectAll))
