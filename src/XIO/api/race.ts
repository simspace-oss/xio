import type { Fiber } from "../../Fiber/definition"
import type { XIO } from "../definition"

import { size } from "@simspace/collections/Iterable"
import * as V from "@simspace/collections/Vector"
import * as E from "fp-ts/Either"
import { flow, pipe } from "fp-ts/function"

import * as C from "../../Cause"
import * as Ex from "../../Exit"
import * as Fi from "../../Fiber/api"
import * as F from "../../Future"
import * as Ref from "../../Ref"
import { RaceWith } from "../definition"
import {
  chain,
  chainFirst,
  defer,
  descriptorWith,
  done,
  foreach,
  fork,
  gen,
  interruptible,
  map,
  mapErrorCause,
  onInterrupt,
  result,
  uninterruptibleMask,
  unit,
} from "./core"
import { uninterruptibleMaskForce } from "./fiberScope"

/**
 * Returns an effect that races this effect with the specified effect, calling
 * the specified finisher as soon as one result or the other has been
 * computed.
 */
export const raceWith =
  <E, A, R1, E1, B, R2, E2, C, R3, E3, D>(
    that: XIO<R1, E1, B>,
    leftDone: (winner: Ex.Exit<E, A>, loser: Fiber<E1, B>) => XIO<R2, E2, C>,
    rightDone: (winnder: Ex.Exit<E1, B>, loser: Fiber<E, A>) => XIO<R3, E3, D>,
  ) =>
  <R>(xio: XIO<R, E, A>): XIO<R & R1 & R2 & R3, E2 | E3, C | D> =>
    defer(
      () =>
        new RaceWith(
          xio,
          that,
          (winner, loser) =>
            pipe(
              Fi.await(winner),
              chain((exit) =>
                pipe(
                  exit,
                  Ex.match(
                    () => leftDone(exit, loser),
                    () =>
                      pipe(
                        Fi.inheritRefs(winner),
                        chain(() => leftDone(exit, loser)),
                      ),
                  ),
                ),
              ),
            ),
          (winner, loser) =>
            pipe(
              Fi.await(winner),
              chain((exit) =>
                pipe(
                  exit,
                  Ex.match(
                    () => rightDone(exit, loser),
                    () =>
                      pipe(
                        Fi.inheritRefs(winner),
                        chain(() => rightDone(exit, loser)),
                      ),
                  ),
                ),
              ),
            ),
        ),
    )

/**
 * Returns an effect that races this effect with the specified effect,
 * returning the first successful `A` from the faster side. If one effect
 * succeeds, the other will be interrupted. If neither succeeds, then the
 * effect will fail with some error.
 *
 * WARNING: The raced effect will safely interrupt the "loser", but will not
 * resume until the loser has been cleanly terminated. If early return is
 * desired, then instead of performing `pipe(l, race(r))`, perform
 * `pipe(disconnect(l), race(disconnect(r)))`,
 * which disconnects left and right interruption signals,
 * allowing a fast return, with interruption performed in the background.
 *
 * Note that if the `race` is embedded into an uninterruptible region, then
 * because the loser cannot be interrupted, it will be allowed to continue
 * executing in the background, without delaying the return of the race.
 */
export const race =
  <R1, E1, B>(that: XIO<R1, E1, B>) =>
  <R, E, A>(xio: XIO<R, E, A>): XIO<R & R1, E | E1, A | B> =>
    descriptorWith((descriptior) => {
      const parentId = descriptior.id

      const maybeDisconnect = <R, E, A>(xio: XIO<R, E, A>): XIO<R, E, A> =>
        uninterruptibleMaskForce((force) => force(xio))

      return pipe(
        maybeDisconnect(xio),
        raceWith(
          maybeDisconnect(that),
          (exit, right) =>
            pipe(
              exit,
              Ex.matchXIO(
                (cause) =>
                  pipe(
                    Fi.join(right),
                    mapErrorCause((cause2) => C.both(cause, cause2)),
                  ),
                (a) =>
                  pipe(
                    right,
                    Fi.interruptAs(parentId),
                    map(() => a),
                  ),
              ),
            ),
          (exit, left) =>
            pipe(
              exit,
              Ex.matchXIO(
                (cause) =>
                  pipe(
                    Fi.join(left),
                    mapErrorCause((cause2) => C.both(cause2, cause)),
                  ),
                (a) =>
                  pipe(
                    left,
                    Fi.interruptAs(parentId),
                    map(() => a),
                  ),
              ),
            ),
        ),
      )
    })

/**
 * Returns an effect that races this effect with the specified effect,
 * yielding the first result to succeed. If neither effect succeeds, then the
 * composed effect will fail with some error.
 *
 * WARNING: The raced effect will safely interrupt the "loser", but will not
 * resume until the loser has been cleanly terminated. If early return is
 * desired, then instead of performing `pipe(l, raceEither(r))`, perform
 * `pipe(disconnect(l), raceEither(disconnect(r)))`,
 * which disconnects left and right interruption signals,
 * allowing a fast return, with interruption performed in the background.
 */
export const raceEither =
  <R1, E1, B>(right: XIO<R1, E1, B>) =>
  <R, E, A>(left: XIO<R, E, A>): XIO<R & R1, E | E1, E.Either<A, B>> =>
    pipe(left, map(E.left), race(pipe(right, map(E.right))))

/**
 * Returns an effect that races this effect with the specified effect,
 * yielding the first result to complete, whether by success or failure. If
 * neither effect completes, then the composed effect will not complete.
 *
 * WARNING: The raced effect will safely interrupt the "loser", but will not
 * resume until the loser has been cleanly terminated. If early return is
 * desired, then instead of performing `pipe(l, raceFirst(r))`, perform
 * `pipe(disconnect(l), raceFirst(disconnect(r)))`,
 * which disconnects left and right interruption signals,
 * allowing a fast return, with interruption performed in the background.
 */
export const raceFirst =
  <R1, E1, B>(that: XIO<R1, E1, B>) =>
  <R, E, A>(xio: XIO<R, E, A>): XIO<R & R1, E | E1, A | B> =>
    pipe(
      result(xio),
      race(result(that)),
      chain((ex) => done<E | E1, A | B>(ex)),
    )

/**
 * Returns an effect that races all the specified effects,
 * yielding the value of the first effect to succeed with a value. Losers of
 * the race will be interrupted immediately
 */
export const raceAll = <R, E, A>(xios: Iterable<XIO<R, E, A>>): XIO<R, E, A> =>
  gen(function* (_) {
    const done = yield* _(F.make<E, [A, Fiber<E, A>]>())
    const fails = yield* _(Ref.make(size(xios)))
    return yield* _(
      uninterruptibleMask((restore) =>
        gen(function* (_) {
          const fibers = yield* _(foreach(xios, flow(interruptible, fork)))
          yield* _(
            pipe(
              fibers,
              V.reduce(unit, (awaiter, fiber) =>
                pipe(
                  awaiter,
                  chain(() =>
                    pipe(Fi.await(fiber), chain(raceAllArbiter(fibers, fiber, done, fails)), fork),
                  ),
                ),
              ),
            ),
          )
          const inheritRefs = (res: [A, Fiber<E, A>]) =>
            pipe(
              Fi.inheritRefs(res[1]),
              map(() => res[0]),
            )
          return yield* _(
            pipe(
              restore(pipe(F.await(done), chain(inheritRefs))),
              onInterrupt(
                pipe(
                  fibers,
                  V.reduce(unit, (interruption, fiber) =>
                    pipe(
                      interruption,
                      chainFirst(() => Fi.interrupt(fiber)),
                    ),
                  ),
                ),
              ),
            ),
          )
        }),
      ),
    )
  })

function raceAllArbiter<R, E, A>(
  fibers: V.Vector<Fiber<E, A>>,
  winner: Fiber<E, A>,
  future: F.Future<E, [A, Fiber<E, A>]>,
  fails: Ref.Ref<number>,
) {
  return (res: Ex.Exit<E, A>): XIO<R, never, unknown> =>
    pipe(
      res,
      Ex.matchXIO(
        (cause) =>
          pipe(
            fails,
            Ref.modify((n) => {
              if (n === 0) {
                return [pipe(future, F.failCause(cause)), n - 1]
              } else {
                return [unit, n - 1]
              }
            }),
          ),
        (a) =>
          pipe(
            future,
            F.succeed([a, winner]),
            chain((set) => {
              if (set) {
                return pipe(
                  fibers,
                  V.reduce(unit, (xio, f) =>
                    f === winner
                      ? xio
                      : pipe(
                          xio,
                          chainFirst(() => Fi.interrupt(f)),
                        ),
                  ),
                )
              } else {
                return unit
              }
            }),
          ),
      ),
    )
}
