import type { UIO, XIO } from "../definition"

import { pipe } from "fp-ts/function"
import * as O from "fp-ts/Option"

import * as FR from "../../FiberRef/api"
import { unsafeMake } from "../../FiberRef/internal/FiberRef"
import { defer } from "./core"

/**
 * A `FiberRef` describing the number of fibers used to concurrent operators
 */
export const Concurrency = unsafeMake<O.Option<number>>(O.none)

/**
 * Retrieves the maximum number of fibers for concurrent operators or `None` if
 * it is unbounded
 */
export const concurrency: UIO<O.Option<number>> = FR.get(Concurrency)

/**
 * Retrieves the current maximum number of fibers for concurrent operators and
 * uses it to run the specified `XIO`
 */
export const concurrencyWith = <R, E, A>(f: (concurrency: O.Option<number>) => XIO<R, E, A>) =>
  pipe(Concurrency, FR.getWith(f))

/**
 * Runs the specified `XIO` with the specified maximum number of fibers for
 * concurrent operators.
 */
export const withConcurrency =
  (n: number) =>
  <R, E, A>(xio: XIO<R, E, A>): XIO<R, E, A> =>
    pipe(xio, FR.locally(Concurrency, O.some(n)))

/**
 * Runs the specified `XIO` with an unbounded maximum number of fibers for
 * concurrent operators.
 */
export const withConcurrencyUnbounded = <R, E, A>(xio: XIO<R, E, A>): XIO<R, E, A> =>
  defer(() => pipe(xio, FR.locally(Concurrency, O.none)))
