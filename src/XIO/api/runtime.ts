import type { Cause } from "../../Cause"
import type { FiberRef } from "../../FiberRef/definition"
import type { XIO } from "../definition"
import type { ReaderTaskEither } from "fp-ts/lib/ReaderTaskEither"

import * as HM from "@simspace/collections/HashMap"
import * as L from "@simspace/collections/List"
import * as E from "fp-ts/Either"
import { identity, pipe } from "fp-ts/function"
import * as O from "fp-ts/Option"

import * as Ex from "../../Exit"
import { FiberContext } from "../../Fiber/internal/FiberContext"
import * as FId from "../../FiberId"
import { currentEnvironment, currentScheduler } from "../../FiberRef/primitives"
import { StagedScheduler } from "../../internal/Scheduler"
import { Stack } from "../../internal/Stack"
import * as XIOEnv from "../../XIOEnv"
import { provide } from "./core"

export interface RuntimeConfig {
  readonly yieldOpCount: number
  readonly immediate: boolean
}

export class Runtime<R> {
  constructor(readonly environment: R, readonly config: RuntimeConfig) {}

  unsafeRunFiber = <E, A>(xio: XIO<R, E, A>): FiberContext<E, A> => {
    const fiberId = FId.unsafeMake()

    const children = new Set<FiberContext<unknown, unknown>>()

    const interruptStack = new Stack<boolean>()
    interruptStack.push(true)

    const context = new FiberContext<E, A>(
      fiberId,
      this.config,
      interruptStack,
      HM.fromReadonlyArray<FiberRef<unknown>, L.Cons<[FId.Runtime, unknown]>>([
        [XIOEnv.services, L.cons([fiberId, XIOEnv.live])],
        [currentEnvironment, L.cons([fiberId, this.environment])],
      ]),
      children,
    )

    if (this.config.immediate) {
      context.unsafeRunNow(xio)
    } else {
      context.unsafeRunLater(xio)
    }

    return context
  }

  unsafeRunCancelableWith = <E, A>(
    xio: XIO<R, E, A>,
    k: (exit: Ex.Exit<E, A>) => void,
  ): ((fiberId: FId.FiberId) => (k: (exit: Ex.Exit<E, A>) => void) => void) => {
    const fiberId = FId.unsafeMake()

    const children = new Set<FiberContext<unknown, unknown>>()

    const interruptStack = new Stack<boolean>()
    interruptStack.push(true)

    const context = new FiberContext<E, A>(
      fiberId,
      this.config,
      interruptStack,
      HM.fromReadonlyArray<FiberRef<unknown>, L.Cons<[FId.Runtime, unknown]>>([
        [XIOEnv.services, L.cons([fiberId, XIOEnv.live])],
        [currentEnvironment, L.cons([fiberId, this.environment])],
      ]),
      children,
    )

    context.unsafeAddObserver((exit) => {
      k(Ex.flatten(exit))
    })
    if (this.config.immediate) {
      context.unsafeRunNow(xio)
    } else {
      context.unsafeRunLater(xio)
    }

    return (fiberId) => (k) =>
      this.unsafeRunWith((exit: Ex.Exit<never, Ex.Exit<E, A>>) => k(Ex.flatten(exit)))(
        context.interruptAs(fiberId),
      )
  }

  unsafeRunCancelable = <E, A>(
    xio: XIO<R, E, A>,
    k: (exit: Ex.Exit<E, A>) => void,
  ): ((fiberId: FId.FiberId) => void) => {
    const canceler = this.unsafeRunCancelableWith(xio, k)
    return (fiberId) =>
      canceler(fiberId)(() => {
        //
      })
  }

  unsafeRunWith =
    <E, A>(k: (exit: Ex.Exit<E, A>) => void) =>
    (xio: XIO<R, E, A>): void => {
      this.unsafeRunCancelable(xio, k)
    }

  unsafeRun = <E, A>(xio: XIO<R, E, A>): void => {
    this.unsafeRunCancelable(xio, () => {
      //
    })
  }

  unsafeRunPromiseExit = <E, A>(xio: XIO<R, E, A>): Promise<Ex.Exit<E, A>> =>
    new Promise((resolve) => {
      this.unsafeRunWith(resolve)(xio)
    })

  unsafeRunPromise = <E, A>(xio: XIO<R, E, A>): Promise<A> =>
    this.unsafeRunPromiseExit(xio).then(
      Ex.match((cause) => {
        throw cause
      }, identity),
    )

  unsafeRunSyncExit = <E, A>(xio: XIO<R, E, A>): Ex.Exit<E | FiberContext<E, A>, A> => {
    const fiberId = FId.unsafeMake()

    const children = new Set<FiberContext<unknown, unknown>>()

    const interruptStack = new Stack<boolean>()
    interruptStack.push(true)

    const scheduler = new StagedScheduler()

    const context = new FiberContext<E, A>(
      fiberId,
      this.config,
      interruptStack,
      HM.fromReadonlyArray<FiberRef<unknown>, L.Cons<[FId.Runtime, unknown]>>([
        [XIOEnv.services, L.cons([fiberId, XIOEnv.live])],
        [currentEnvironment, L.cons([fiberId, this.environment])],
        [currentScheduler, L.cons([fiberId, scheduler])],
      ]),
      children,
    )

    context.unsafeRunNow(xio)
    scheduler.flush()

    const result = context.unsafePoll()

    if (O.isSome(result)) {
      return result.value
    }

    return Ex.fail(context)
  }
}

const defaultRuntimeConfig: RuntimeConfig = {
  yieldOpCount: 2048,
  immediate: false,
}

export const {
  unsafeRunCancelableWith,
  unsafeRunCancelable,
  unsafeRunPromiseExit,
  unsafeRunPromise,
  unsafeRunWith,
  unsafeRun,
  unsafeRunFiber,
  unsafeRunSyncExit,
} = new Runtime({} as unknown, defaultRuntimeConfig)

export const toReaderTaskEither =
  <R, E, A>(xio: XIO<R, E, A>): ReaderTaskEither<R, Cause<E>, A> =>
  (r) =>
  () =>
    pipe(xio, provide(r), unsafeRunPromiseExit).then(Ex.match(E.left, E.right))
