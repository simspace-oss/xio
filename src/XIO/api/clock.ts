import type { UIO, XIO } from "../definition"

import { pipe } from "fp-ts/function"

import * as Clock from "../../Clock"
import { chain } from "./core"

export const sleep = (duration: number): UIO<void> => Clock.sleep(duration)

export const delay =
  (duration: number) =>
  <R, E, A>(xio: XIO<R, E, A>): XIO<R, E, A> =>
    pipe(
      sleep(duration),
      chain(() => xio),
    )
