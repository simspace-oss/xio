import type { XIO } from "../definition"

import { pipe } from "fp-ts/function"
import * as O from "fp-ts/Option"

import { sleep } from "./clock"
import { interruptible, map } from "./core"
import { raceFirst } from "./race"

export const timeoutTo =
  <A, B, C>(duration: number, def: () => B, f: (a: A) => C) =>
  <R, E>(xio: XIO<R, E, A>): XIO<R, E, B | C> =>
    pipe(
      xio,
      map(f),
      raceFirst(
        pipe(
          sleep(duration),
          interruptible,
          map(() => def()),
        ),
      ),
    )

export const timeout = (
  duration: number,
): (<R, E, A>(xio: XIO<R, E, A>) => XIO<R, E, O.Option<A>>) =>
  timeoutTo(duration, () => O.none, O.some)
