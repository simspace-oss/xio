import type * as FId from "../../FiberId"
import type { XIO } from "../definition"

import { pipe, tuple, unsafeCoerce } from "fp-ts/function"

import * as C from "../../Cause"
import * as Ex from "../../Exit"
import * as Fi from "../../Fiber"
import { RaceWith } from "../definition"
import { apSecond, chain, descriptorWith, failCause, succeed, uninterruptibleMask } from "./core"
import { transplant } from "./fiberScope"

/**
 * Returns an effect that executes both this effect and the specified effect,
 * in parallel, combining their results with the specified `f` function. If
 * either side fails, then the other side will be interrupted.
 */
export const zipWithPar =
  <A, R1, E1, B, C>(fb: XIO<R1, E1, B>, f: (a: A, b: B) => C) =>
  <R, E>(fa: XIO<R, E, A>): XIO<R & R1, E | E1, C> => {
    const g = (b: B, a: A) => f(a, b)
    return uninterruptibleMask((restore) =>
      transplant((graft) =>
        descriptorWith(
          (descriptor) =>
            new RaceWith(
              graft(restore(fa)),
              graft(restore(fb)),
              coordinate(descriptor.id, f, true),
              coordinate(descriptor.id, g, false),
            ),
        ),
      ),
    )
  }

/**
 * Returns an effect that executes both this effect and the specified effect,
 * in parallel, combining their results into a tuple. If either side fails,
 * then the other side will be interrupted.
 */
export const zipPar = <R1, E1, B>(
  fb: XIO<R1, E1, B>,
): (<R, E, A>(fa: XIO<R, E, A>) => XIO<R & R1, E | E1, readonly [A, B]>) => zipWithPar(fb, tuple)

export const apPar = <R1, E1, A>(
  fa: XIO<R1, E1, A>,
): (<R, E, B>(fab: XIO<R, E, (a: A) => B>) => XIO<R & R1, E | E1, B>) =>
  zipWithPar(fa, (f, a) => f(a))

/**
 * Returns an effect that executes both this effect and the specified effect,
 * in parallel, the first effect's result is returned. If either side fails, then the
 * other side will be interrupted.
 */
export const apFirstPar = <R1, E1, B>(
  fb: XIO<R1, E1, B>,
): (<R, E, A>(fa: XIO<R, E, A>) => XIO<R & R1, E | E1, A>) => zipWithPar(fb, (a, _) => a)

/**
 * Returns an effect that executes both this effect and the specified effect,
 * in parallel, the second effect's result is returned. If either side fails, then the
 * other side will be interrupted.
 */
export const apSecondPar = <R1, E1, B>(
  fb: XIO<R1, E1, B>,
): (<R, E, A>(fa: XIO<R, E, A>) => XIO<R & R1, E | E1, B>) => zipWithPar(fb, (_, b) => b)

export const apSPar =
  <N extends string, A, R1, E1, B>(name: Exclude<N, keyof A>, fb: XIO<R1, E1, B>) =>
  <R, E>(
    fa: XIO<R, E, A>,
  ): XIO<R & R1, E | E1, { readonly [K in keyof A | N]: K extends keyof A ? A[K] : B }> =>
    pipe(
      fa,
      zipWithPar(fb, (a, b) => unsafeCoerce({ ...a, [name]: b })),
    )

const coordinate =
  <A, B, C>(fiberId: FId.FiberId, f: (a: A, b: B) => C, leftWinner: boolean) =>
  <R, E, R1, E1>(winner: Fi.Fiber<E, A>, loser: Fi.Fiber<E1, B>): XIO<R & R1, E | E1, C> => {
    return pipe(
      Fi.await(winner),
      chain(
        Ex.match(
          (cause) =>
            pipe(
              loser,
              Fi.interruptAs(fiberId),
              chain(
                Ex.match(
                  (loserCause) =>
                    leftWinner
                      ? failCause(C.both(cause, loserCause))
                      : failCause(C.both(loserCause, cause)),
                  () => failCause(cause),
                ),
              ),
            ),
          (a) =>
            pipe(
              Fi.await(loser),
              chain(
                Ex.match(failCause, (b) =>
                  pipe(
                    Fi.inheritRefs(winner),
                    apSecond(Fi.inheritRefs(loser)),
                    apSecond(succeed(f(a, b))),
                  ),
                ),
              ),
            ),
        ),
      ),
    )
  }
