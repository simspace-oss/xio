import type { XIO } from "../definition"
import type { ReadonlyNonEmptyArray } from "fp-ts/lib/ReadonlyNonEmptyArray"
import type { Separated } from "fp-ts/lib/Separated"

import { size, zipWithIndex } from "@simspace/collections/Iterable"
import * as V from "@simspace/collections/Vector"
import { flow, identity, pipe } from "fp-ts/function"
import * as O from "fp-ts/Option"
import * as RA from "fp-ts/ReadonlyArray"

import * as C from "../../Cause"
import * as Ex from "../../Exit"
import * as FId from "../../FiberId"
import * as F from "../../Future"
import { AtomicNumber } from "../../internal/AtomicNumber"
import * as Q from "../../Queue"
import { concurrencyWith } from "./concurrency"
import {
  bind,
  chain,
  chainFirst,
  defer,
  Do,
  either,
  failCause,
  foreach,
  foreachDiscard,
  fromIO,
  map,
  matchCauseXIO,
  succeed,
  uninterruptibleMask,
  unit,
} from "./core"
import { forkDaemon, transplant } from "./fiberScope"
import * as Fi from "./internal/foreachPar-fiber"

/**
 * Applies the function `f` to each element of the `Iterable<A>` and runs
 * produced effects concurrently, discarding the results.
 *
 * For a sequential version of this function, see `foreachDiscard`.
 *
 * Optimized to avoid keeping full tree of effects, so that method could be
 * able to handle large input sequences.
 *
 * Additionally, interrupts all effects on any failure.
 */
export const foreachDiscardPar = <R, E, A, B>(
  as: Iterable<A>,
  f: (a: A) => XIO<R, E, B>,
): XIO<R, E, void> =>
  concurrencyWith(
    O.match(
      () => foreachConcurrentUnboundedDiscard(as, f),
      (n) => foreachConcurrentBoundedDiscard(as, n, f),
    ),
  )

/**
 * Applies the function `f` to each element of the `Iterable<A>` concurrently,
 * and returns the results in a `Vector<B>`.
 *
 * For a sequential version of this function, see `foreach`.
 */
export const foreachPar = <R, E, A, B>(
  as: Iterable<A>,
  f: (a: A) => XIO<R, E, B>,
): XIO<R, E, V.Vector<B>> =>
  concurrencyWith(
    O.match(
      () => foreachConcurrentUnbounded(as, (_, a) => f(a)),
      (n) => foreachConcurrentBounded(as, n, (_, a) => f(a)),
    ),
  )

/**
 * Applies the function `f` to each element of the `Iterable<A>` concurrently,
 * and returns the results in a `Vector<B>`.
 *
 * For a sequential version of this function, see `foreach`.
 */
export const foreachWithIndexPar = <R, E, A, B>(
  as: Iterable<A>,
  f: (i: number, a: A) => XIO<R, E, B>,
): XIO<R, E, V.Vector<B>> =>
  concurrencyWith(
    O.match(
      () => foreachConcurrentUnbounded(as, f),
      (n) => foreachConcurrentBounded(as, n, f),
    ),
  )

export const traverseReadonlyNonEmptyArrayWithIndexPar =
  <A, R, E, B>(f: (index: number, a: A) => XIO<R, E, B>) =>
  (as: ReadonlyNonEmptyArray<A>): XIO<R, E, ReadonlyNonEmptyArray<B>> =>
    pipe(foreachWithIndexPar(as, f), map(V.toReadonlyArray)) as unknown as XIO<
      R,
      E,
      ReadonlyNonEmptyArray<B>
    >

export const traverseReadonlyNonEmptyArrayPar = <A, R, E, B>(
  f: (a: A) => XIO<R, E, B>,
): ((as: ReadonlyNonEmptyArray<A>) => XIO<R, E, ReadonlyNonEmptyArray<B>>) =>
  traverseReadonlyNonEmptyArrayWithIndexPar((_, a) => f(a))

export const traverseReadonlyArrayWithIndexPar =
  <A, R, E, B>(f: (index: number, a: A) => XIO<R, E, B>) =>
  (as: ReadonlyArray<A>): XIO<R, E, ReadonlyArray<B>> =>
    pipe(foreachWithIndexPar(as, f), map(V.toReadonlyArray))

export const traverseReadonlyArrayPar = <A, R, E, B>(
  f: (a: A) => XIO<R, E, B>,
): ((as: ReadonlyArray<A>) => XIO<R, E, ReadonlyArray<B>>) =>
  traverseReadonlyArrayWithIndexPar((_, a) => f(a))

export const traverseArrayPar = traverseReadonlyArrayPar

export const sequenceReadonlyArrayPar: <R, E, A>(
  xios: ReadonlyArray<XIO<R, E, A>>,
) => XIO<R, E, ReadonlyArray<A>> = traverseReadonlyArrayPar(identity)

export const sequenceArrayPar = sequenceReadonlyArrayPar

export const sequenceReadonlyArrayPartitioningFailuresPar = <R, E, A>(
  xios: ReadonlyArray<XIO<R, E, A>>,
): XIO<R, never, Separated<ReadonlyArray<E>, ReadonlyArray<A>>> =>
  pipe(xios, RA.map(either), sequenceArrayPar, map(RA.separate))

export const sequenceArrayPartitioningFailuresPar = sequenceReadonlyArrayPartitioningFailuresPar

// Internal

function foreachConcurrentUnbounded<R, E, A, B>(
  as: Iterable<A>,
  f: (i: number, a: A) => XIO<R, E, B>,
): XIO<R, E, V.Vector<B>> {
  return pipe(
    fromIO(() => [] as Array<B>),
    chain((array) =>
      pipe(
        foreachConcurrentUnboundedDiscard(zipWithIndex(as), ([i, a]) =>
          pipe(
            defer(() => f(i, a)),
            chain((b) =>
              fromIO(() => {
                array[i] = b
              }),
            ),
          ),
        ),
        map(() => array),
      ),
    ),
    map(V.from),
  )
}

function foreachConcurrentUnboundedDiscard<R, E, A>(
  as: Iterable<A>,
  f: (a: A) => XIO<R, E, void>,
): XIO<R, E, void> {
  return defer(() => {
    const arr = Array.from(as)
    const size = arr.length

    if (size === 0) {
      return unit
    }

    return uninterruptibleMask((restore) => {
      const future = F.unsafeMake<void, void>(FId.none)
      const ref = new AtomicNumber(0)

      return pipe(
        transplant((graft) =>
          foreach(as, (a) =>
            pipe(
              restore(defer(() => f(a))),
              matchCauseXIO(
                (cause) =>
                  pipe(
                    future,
                    F.fail(undefined),
                    chain(() => failCause(cause)),
                  ),
                () => {
                  if (ref.incrementAndGet() === size) {
                    F.unsafeDone<void, void>(unit)(future)
                  }
                  return unit
                },
              ),
              graft,
              forkDaemon,
            ),
          ),
        ),
        chain((fibers) =>
          pipe(
            restore(F.await(future)),
            matchCauseXIO(
              (cause) =>
                pipe(
                  foreachConcurrentUnbounded(fibers, (_, fiber) => Fi.interrupt(fiber)),
                  chain((exits) =>
                    pipe(
                      Ex.collectAllPar(exits),
                      O.match(
                        () => failCause(C.stripFailures(cause)),
                        (exit) =>
                          Ex.isFailure(exit)
                            ? failCause(C.both(C.stripFailures(cause), exit.cause))
                            : failCause(C.stripFailures(cause)),
                      ),
                    ),
                  ),
                ),
              () => foreachDiscard(fibers, Fi.inheritRefs),
            ),
          ),
        ),
      )
    })
  })
}

function foreachConcurrentBoundedDiscardWorker<R, E, A>(
  queue: Q.Queue<A>,
  f: (a: A) => XIO<R, E, void>,
): XIO<R, E, void> {
  return pipe(
    Q.poll(queue),
    chain(
      O.match(
        () => unit,
        flow(
          f,
          chain(() => foreachConcurrentBoundedDiscardWorker(queue, f)),
        ),
      ),
    ),
  )
}

function foreachConcurrentBoundedDiscard<R, E, A>(
  as: Iterable<A>,
  n: number,
  f: (a: A) => XIO<R, E, void>,
): XIO<R, E, void> {
  return defer(() => {
    const len = size(as)
    if (len === 0) {
      return unit
    }
    return pipe(
      Q.bounded<A>(len),
      chainFirst((queue) => pipe(queue, Q.offerAll(as))),
      chain((queue) =>
        foreachConcurrentUnboundedDiscard(
          RA.replicate(n, foreachConcurrentBoundedDiscardWorker(queue, f)),
          identity,
        ),
      ),
    )
  })
}

function foreachConcurrentBoundedWorker<R, E, A, B>(
  queue: Q.Queue<readonly [number, A]>,
  array: Array<B>,
  f: (i: number, a: A) => XIO<R, E, B>,
): XIO<R, E, void> {
  return pipe(
    Q.poll(queue),
    chain(
      O.match(
        () => unit,
        ([i, a]) =>
          pipe(
            f(i, a),
            chain((b) =>
              fromIO(() => {
                array[i] = b
              }),
            ),
            chain(() => foreachConcurrentBoundedWorker(queue, array, f)),
          ),
      ),
    ),
  )
}

function foreachConcurrentBounded<R, E, A, B>(
  as: Iterable<A>,
  n: number,
  f: (i: number, a: A) => XIO<R, E, B>,
): XIO<R, E, V.Vector<B>> {
  return defer(() => {
    const len = size(as)
    if (len === 0) {
      return succeed(V.empty())
    }
    return pipe(
      Do,
      bind("array", () => fromIO(() => new Array<B>(len))),
      bind("queue", () => Q.bounded<readonly [number, A]>(len)),
      chainFirst(({ queue }) => pipe(queue, Q.offerAll(zipWithIndex(as)))),
      chainFirst(({ array, queue }) =>
        foreachConcurrentUnboundedDiscard(
          RA.replicate(n, foreachConcurrentBoundedWorker(queue, array, f)),
          identity,
        ),
      ),
      map(({ array }) => V.from(array)),
    )
  })
}
