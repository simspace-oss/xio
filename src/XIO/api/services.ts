import type { Clock } from "../../Clock/definition"
import type { UIO, XIO } from "../definition"

import { pipe } from "fp-ts/lib/function"

import { ClockTag } from "../../Clock/definition"
import * as FR from "../../FiberRef/api"
import * as XIOEnv from "../../XIOEnv"
import { succeed } from "./core"

export const clockWith = <R, E, A>(f: (clock: Clock) => XIO<R, E, A>): XIO<R, E, A> =>
  pipe(
    XIOEnv.services,
    FR.getWith((services) => f(ClockTag.unsafeGet(services))),
  )

export const clock: UIO<Clock> = clockWith(succeed)
