import type { Fiber } from "../../Fiber"
import type { FiberScope } from "../../Fiber/internal/FiberScope"
import type { XIO } from "../definition"

import { pipe } from "fp-ts/lib/function"
import * as O from "fp-ts/Option"

import { interruptAs, join } from "../../Fiber/api"
import { globalScope } from "../../Fiber/internal/FiberScope"
import * as InterruptStatus from "../../InterruptStatus"
import { Fork, GetForkScope, OverrideForkScope } from "../definition"
import {
  checkInterruptible,
  defer,
  fiberId,
  gen,
  interruptible,
  interruptStatus,
  onInterrupt,
  uninterruptible,
  uninterruptibleMask,
} from "./core"

type Graft = <R, E, A>(xio: XIO<R, E, A>) => XIO<R, E, A>

export const transplant = <R, E, A>(f: (_: Graft) => XIO<R, E, A>): XIO<R, E, A> =>
  new GetForkScope((scope) => f(grafter(scope)))

export const forkDaemon = <R, E, A>(xio: XIO<R, E, A>): XIO<R, never, Fiber<E, A>> =>
  new Fork(xio, O.some(globalScope))

export const disconnect = <R, E, A>(xio: XIO<R, E, A>): XIO<R, E, A> =>
  uninterruptibleMask((restore) =>
    gen(function* (_) {
      const id = yield* _(fiberId)
      const fiber = yield* _(pipe(restore(xio), forkDaemon))
      return yield* _(
        pipe(fiber, join, restore, onInterrupt(pipe(fiber, interruptAs(id), forkDaemon))),
      )
    }),
  )

function grafter(scope: FiberScope): Graft {
  return (xio) => defer(() => new OverrideForkScope(xio, O.some(scope)))
}

type InterruptStatusForce = <R, E, A>(xio: XIO<R, E, A>) => XIO<R, E, A>

function force(flag: InterruptStatus.InterruptStatus) {
  return <R, E, A>(xio: XIO<R, E, A>): XIO<R, E, A> =>
    defer(() => {
      if (InterruptStatus.isUninterruptible(flag)) {
        return pipe(uninterruptible(xio), disconnect, interruptible)
      } else {
        return pipe(xio, interruptStatus(flag))
      }
    })
}

export const uninterruptibleMaskForce = <R, E, A>(
  f: (force: InterruptStatusForce) => XIO<R, E, A>,
): XIO<R, E, A> => checkInterruptible((flag) => f(force(flag)))
