import type { Cause } from "../../Cause"
import type { FiberDescriptor } from "../../Fiber/FiberDescriptor"
import type { FiberContext } from "../../Fiber/internal/FiberContext"
import type { FiberId } from "../../FiberId"
import type { FiberRefs } from "../../FiberRefs"
import type { _E, _R } from "../../internal/types"
import type { Has, Tag } from "../../Tag"
import type { UIO, XIO } from "../definition"
import type { IO } from "fp-ts/IO"
import type { IOEither } from "fp-ts/lib/IOEither"
import type { Refinement } from "fp-ts/lib/Refinement"
import type { Predicate } from "fp-ts/Predicate"
import type { Reader } from "fp-ts/Reader"
import type { ReaderTask } from "fp-ts/ReaderTask"
import type { ReaderTaskEither } from "fp-ts/ReaderTaskEither"
import type { ReadonlyNonEmptyArray } from "fp-ts/ReadonlyNonEmptyArray"
import type { Task } from "fp-ts/Task"
import type { TaskEither } from "fp-ts/TaskEither"
import type { TaskOption } from "fp-ts/TaskOption"

import * as V from "@simspace/collections/Vector"
import * as B from "fp-ts/boolean"
import * as E from "fp-ts/Either"
import { flow, identity, pipe, tuple, unsafeCoerce } from "fp-ts/function"
import * as O from "fp-ts/Option"
import * as RA from "fp-ts/ReadonlyArray"

import * as C from "../../Cause"
import * as Ex from "../../Exit"
import * as FId from "../../FiberId"
import * as InterruptStatus from "../../InterruptStatus"
import { Provide, Read } from "../definition"
import { Yield } from "../definition"
import {
  Async,
  Chain,
  Defer,
  Descriptor,
  Ensuring,
  Fail,
  FiberRefModifyAll,
  Fork,
  FromIO,
  GetInterruptStatus,
  Match,
  SetInterruptStatus,
  Succeed,
} from "../definition"

/**
 * Lifts a lazy value or synchronous side-effect into a pure `XIO` value.
 */
export const fromIO = <A>(effect: () => A): XIO<unknown, never, A> => new FromIO(effect)

/**
 * Lifts a lazy value or synchronous side-effect into a pure `XIO` value
 *
 * @note alias of {@link fromIO}
 */
export const rightIO = fromIO

/**
 * Lifts a pure value into an `XIO`
 */
export const succeed = <E = never, A = never>(value: A): XIO<unknown, E, A> => new Succeed(value)

/**
 * Lifts a pure value into an `XIO`
 *
 * @note alias of {@link succeed}
 */
export const right = succeed

/**
 * Lifts a pure value into an `XIO`
 *
 * @note alias of {@link succeed}
 */
export const of = succeed

/**
 * Lifts a lazy `Cause` or synchronous side-effect that returns a `Cause` into
 * the failure channel of an `XIO`. The resulting `XIO` will fail with the given `Cause`.
 */
export const failCauseIO = <E>(cause: () => C.Cause<E>): XIO<unknown, E, never> => new Fail(cause)

/**
 * Lifts a pure `Cause` into the failure channel of an `XIO`. The resulting `XIO` will fail
 * with the given `Cause`
 */
export const failCause = <E>(cause: C.Cause<E>): XIO<unknown, E, never> => new Fail(() => cause)

/**
 * Lifts a lazy value or synchronous side-effect into the failure channel of an `XIO`.
 * The resulting `XIO` will fail with the given value
 */
export const failIO = <E>(value: () => E): XIO<unknown, E, never> =>
  failCauseIO(() => C.fail(value()))

/**
 * Lifts a lazy value or synchronous side-effect into the failure channel of an `XIO`.
 * The resulting `XIO` will fail with the given value
 *
 * @note alias of {@link failIO}
 */
export const leftIO = failIO

/**
 * Lifts a pure value into the failure channel of an `XIO`. The resulting `XIO` will fail
 * with the given value
 */
export const fail = <E>(value: E): XIO<unknown, E, never> => failIO(() => value)

/**
 * Lifts a pure value into the failure channel of an `XIO`. The resulting `XIO` will fail
 * with the given value
 *
 * @note alias of {@link fail}
 */
export const left = fail

/**
 * Lifts an untyped failure into an `XIO`. The resulting `XIO` will fail with the
 * given untyped failure.
 */
export const haltIO = (defect: () => unknown): XIO<unknown, never, never> =>
  failCauseIO(() => C.halt(defect()))

/**
 * Lifts an untyped failure into an `XIO`. The resulting `XIO` will fail with the
 * given untyped failure.
 */
export const halt = (value: unknown): XIO<unknown, never, never> => failCause(C.halt(value))

/**
 * Returns a lazily constructed `XIO`, whose construction may itself require
 * effects. The construction of the effect must not throw exceptions.
 */
export const defer = <R, E, A>(make: () => XIO<R, E, A>): XIO<R, E, A> => new Defer(make)

/**
 * Returns an effect that yields to the runtime system, starting on a fresh
 * stack. Manual use of this method can improve fairness, at the cost of
 * overhead.
 */
export const yieldNow: UIO<void> = new Yield()

/**
 * Effectfully accesses the environment of the effect.
 */
export const asksXIO = <R0, R, E, A>(f: (environment: R0) => XIO<R, E, A>): XIO<R0 & R, E, A> =>
  new Read(f)

/**
 * Accesses the whole environment of the effect.
 */
export const ask = <R>(): XIO<R, never, R> => asksXIO((r: R) => succeed(r))

/**
 * Accesses the environment of the effect.
 */
export const asks = <R, A>(f: (environment: R) => A): XIO<R, never, A> =>
  asksXIO((r: R) => succeed(f(r)))

/**
 * Provides the effect with its required environment, which eliminates
 * its dependency on `R`.
 */
export const provide =
  <R>(environment: R) =>
  <E, A>(xio: XIO<R, E, A>): XIO<unknown, E, A> =>
    new Provide(xio, environment)

/**
 * Transforms the environment being provided to this effect with the specified
 * function.
 */
export const local =
  <R0, R>(f: (environment: R0) => R) =>
  <E, A>(xio: XIO<R, E, A>): XIO<R0, E, A> =>
    asksXIO((environment: R0) => pipe(xio, provide(f(environment))))

/**
 * Provides the effect with a portion of its environment. If the given environment contains
 * keys of the current environment, those keys in the current environment will be overwritten.
 */
export const providePartial = <R0>(
  environment: R0,
): (<R, E, A>(xio: XIO<R0 & R, E, A>) => XIO<R, E, A>) => local((r) => ({ ...r, ...environment }))

/**
 * Effectfully accesses a service in the environment
 */
export const asksServiceXIO = <S, R, E, A>(
  tag: Tag<S>,
  f: (service: S) => XIO<R, E, A>,
): XIO<R & Has<S>, E, A> => asksXIO((r: Has<S>) => f(tag.unsafeGet(r)))

/**
 * Accesses a service in the environment
 */
export const asksService = <S, A>(tag: Tag<S>, f: (service: S) => A): XIO<Has<S>, never, A> =>
  asksServiceXIO(tag, (service) => fromIO(() => f(service)))

/**
 * Accesses a service in the environment
 */
export const askService = <S>(tag: Tag<S>): XIO<Has<S>, never, S> => asksService(tag, identity)

/**
 * Provides an effect with a tagged service
 */
export const provideService = <S>(
  tag: Tag<S>,
  service: S,
): (<R, E, A>(xio: XIO<R & Has<S>, E, A>) => XIO<R, E, A>) => providePartial(tag.of(service))

/**
 * Lifts an `Either` into an `XIO`
 */
export const fromEither: <E, A>(either: E.Either<E, A>) => XIO<unknown, E, A> = E.matchW(
  fail,
  succeed,
)

/**
 * Lifts an `Option` into an `XIO`
 */
export const fromOption = <E>(onNone: () => E): (<A>(option: O.Option<A>) => XIO<unknown, E, A>) =>
  O.matchW(flow(onNone, fail), succeed)

/**
 * Lifts a `Reader` into an `XIO`
 *
 * @note alias of {@link asks}
 */
export const fromReader: <R, A>(ra: Reader<R, A>) => XIO<R, never, A> = asks

/**
 * Lifts a `Task` into an `XIO`
 */
export const fromTask = <A>(task: Task<A>): XIO<unknown, never, A> =>
  async((cb) => {
    task().then((value) => cb(succeed(value)))
  })

/**
 * Lifts a `Task` into an `XIO`
 *
 * @note alias of {@link fromTask}
 */
export const rightTask = fromTask

/**
 * Lifts a `Task` into the failure channel of an `XIO`
 */
export const failTask = <E>(task: Task<E>): XIO<unknown, E, never> =>
  async((cb) => {
    task().then((value) => cb(fail(value)))
  })

/**
 * Lifts a `Task` into the failure channel of an `XIO`
 *
 * @note alias of {@link failTask}
 */
export const leftTask = failTask

/**
 * Lifts a `TaskOption` into the failure channel of an `XIO`
 */
export const fromTaskOption: <A>(taskOption: TaskOption<A>) => XIO<unknown, never, O.Option<A>> =
  fromTask

/**
 * Lifts a `TaskEither` into an `XIO`
 */
export const fromTaskEither = <E, A>(taskEither: TaskEither<E, A>): XIO<unknown, E, A> =>
  async((cb) => {
    taskEither().then(flow(E.matchW(fail, succeed), cb))
  })

/**
 * Lifts a `ReaderTask` into an `XIO`. The return effect will succeed with the value of the
 * `ReaderTask`
 */
export const fromReaderTask = <R, A>(readerTask: ReaderTask<R, A>): XIO<R, never, A> =>
  asksXIO((r: R) => defer(() => fromTask(readerTask(r))))

/**
 * Lifts a `ReaderTask` into an `XIO`. The return effect will succeed with the value of the
 * `ReaderTask`
 *
 * @note alias of {@link fromReaderTask}
 */
export const rightReaderTask = fromReaderTask

/**
 * Lifts a `ReaderTask` into an `XIO`. The return effect will fail with the value of the
 * `ReaderTask`
 */
export const failReaderTask = <R, E>(readerTask: ReaderTask<R, E>): XIO<R, E, never> =>
  asksXIO((r: R) => defer(() => failTask(readerTask(r))))

/**
 * Lifts a `ReaderTask` into an `XIO`. The return effect will fail with the value of the
 * `ReaderTask`
 *
 * @note alias of {@link failReaderTask}
 */
export const leftReaderTask = failReaderTask

/**
 * Lifts a `ReaderTaskEither` into an `XIO`.
 */
export const fromReaderTaskEither = <R, E, A>(effect: ReaderTaskEither<R, E, A>): XIO<R, E, A> =>
  asksXIO((r: R) => defer(() => fromTaskEither(effect(r))))

/**
 * Lifts a lazy `Promise` into an `XIO`, mapping rejections into typed failures.
 */
export const fromPromiseCatch = <E, A>(
  promise: () => Promise<A>,
  onRejected: (reason: unknown) => E,
): XIO<unknown, E, A> =>
  async((cb) => {
    promise().then(
      (value) => cb(succeed(value)),
      (reason) => cb(fail(onRejected(reason))),
    )
  })

/**
 * Lifts a lazy `Promise` into an `XIO`, mapping rejections into unchecked defects.
 */
export const fromPromiseHalt = <A>(promise: () => Promise<A>): XIO<unknown, never, A> =>
  async((cb) => {
    promise().then(
      (value) => cb(succeed(value)),
      (reason) => cb(halt(reason)),
    )
  })

export function fromPredicate<E, A, B extends A>(
  refinement: Refinement<A, B>,
  onFalse: (a: A) => E,
): <R>(a: A) => XIO<R, E, B>
export function fromPredicate<E, A>(
  predicate: Predicate<A>,
  onFalse: (a: A) => E,
): <R>(a: A) => XIO<R, E, A>
export function fromPredicate<E, A>(
  predicate: Predicate<A>,
  onFalse: (a: A) => E,
): <R>(a: A) => XIO<R, E, A> {
  return (a) => (predicate(a) ? succeed(a) : fail(onFalse(a)))
}

/**
 * Constructs an `XIO` based on information about the current fiber, such as
 * its identity.
 */
export const descriptorWith = <R, E, A>(
  f: (descriptor: FiberDescriptor) => XIO<R, E, A>,
): XIO<R, E, A> => new Descriptor(f)

/**
 * Lifts an `Exit` into an `XIO`. The resulting effect will fail if the `Exit` is a
 * `Failure`, or succeed if the `Exit` is `Success`
 */
export const doneIO = <E, A>(exit: () => Ex.Exit<E, A>): XIO<unknown, E, A> =>
  defer(() => pipe(exit(), Ex.match(failCause, succeed)))

/**
 * Lifts an `Exit` into an `XIO`. The resulting effect will fail if the `Exit` is a
 * `Failure`, or succeed if the `Exit` is `Success`
 */
export const done: <E, A>(exit: Ex.Exit<E, A>) => XIO<unknown, E, A> = Ex.match(failCause, succeed)

/**
 * Lifts an `Exit` into an `XIO`. The resulting effect will fail if the `Exit` is a
 * `Failure`, or succeed if the `Exit` is `Success`
 *
 * @note alias of {@link done}
 */
export const fromExit = done

/**
 * Returns the `FiberId` of the fiber that executes this `XIO`
 */
export const fiberId: UIO<FiberId> = descriptorWith((descriptor) => succeed(descriptor.id))

/**
 * An `XIO` that succeeds with `void`
 */
export const unit: XIO<unknown, never, void> = succeed(undefined)

/**
 * An `XIO` that succeeds with `void`
 *
 * @note alias of {@link unit}
 */
export const noOpXIO: XIO<unknown, never, void> = unit

/**
 * Matches over the failure or success of an `XIO`. The effect will continue
 * with either the effect returned by `onFailure` or `onSuccess`
 */
export const matchCauseXIO =
  <E, A, R1, E1, B, R2, E2, C>(
    onFailure: (cause: Cause<E>) => XIO<R1, E1, B>,
    onSuccess: (a: A) => XIO<R2, E2, C>,
  ) =>
  <R>(xio: XIO<R, E, A>): XIO<R & R1 & R2, E1 | E2, B | C> =>
    new Match(xio, onFailure, onSuccess)

/**
 * Matches over the failure or success of an `XIO`. The effect will continue
 * with either the effect returned by `onFailure` or `onSuccess`
 */
export const matchXIO = <E, A, R1, E1, B, R2, E2, C>(
  onFailure: (e: E) => XIO<R1, E1, B>,
  onSuccess: (a: A) => XIO<R2, E2, C>,
): (<R>(xio: XIO<R, E, A>) => XIO<R & R1 & R2, E1 | E2, B | C>) =>
  matchCauseXIO(flow(C.failureOrCause, E.match(onFailure, failCause)), onSuccess)

/**
 * Matches over the failure or success of an `XIO`. The effect will continue
 * with either the effect returned by `onFailure` or `onSuccess`
 *
 * @note alias of {@link matchXIO}
 */
export const matchE = matchXIO

/**
 * Matches over the failure or success of an `XIO`, returning an effect that
 * will not fail, but will succeed with the result of `onFailure` or `onSuccess`
 */
export const match = <E, A, B, C>(
  onFailure: (e: E) => B,
  onSuccess: (a: A) => C,
): (<R>(xio: XIO<R, E, A>) => XIO<R, never, B | C>) =>
  matchXIO(flow(onFailure, succeed), flow(onSuccess, succeed))

/**
 * Recovers from all errors
 */
export const catchAllCause = <E, R1, E1, B>(
  f: (cause: Cause<E>) => XIO<R1, E1, B>,
): (<R, A>(xio: XIO<R, E, A>) => XIO<R & R1, E1, A | B>) => matchCauseXIO(f, succeed)

/**
 * Recovers from all errors
 *
 * @note alias of {@link catchAllCause}
 */
export const orElseCause = catchAllCause

/**
 * Recovers from all typed failures
 */
export const catchAll = <E, R1, E1, B>(
  f: (e: E) => XIO<R1, E1, B>,
): (<R, A>(xio: XIO<R, E, A>) => XIO<R & R1, E1, A | B>) => matchXIO(f, succeed)

/**
 * Recovers from all typed failures
 *
 * @note alias of {@link orElse}
 */
export const orElse = catchAll

/**
 * Returns an effect that effectually "peeks" at the defect of this effect.
 */
export const tapDefect = <R1, E1, B>(
  f: (defect: Cause<never>) => XIO<R1, E1, B>,
): (<R, E, A>(xio: XIO<R, E, A>) => XIO<R & R1, E | E1, A>) =>
  matchCauseXIO(
    (c) =>
      pipe(
        C.stripFailures(c),
        f,
        chain(() => failCause(c)),
      ),
    succeed,
  )

/**
 * Returns an effect that effectfully "peeks" at the failure of this effect.
 */
export const tapError = <E, R1, E1, B>(
  f: (e: E) => XIO<R1, E1, B>,
): (<R, A>(xio: XIO<R, E, A>) => XIO<R & R1, E | E1, A>) =>
  matchCauseXIO(
    (cause) =>
      pipe(
        C.failureOrCause(cause),
        E.match(
          (e) =>
            pipe(
              f(e),
              chain(() => failCause(cause)),
            ),
          () => failCause(cause),
        ),
      ),
    succeed,
  )

/**
 * Returns an effect that effectfully "peeks" at the failure of this effect.
 *
 * @note alias of {@link tapError}
 */
export const orElseFirst = tapError

/**
 * Returns an effect that effectfully "peeks" at the failure or success of
 * this effect.
 */
export const tapBoth = <E, A, R1, E1, B, R2, E2, C>(
  f: (e: E) => XIO<R1, E1, B>,
  g: (a: A) => XIO<R2, E2, C>,
): (<R>(ma: XIO<R, E, A>) => XIO<R & R1 & R2, E | E1 | E2, A>) =>
  matchCauseXIO(
    (c) =>
      pipe(
        C.failureOrCause(c),
        E.match(
          flow(
            f,
            chain(() => failCause(c)),
          ),
          () => failCause(c),
        ),
      ),
    (a) => pipe(g(a), as(a)),
  )

/**
 * Returns an effect that effectfully "peeks" at the failure or success of
 * this effect.
 *
 * @note alias of {@link tapBoth}
 */
export const bichainFirst = tapBoth

export const alt = <R1, E1, B>(
  that: () => XIO<R1, E1, B>,
): (<R, E, A>(xio: XIO<R, E, A>) => XIO<R & R1, E1, A | B>) => matchXIO(() => that(), succeed)

/**
 * Returns an effect that semantically runs the effect on a fiber, producing
 * an `Exit` for the completion value of the fiber.
 */
export const result: <R, E, A>(xio: XIO<R, E, A>) => XIO<R, never, Ex.Exit<E, A>> = matchCauseXIO(
  flow(Ex.failCause, succeed),
  flow(Ex.succeed, succeed),
)

/**
 * Returns an `XIO` that will execute an effect, then pass its value to the continuation function `f`
 */
export const chain =
  <A, R1, E1, B>(f: (a: A) => XIO<R1, E1, B>) =>
  <R, E>(ma: XIO<R, E, A>): XIO<R & R1, E | E1, B> =>
    new Chain(ma, f)

/**
 * Lifts a `ReaderIO` into an `XIO`
 */
export const fromReaderIO: <R, A>(rio: Reader<R, IO<A>>) => XIO<R, never, A> = flow(
  fromReader,
  chain(fromIO),
)

/**
 * Lifts a `ReaderIO` into an `XIO`
 *
 * @note alias of {@link fromReaderIO}
 */
export const rightReaderIO = fromReaderIO

/**
 * Lifts an `IOEither` into an `XIO`
 */
export const fromIOEither: <E, A>(ioEither: IOEither<E, A>) => XIO<unknown, E, A> = flow(
  fromIO,
  chain(fromEither),
)

/**
 * Lifts a `ReaderIOEither` into an `XIO`
 */
export const fromReaderIOEither: <R, E, A>(effect: Reader<R, IOEither<E, A>>) => XIO<R, E, A> =
  flow(fromReaderIO, chain(fromEither))

export const chainEither = <A, E1, B>(
  f: (a: A) => E.Either<E1, B>,
): (<R, E>(xio: XIO<R, E, A>) => XIO<R, E | E1, B>) => chain(flow(f, fromEither))

/**
 * @note alias of {@link chainEither}
 */
export const chainEitherK = chainEither

export const chainFirstEither = <A, E1, B>(
  f: (a: A) => E.Either<E1, B>,
): (<R, E>(xio: XIO<R, E, A>) => XIO<R, E | E1, A>) => chainFirst(flow(f, fromEither))

/**
 * @note alias of {@link chainFirstEither}
 */
export const chainFirstEitherK = chainFirstEither

export const chainOption = <A, E1, B>(
  f: (a: A) => O.Option<B>,
  onNone: () => E1,
): (<R, E>(xio: XIO<R, E, A>) => XIO<R, E | E1, B>) => chain(flow(f, fromOption(onNone)))

/**
 * @note alias of {@link chainOption}
 */
export const chainOptionK = chainOption

export const chainFirstOption = <A, E1, B>(
  f: (a: A) => O.Option<B>,
  onNone: () => E1,
): (<R, E>(xio: XIO<R, E, A>) => XIO<R, E | E1, A>) => chainFirst(flow(f, fromOption(onNone)))

/**
 * @note alias of {@link chainFirstOption}
 */
export const chainFirstOptionK = chainFirstOption

export const chainExit = <A, E1, B>(
  f: (a: A) => Ex.Exit<E1, B>,
): (<R, E>(xio: XIO<R, E, A>) => XIO<R, E | E1, B>) => chain((a) => fromExit(f(a)))

/**
 * Lifts a `Reader` into an `XIO`
 */
export const chainReader = <A, R1, B>(
  f: (a: A) => Reader<R1, B>,
): (<R, E>(xio: XIO<R, E, A>) => XIO<R & R1, E, B>) => chain((a) => fromReader(f(a)))

/**
 * @note alias of {@link chainReader}
 */
export const chainReaderK = chainReader

export const chainIO = <A, B>(f: (a: A) => IO<B>): (<R, E>(xio: XIO<R, E, A>) => XIO<R, E, B>) =>
  chain((a) => fromIO(f(a)))

/**
 * @note alias of {@link chainIO}
 */
export const chainIOK = chainIO

export const chainFirstIO = <A, B>(
  f: (a: A) => IO<B>,
): (<R, E>(xio: XIO<R, E, A>) => XIO<R, E, A>) => chainFirst((a) => fromIO(f(a)))

/**
 * @note alias of {@link chainFirstIO}
 */
export const chainFirstIOK = chainFirstIO

export const chainReaderIO = <A, R1, B>(
  f: (a: A) => Reader<R1, IO<B>>,
): (<R, E>(xio: XIO<R, E, A>) => XIO<R & R1, E, B>) => chain((a) => fromReaderIO(f(a)))

/**
 * @note alias of {@link chainReaderIO}
 */
export const chainReaderIOK = chainReaderIO

export const chainFirstReaderIO = <A, R1, B>(
  f: (a: A) => Reader<R1, IO<B>>,
): (<R, E>(xio: XIO<R, E, A>) => XIO<R & R1, E, A>) => chainFirst((a) => fromReaderIO(f(a)))

/**
 * @note alias of {@link chainFirstReaderIO}
 */
export const chainFirstReaderIOK = chainFirstReaderIO

export const chainIOEither = <A, E1, B>(
  f: (a: A) => IOEither<E1, B>,
): (<R, E>(xio: XIO<R, E, A>) => XIO<R, E | E1, B>) => chain((a) => fromIOEither(f(a)))

/**
 * @note alias of {@link chainIOEither}
 */
export const chainIOEitherK = chainIOEither

export const chainFirstIOEither = <A, E1, B>(
  f: (a: A) => IOEither<E1, B>,
): (<R, E>(xio: XIO<R, E, A>) => XIO<R, E | E1, A>) => chainFirst((a) => fromIOEither(f(a)))

/**
 * @note alias of {@link chainFirstIOEither}
 */
export const chainFirstIOEitherK = chainFirstIOEither

export const chainReaderIOEither = <A, R1, E1, B>(
  f: (a: A) => Reader<R1, IOEither<E1, B>>,
): (<R, E>(xio: XIO<R, E, A>) => XIO<R & R1, E | E1, B>) => chain((a) => fromReaderIOEither(f(a)))

/**
 * @note alias of {@link chainReaderIOEither}
 */
export const chainReaderIOEitherK = chainReaderIOEither

export const chainFirstReaderIOEither = <A, R1, E1, B>(
  f: (a: A) => Reader<R1, IOEither<E1, B>>,
): (<R, E>(xio: XIO<R, E, A>) => XIO<R & R1, E | E1, A>) =>
  chainFirst((a) => fromReaderIOEither(f(a)))

/**
 * @note alias of {@link chainFirstReaderIOEither}
 */
export const chainFirstReaderIOEitherK = chainFirstReaderIOEither

export const chainTask = <A, B>(
  f: (a: A) => Task<B>,
): (<R, E>(xio: XIO<R, E, A>) => XIO<R, E, B>) => chain((a) => fromTask(f(a)))

/**
 * @note alias of {@link chainTask}
 */
export const chainTaskK = chainTask

export const chainFirstTask = <A, B>(
  f: (a: A) => Task<B>,
): (<R, E>(xio: XIO<R, E, A>) => XIO<R, E, A>) => chainFirst((a) => fromTask(f(a)))

/**
 * @note alias of {@link chainFirstTask}
 */
export const chainFirstTaskK = chainFirstTask

export const chainTaskOption: <A, B>(
  f: (a: A) => TaskOption<B>,
) => <R, E>(xio: XIO<R, E, A>) => XIO<R, E, O.Option<B>> = chainTask

/**
 * @note alias of {@link chainTaskOption}
 */
export const chainTaskOptionK = chainTaskOption

export const chainFirstTaskOption: <A, B>(
  f: (a: A) => TaskOption<B>,
) => <R, E>(xio: XIO<R, E, A>) => XIO<R, E, A> = chainFirstTask

/**
 * @note alias of {@link chainFirstTaskOption}
 */
export const chainFirstTaskOptionK = chainFirstTaskOption

export const chainTaskEither = <A, E1, B>(
  f: (a: A) => TaskEither<E1, B>,
): (<R, E>(xio: XIO<R, E, A>) => XIO<R, E | E1, B>) => chain((a) => fromTaskEither(f(a)))

/**
 * @note alias of {@link chainTaskEither}
 */
export const chainTaskEitherK = chainTaskEither

export const chainFirstTaskEither = <A, E1, B>(
  f: (a: A) => TaskEither<E1, B>,
): (<R, E>(xio: XIO<R, E, A>) => XIO<R, E | E1, A>) => chainFirst((a) => fromTaskEither(f(a)))

/**
 * @note alias of {@link chainFirstTaskEither}
 */
export const chainFirstTaskEitherK = chainFirstTaskEither

export const chainReaderTask = <A, R1, B>(
  f: (a: A) => ReaderTask<R1, B>,
): (<R, E>(xio: XIO<R, E, A>) => XIO<R & R1, E, B>) => chain((a) => fromReaderTask(f(a)))

/**
 * @note alias of {@link chainReaderTask}
 */
export const chainReaderTaskK = chainReaderTask

export const chainFirstReaderTask = <A, R1, B>(
  f: (a: A) => ReaderTask<R1, B>,
): (<R, E>(xio: XIO<R, E, A>) => XIO<R & R1, E, A>) => chainFirst((a) => fromReaderTask(f(a)))

/**
 * @note alias of {@link chainFirstReaderTask}
 */
export const chainFirstReaderTaskK = chainFirstReaderTask

export const chainReaderTaskEither = <A, R1, E1, B>(
  f: (a: A) => ReaderTaskEither<R1, E1, B>,
): (<R, E>(xio: XIO<R, E, A>) => XIO<R & R1, E | E1, B>) => chain((a) => fromReaderTaskEither(f(a)))

/**
 * @note alias of {@link chainReaderTaskEither}
 */
export const chainReaderTaskEitherK = chainReaderTaskEither

export const chainFirstReaderTaskEither = <A, R1, E1, B>(
  f: (a: A) => ReaderTaskEither<R1, E1, B>,
): (<R, E>(xio: XIO<R, E, A>) => XIO<R & R1, E | E1, A>) =>
  chainFirst((a) => fromReaderTaskEither(f(a)))

/**
 * @note alias of {@link chainFirstReaderTaskEither}
 */
export const chainFirstReaderTaskEitherK = chainFirstReaderTaskEither

export const bichainFirstIO = <E, A, B, C>(
  onFailure: (e: E) => IO<B>,
  onSuccess: (a: A) => IO<C>,
): (<R>(xio: XIO<R, E, A>) => XIO<R, E, A>) =>
  bichainFirst(flow(onFailure, fromIO), flow(onSuccess, fromIO))

export const bichainFirstIOK = bichainFirstIO

export const bichainFirstReaderIO = <E, A, R1, B, R2, C>(
  onFailure: (e: E) => Reader<R1, IO<B>>,
  onSuccess: (a: A) => Reader<R2, IO<C>>,
): (<R>(xio: XIO<R, E, A>) => XIO<R & R1 & R2, E, A>) =>
  bichainFirst(flow(onFailure, fromReaderIO), flow(onSuccess, fromReaderIO))

export const bichainFirstReaderIOK = bichainFirstReaderIO

export const bichainFirstIOEither = <E, A, E1, B, E2, C>(
  onFailure: (e: E) => IOEither<E1, B>,
  onSuccess: (a: A) => IOEither<E2, C>,
): (<R>(xio: XIO<R, E, A>) => XIO<R, E | E1 | E2, A>) =>
  bichainFirst(flow(onFailure, fromIOEither), flow(onSuccess, fromIOEither))

export const bichainFirstIOEitherK = bichainFirstIOEither

export const bichainFirstReaderIOEither = <E, A, R1, E1, B, R2, E2, C>(
  onFailure: (e: E) => Reader<R1, IOEither<E1, B>>,
  onSuccess: (a: A) => Reader<R2, IOEither<E2, C>>,
): (<R>(xio: XIO<R, E, A>) => XIO<R & R1 & R2, E | E1 | E2, A>) =>
  bichainFirst(flow(onFailure, fromReaderIOEither), flow(onSuccess, fromReaderIOEither))

export const bichainFirstReaderIOEitherK = bichainFirstReaderIOEither

export const bichainFirstTask = <E, A, B, C>(
  onFailure: (e: E) => Task<B>,
  onSuccess: (a: A) => Task<C>,
): (<R>(xio: XIO<R, E, A>) => XIO<R, E, A>) =>
  bichainFirst(flow(onFailure, fromTask), flow(onSuccess, fromTask))

export const bichainFirstTaskK = bichainFirstTask

export const bichainFirstReaderTask = <E, A, R1, B, R2, C>(
  onFailure: (e: E) => ReaderTask<R1, B>,
  onSuccess: (a: A) => ReaderTask<R2, C>,
): (<R>(xio: XIO<R, E, A>) => XIO<R & R1 & R2, E, A>) =>
  bichainFirst(flow(onFailure, fromReaderTask), flow(onSuccess, fromReaderTask))

export const bichainFirstReaderTaskK = bichainFirstReaderTask

export const bichainFirstTaskEither = <E, A, E1, B, E2, C>(
  onFailure: (e: E) => TaskEither<E1, B>,
  onSuccess: (a: A) => TaskEither<E2, C>,
): (<R>(xio: XIO<R, E, A>) => XIO<R, E | E1 | E2, A>) =>
  bichainFirst(flow(onFailure, fromTaskEither), flow(onSuccess, fromTaskEither))

export const bichainFirstTaskEitherK = bichainFirstTaskEither

export const bichainFirstReaderTaskEither = <E, A, R1, E1, B, R2, E2, C>(
  onFailure: (e: E) => ReaderTaskEither<R1, E1, B>,
  onSuccess: (a: A) => ReaderTaskEither<R2, E2, C>,
): (<R>(xio: XIO<R, E, A>) => XIO<R & R1 & R2, E | E1 | E2, A>) =>
  bichainFirst(flow(onFailure, fromReaderTaskEither), flow(onSuccess, fromReaderTaskEither))

export const bichainFirstReaderTaskEitherK = bichainFirstReaderTaskEither

/**
 * Returns an `XIO` that will have its success transformed by `f`
 */
export const map = <A, B>(f: (a: A) => B): (<R, E>(fa: XIO<R, E, A>) => XIO<R, E, B>) =>
  chain((a) => succeed(f(a)))

/**
 * Maps the success value of this effect to the specified constant value.
 */
export const as = <B>(b: B): (<R, E, A>(fa: XIO<R, E, A>) => XIO<R, E, B>) => map(() => b)

/**
 * Maps the success value of this effect to the specified constant value.
 *
 * @note alias of {@link as}
 */
export const voidWith = as

/**
 * Returns the effect resulting from mapping the success of this effect to
 * `void`.
 */
export const asUnit: <R, E, A>(xio: XIO<R, E, A>) => XIO<R, E, void> = map(() => void 0)

/**
 * Sequentially zips this effect with the specified effect using the specified
 * combiner function.
 */
export const zipWith = <A, R1, E1, B, C>(
  fb: XIO<R1, E1, B>,
  f: (a: A, b: B) => C,
): (<R, E>(fa: XIO<R, E, A>) => XIO<R & R1, E | E1, C>) => chain((a) => map((b: B) => f(a, b))(fb))

/**
 * Sequentially zips two effects, combining the results into a tuple.
 */
export const zip = <R1, E1, B>(
  fb: XIO<R1, E1, B>,
): (<R, E, A>(fa: XIO<R, E, A>) => XIO<R & R1, E | E1, readonly [A, B]>) => zipWith(fb, tuple)

export const ap = <R1, E1, A>(
  fa: XIO<R1, E1, A>,
): (<R, E, B>(fab: XIO<R, E, (a: A) => B>) => XIO<R & R1, E | E1, B>) => zipWith(fa, (f, a) => f(a))

/**
 * Sequences the specified effect after the first, but ignores the value
 * produced by the second effect.
 */
export const apFirst = <R1, E1, B>(
  fb: XIO<R1, E1, B>,
): (<R, E, A>(fa: XIO<R, E, A>) => XIO<R & R1, E | E1, A>) => chain((a) => pipe(fb, as(a)))

/**
 * Sequences the specified effect after the first, but ignores the value
 * produced by the first effect.
 */
export const apSecond = <R1, E1, B>(
  fb: XIO<R1, E1, B>,
): (<R, E, A>(fa: XIO<R, E, A>) => XIO<R & R1, E | E1, B>) => chain(() => fb)

/**
 * Returns an `XIO` that executes a synchronous side-effect, catches any thrown errors,
 * and translates them into typed failures with the specified function `onError`
 */
export const tryCatch = <E, A>(f: () => A, onError: (e: unknown) => E): XIO<unknown, E, A> =>
  defer(() => {
    try {
      return succeed(f())
    } catch (e) {
      return fail(onError(e))
    }
  })

/**
 * Returns an `XIO` that will execute an effect, then pass its value to the
 * continuation function `f`. The resulting `XIO` will succeed with the original
 * effect's value, or fail with either failure that occurs.
 */
export const chainFirst = <A, R1, E1, B>(
  f: (a: A) => XIO<R1, E1, B>,
): (<R, E>(ma: XIO<R, E, A>) => XIO<R & R1, E | E1, A>) =>
  chain((a) =>
    pipe(
      f(a),
      map(() => a),
    ),
  )

/**
 * Returns an `XIO` that first executes the outer effect, and then executes
 * the inner effect, returning the value from the inner effect
 */
export const flatten: <R, E, R1, E1, A>(mma: XIO<R, E, XIO<R1, E1, A>>) => XIO<R & R1, E | E1, A> =
  chain(identity)

/**
 * Returns an effect with its full cause of failure mapped using the specified
 * function. This can be used to transform errors while preserving the
 * original structure of `Cause`.
 */
export const mapErrorCause = <E, E1>(
  f: (cause: C.Cause<E>) => C.Cause<E1>,
): (<R, A>(xio: XIO<R, E, A>) => XIO<R, E1, A>) => matchCauseXIO(flow(f, failCause), succeed)

/**
 * Returns an effect with its error channel mapped using the specified
 * function. This can be used to lift a "smaller" error into a "larger" error.
 */
export const mapError = <E, E1>(f: (e: E) => E1): (<R, A>(xio: XIO<R, E, A>) => XIO<R, E1, A>) =>
  matchCauseXIO(
    (c) =>
      pipe(
        C.failureOrCause(c),
        E.matchW((e) => fail(f(e)), failCause),
      ),
    succeed,
  )

/**
 * Returns an effect with its error channel mapped using the specified
 * function. This can be used to lift a "smaller" error into a "larger" error.
 *
 * @note alias of {@link mapError}
 */
export const mapLeft = mapError

/**
 * Returns an effect whose failure and success channels have been mapped by
 * the specified pair of functions, `f` and `g`.
 */
export const bimap = <E, A, E1, B>(
  f: (e: E) => E1,
  g: (a: A) => B,
): (<R>(xio: XIO<R, E, A>) => XIO<R, E1, B>) => matchXIO(flow(f, fail), flow(g, succeed))

/**
 * Lifts an asynchronous side-effect into an `XIO`. The side-effect has
 * the option of returning the value synchronously, which is useful in cases
 * where it cannot be determined if the effect is synchronous or asynchronous
 * until the side-effect is actually executed. The effect also has the option
 * of returning a canceler, which will be used by the runtime to cancel the
 * asynchronous effect if the fiber executing the effect is interrupted.
 *
 * If the register function returns a value synchronously, then the callback
 * function `(cont: XIO<R, E, A>) => void` must not be called. Otherwise the callback
 * function must be called at most once.
 */
export const asyncInterrupt = <R, E, A>(
  register: (k: (cont: XIO<R, E, A>) => void) => E.Either<XIO<R, never, void>, XIO<R, E, A>>,
  blockingOn: FId.FiberId = FId.none,
): XIO<R, E, A> => new Async(register, blockingOn)

/**
 * Imports an asynchronous effect into a pure `XIO` value, possibly returning
 * the value synchronously.
 *
 * If the register function returns a value synchronously, then the callback
 * function `(cont: XIO<R, E, A>) => void` must not be called. Otherwise the callback
 * function must be called at most once.
 */
export const asyncOption = <R, E, A>(
  register: (k: (cont: XIO<R, E, A>) => void) => O.Option<XIO<R, E, A>>,
  blockingOn: FId.FiberId = FId.none,
): XIO<R, E, A> =>
  asyncInterrupt(
    flow(
      register,
      O.match(() => E.left(unit), E.right),
    ),
    blockingOn,
  )

/**
 * Imports an asynchronous side-effect into a pure `XIO` value.
 *
 * The callback function `(cont: XIO<R, E, A>) => void` must be called at most once.
 */
export const async = <R, E, A>(
  register: (k: (cont: XIO<R, E, A>) => void) => void,
  blockingOn: FId.FiberId = FId.none,
): XIO<R, E, A> =>
  asyncOption((k) => {
    register(k)
    return O.none
  }, blockingOn)

/**
 * Returns an effect that forks this `XIO` into its own separate fiber,
 * returning the fiber immediately, without waiting for it to begin executing
 * the effect.
 *
 * You can use the `fork` method whenever you want to execute an effect in a
 * new fiber, concurrently and without "blocking" the fiber executing other
 * effects. Using fibers directly can be tricky, so instead of using this method
 * directly, consider other higher-level methods, such as `raceWith`,
 * `zipPar`, and so forth.
 *
 * The fiber returned by this method has methods to interrupt the fiber and to
 * wait for it to finish executing the effect. See `Fiber` for more
 * information.
 *
 * Whenever you use this method to launch a new fiber, the new fiber is
 * attached to the parent fiber's scope. This means when the parent fiber
 * terminates, the child fiber will be terminated as well, ensuring that no
 * fibers leak. This behavior is called "auto supervision", and if this
 * behavior is not desired, you may use `forkDaemon` or `forkIn`.
 */
export const fork = <R, E, A>(xio: XIO<R, E, A>): XIO<R, never, FiberContext<E, A>> =>
  new Fork(xio, O.none)

/**
 * Applies the function `f` to each element of the `Iterable<A>` and runs
 * produced effects sequentially.
 */
export const foreachWithIndexDiscard = <A, R, E, B>(
  as: Iterable<A>,
  f: (i: number, a: A) => XIO<R, E, B>,
): XIO<R, E, void> => defer(() => foreachWithIndexDiscardLoop(as[Symbol.iterator](), f))

/**
 * Applies the function `f` to each element of the `Iterable<A>` and runs
 * produced effects sequentially.
 */
export const foreachDiscard = <A, R, E, B>(
  as: Iterable<A>,
  f: (a: A) => XIO<R, E, B>,
): XIO<R, E, void> => foreachWithIndexDiscard(as, (_, a) => f(a))

/**
 * Applies the function `f` to each element of the `Iterable<A>` and returns
 * the results in an immutable `Vector`.
 *
 * For a concurrent version of this function, see `foreachC`. If you do not need
 * the results, see `foreachDiscard` for a more efficient implementation.
 */
export const foreachWithIndex = <A, R, E, B>(
  as: Iterable<A>,
  f: (i: number, a: A) => XIO<R, E, B>,
): XIO<R, E, V.Vector<B>> =>
  defer(() => {
    let acc: V.Vector<B> = V.empty()
    return pipe(
      foreachWithIndexDiscard(as, (i, a) =>
        pipe(
          f(i, a),
          chain((b) => {
            acc = V.append(b)(acc)
            return unit
          }),
        ),
      ),
      map(() => acc),
    )
  })

/**
 * Applies the function `f` to each element of the `Iterable<A>` and returns
 * the results in an immutable `Vector`.
 *
 * For a concurrent version of this function, see `foreachC`. If you do not need
 * the results, see `foreachDiscard` for a more efficient implementation.
 */
export const foreach = <A, R, E, B>(
  as: Iterable<A>,
  f: (a: A) => XIO<R, E, B>,
): XIO<R, E, V.Vector<B>> => foreachWithIndex(as, (_, a) => f(a))

export const traverseReadonlyNonEmptyArrayWithIndex =
  <A, R, E, B>(f: (index: number, a: A) => XIO<R, E, B>) =>
  (as: ReadonlyNonEmptyArray<A>): XIO<R, E, ReadonlyNonEmptyArray<B>> =>
    defer(() => {
      const acc: Array<B> = Array(as.length)
      return pipe(
        foreachWithIndexDiscard(as, (i, a) =>
          pipe(
            f(i, a),
            chain((b) => {
              acc[i] = b
              return unit
            }),
          ),
        ),
        map(() => acc as unknown as ReadonlyNonEmptyArray<B>),
      )
    })

export const traverseReadonlyArrayWithIndex =
  <A, R, E, B>(f: (index: number, a: A) => XIO<R, E, B>) =>
  (as: ReadonlyArray<A>): XIO<R, E, ReadonlyArray<B>> =>
    RA.isNonEmpty(as) ? traverseReadonlyNonEmptyArrayWithIndex(f)(as) : succeed([])

export const traverseArray = <A, R, E, B>(
  f: (a: A) => XIO<R, E, B>,
): ((as: ReadonlyArray<A>) => XIO<R, E, ReadonlyArray<B>>) =>
  traverseReadonlyArrayWithIndex((_, a) => f(a))

export const traverseArrayCollectingFailures = <A, R, E, B>(
  f: (a: A) => XIO<R, E, B>,
): ((as: ReadonlyArray<A>) => XIO<R, never, ReadonlyArray<E.Either<E, B>>>) =>
  traverseArray(flow(f, either))

export const sequenceArray: <R, E, A>(
  xios: ReadonlyArray<XIO<R, E, A>>,
) => XIO<R, E, ReadonlyArray<A>> = traverseArray(identity)

export function filterOrElse<A, B extends A, E2>(
  refinement: Refinement<A, B>,
  onFalse: (a: A) => E2,
): <R, E1>(ma: XIO<R, E1, A>) => XIO<R, E1 | E2, B>
export function filterOrElse<A, E2>(
  predicate: Predicate<A>,
  onFalse: (a: A) => E2,
): <R, E1, B extends A>(mb: XIO<R, E1, B>) => XIO<R, E1 | E2, B>
export function filterOrElse<A, E2>(
  predicate: Predicate<A>,
  onFalse: (a: A) => E2,
): <R, E1>(ma: XIO<R, E1, A>) => XIO<R, E1 | E2, A>
export function filterOrElse<A, E2>(
  predicate: Predicate<A>,
  onFalse: (a: A) => E2,
): <R, E1>(ma: XIO<R, E1, A>) => XIO<R, E1 | E2, A> {
  return chain((a) => (predicate(a) ? succeed(a) : fail(onFalse(a))))
}

/**
 * Returns an `XIO` that is interrupted as if by the specified fiber.
 */
export const interruptAs = (fiberId: FId.FiberId): UIO<never> => failCause(C.interrupt(fiberId))

/**
 * Returns an `XIO` that is interrupted as if by the fiber executing this effect.
 */
export const interrupt: UIO<never> = pipe(fiberId, chain(interruptAs))

/**
 * Checks the interrupt status, and produces the effect returned by the
 * specified callback.
 */
export const checkInterruptible = <R, E, A>(
  f: (status: InterruptStatus.InterruptStatus) => XIO<R, E, A>,
): XIO<R, E, A> => new GetInterruptStatus(f)

/**
 * Switches the interrupt status for this `XIO`. If `true` is used, then the
 * effect becomes interruptible (the default), while if `false` is used, then
 * the effect becomes uninterruptible. These changes are compositional, so
 * they only xioect regions of the effect.
 */
export const interruptStatus =
  (flag: InterruptStatus.InterruptStatus) =>
  <R, E, A>(xio: XIO<R, E, A>): XIO<R, E, A> =>
    new SetInterruptStatus(xio, flag)

/**
 * Returns a new effect that performs the same operations as this effect, but
 * interruptibly, even if composed inside of an uninterruptible region.
 *
 * Note that effects are interruptible by default, so this function only has
 * meaning if used within an uninterruptible region.
 *
 * WARNING: This operator "punches holes" into effects, allowing them to be
 * interrupted in unexpected places. Do not use this operator unless you know
 * exactly what you are doing. Instead, you should use
 * `uninterruptibleMask`.
 */
export const interruptible: <R, E, A>(xio: XIO<R, E, A>) => XIO<R, E, A> = interruptStatus(
  InterruptStatus.Interruptible,
)

/**
 * Performs this `XIO` uninterruptibly. This will prevent the effect from
 * being terminated externally, but the effect may fail for internal reasons
 * (e.g. an uncaught error) or terminate due to defect.
 *
 * Uninterruptible effects may recover from all failure causes (including
 * interruption of an inner effect that has been made interruptible).
 */
export const uninterruptible: <R, E, A>(xio: XIO<R, E, A>) => XIO<R, E, A> = interruptStatus(
  InterruptStatus.Uninterruptible,
)

type InterruptStatusRestore = <R, E, A>(xio: XIO<R, E, A>) => XIO<R, E, A>

/**
 * Makes the effect uninterruptible, but passes it a restore function that can
 * be used to restore the inherited interruptibility from whatever region the
 * effect is composed into.
 */
export const uninterruptibleMask = <R, E, A>(
  f: (restore: InterruptStatusRestore) => XIO<R, E, A>,
): XIO<R, E, A> => checkInterruptible((flag) => uninterruptible(f(interruptStatus(flag))))

export const ensuring =
  <R1>(finalizer: XIO<R1, never, void>) =>
  <R, E, A>(xio: XIO<R, E, A>): XIO<R & R1, E, A> =>
    new Ensuring(xio, finalizer)

/**
 * Acquires a resource, uses the resource, and then releases the resource.
 * Neither acquisition nor the release will be interrupted, and the
 * resource is guaranteed to be released, so long as the `acquire` effect
 * succeeds. If `use` fails, then after release, the returned effect will fail
 * with the same error.
 */
export function bracket<R, E, A, R1, E1, B, R2>(
  acquire: XIO<R, E, A>,
  use: (a: A) => XIO<R1, E1, B>,
  release: (a: A, exit: Ex.Exit<E1, B>) => XIO<R2, never, void>,
): XIO<R & R1 & R2, E | E1, B> {
  return uninterruptibleMask((restore) =>
    pipe(
      acquire,
      chain((a) =>
        pipe(
          defer(() => restore(use(a))),
          result,
          chain((exit) =>
            pipe(
              defer(() => release(a, exit)),
              matchCauseXIO(
                (cause2) =>
                  pipe(
                    exit,
                    Ex.match(
                      (cause) => C.then(cause, cause2),
                      () => cause2,
                    ),
                    failCause,
                  ),
                () => done(exit),
              ),
            ),
          ),
        ),
      ),
    ),
  )
}

/**
 * Runs the specified effect if this `XIO` is interrupted.
 */
export const onInterrupt =
  <R1>(cleanup: XIO<R1, never, void>) =>
  <R, E, A>(xio: XIO<R, E, A>): XIO<R & R1, E, A> =>
    uninterruptibleMask((restore) =>
      pipe(
        restore(xio),
        matchCauseXIO(
          (cause) =>
            C.isInterrupted(cause)
              ? pipe(
                  cleanup,
                  chain(() => failCause(cause)),
                )
              : failCause(cause),
          succeed,
        ),
      ),
    )

/**
 * Ensures that a cleanup functions runs, whether this effect succeeds, fails,
 * or is interrupted.
 */
export const onExit =
  <E, A, R1>(cleanup: (exit: Ex.Exit<E, A>) => XIO<R1, never, void>) =>
  <R>(xio: XIO<R, E, A>): XIO<R & R1, E, A> =>
    bracket(
      unit,
      () => xio,
      (_, exit) => cleanup(exit),
    )

/**
 * Executes `xio` if the given `condition` effect is determinted to be `true`
 *
 * Semantically similar to:
 * ```
 * if (condition) {
 *  xio()
 * }
 * ```
 */
export const whenXIO =
  <R1, E1>(condition: XIO<R1, E1, boolean>) =>
  <R, E, A>(xio: XIO<R, E, A>): XIO<R & R1, E | E1, O.Option<A>> =>
    defer(() =>
      pipe(
        condition,
        chain(
          B.match(
            () => succeed(O.none),
            () => pipe(xio, map(O.some)),
          ),
        ),
      ),
    )

export const succeedLeft: <E>(e: E) => XIO<unknown, never, E.Either<E, never>> = flow(
  E.left,
  succeed,
)

export const succeedRight: <A>(a: A) => XIO<unknown, never, E.Either<never, A>> = flow(
  E.right,
  succeed,
)

export const either: <R, E, A>(xio: XIO<R, E, A>) => XIO<R, never, E.Either<E, A>> = matchXIO(
  succeedLeft,
  succeedRight,
)

/**
 * An `XIO` that succeeds with an empty object, used to begin a chain of `bind`s
 */
export const Do = succeed({})

export const bindTo = <N extends string>(
  name: N,
): (<R, E, A>(xio: XIO<R, E, A>) => XIO<R, E, { [K in N]: A }>) =>
  map((a) => ({ [name]: a } as { [K in N]: typeof a }))

/**
 * Returns an `XIO` that will execute an effect, then pass its value to the continuation function `f`,
 * using `name` to bind the successful result of the continuation to a field in an object.
 */
export const bind =
  <N extends string, A, R1, E1, B>(name: Exclude<N, keyof A>, f: (a: A) => XIO<R1, E1, B>) =>
  <R, E>(
    ma: XIO<R, E, A>,
  ): XIO<R & R1, E | E1, { readonly [K in keyof A | N]: K extends keyof A ? A[K] : B }> =>
    pipe(
      ma,
      chain((a) =>
        pipe(
          f(a),
          map((b) => unsafeCoerce({ ...a, [name]: b })),
        ),
      ),
    )

export const apS =
  <N extends string, A, R1, E1, B>(name: Exclude<N, keyof A>, fb: XIO<R1, E1, B>) =>
  <R, E>(
    fa: XIO<R, E, A>,
  ): XIO<R & R1, E | E1, { readonly [K in keyof A | N]: K extends keyof A ? A[K] : B }> =>
    pipe(
      fa,
      zipWith(fb, (a, b) => unsafeCoerce({ ...a, [name]: b })),
    )

/**
 * Updates the `FiberRef` values for the fiber running this effect using the
 * specified function.
 */
export const updateFiberRefs = (
  f: (fiberId: FId.Runtime, fiberRefs: FiberRefs) => FiberRefs,
): UIO<void> => new FiberRefModifyAll((fiberId, fiberRefs) => [undefined, f(fiberId, fiberRefs)])

/**
 * Swaps the failure and success channels of an `XIO`
 */
export const swap: <R, E, A>(xio: XIO<R, E, A>) => XIO<R, A, E> = matchXIO(succeed, fail)

/**
 * Returns a new effect that repeats this effect the specified number of times
 * or until the first failure. Repeats are in addition to the first execution,
 * so that `repeatN(1)(xio)` yields an effect that executes `io`, and then if
 * that succeeds, executes `io` an additional time.
 */
export const repeatN =
  (n: number) =>
  <R, E, A>(xio: XIO<R, E, A>): XIO<R, E, A> =>
    defer(() => repeatNLoop(xio, n))

/**
 * Repeats this effect until its value satisfies the specified effectful
 * predicate or until the first failure.
 */
export const repeatUntilXIO =
  <A, R1>(f: (a: A) => XIO<R1, never, boolean>) =>
  <R, E>(xio: XIO<R, E, A>): XIO<R & R1, E, A> =>
    pipe(
      xio,
      chain((a) =>
        pipe(
          f(a),
          chain((b) =>
            b
              ? succeed(a)
              : pipe(
                  yieldNow,
                  chain(() => repeatUntilXIO(f)(xio)),
                ),
          ),
        ),
      ),
    )

/**
 * Repeats this effect until its value satisfies the specified predicate or
 * until the first failure.
 */
export const repeatUntil = <A>(p: Predicate<A>): (<R, E>(xio: XIO<R, E, A>) => XIO<R, E, A>) =>
  repeatUntilXIO((a) => succeed(p(a)))

/**
 * Returns a new effect that ignores the success or failure of this effect.
 */
export const ignore: <R, E, A>(xio: XIO<R, E, A>) => XIO<R, never, void> = match(
  () => undefined,
  () => undefined,
)

/**
 * Exposes the full cause of failure of this effect.
 */
export const sandbox: <R, E, A>(xio: XIO<R, E, A>) => XIO<R, C.Cause<E>, A> = matchCauseXIO(
  fail,
  succeed,
)

/**
 * Repeats this effect forever (until the first error).
 */
export const forever = <R, E, A>(xio: XIO<R, E, A>): XIO<R, E, never> =>
  pipe(
    xio,
    chain(() => yieldNow),
    chain(() => forever(xio)),
  )

export const ifXIO = <R1, E1, B, R2, E2, C>(
  onFalse: XIO<R1, E1, B>,
  onTrue: XIO<R2, E2, C>,
): (<R, E>(b: XIO<R, E, boolean>) => XIO<R & R1 & R2, E | E1 | E2, B | C>) =>
  chain((b) => (b ? (onTrue as XIO<R1 & R2, E1 | E2, B | C>) : onFalse))

const if_ = <R1, E1, B, R2, E2, C>(
  onFalse: XIO<R1, E1, B>,
  onTrue: XIO<R2, E2, C>,
): ((b: boolean) => XIO<R1 & R2, E1 | E2, B | C>) => flow(succeed, ifXIO(onFalse, onTrue))

export { if_ as if }

const MAX_SET_TIMEOUT_VALUE = 2 ** 31 - 1

/**
 * An `XIO` that will never terminate, unless interrupted
 *
 * Sematically, it is similar to:
 * ```
 * while(true) {
 *   ...
 * }
 * ```
 */
export const never = asyncInterrupt<unknown, never, never>(() => {
  const handle = setInterval(() => {
    //
  }, MAX_SET_TIMEOUT_VALUE)
  return E.left(
    fromIO(() => {
      clearInterval(handle)
    }),
  )
})

class GenXIO<R, E, A> {
  readonly _R!: (_: R) => void
  readonly _E!: () => E
  readonly _A!: () => A

  constructor(readonly xio: XIO<R, E, A>) {}

  *[Symbol.iterator](): Generator<GenXIO<R, E, A>, A, unknown> {
    // @ts-expect-error -- this is so GenXIO can be yield*-ed
    return yield this
  }
}

export function gen<T extends GenXIO<never, unknown, unknown>, A>(
  f: (i: <R, E, A>(_: XIO<R, E, A>) => GenXIO<R, E, A>) => Generator<T, A, unknown>,
): XIO<_R<T>, _E<T>, A> {
  return defer(() => {
    const iterator = f((_) => new GenXIO(_))
    const state = iterator.next()

    const run = (
      state: IteratorYieldResult<T> | IteratorReturnResult<A>,
    ): XIO<never, unknown, A> => {
      if (state.done === true) {
        return succeed(state.value)
      }
      return new Chain(
        defer(() => state.value.xio),
        (val: unknown) => run(iterator.next(val)),
      )
    }

    return run(state) as XIO<_R<T>, _E<T>, A>
  })
}

/*
 * ------------------------------------------------------------------------------------------------
 * Internal
 * ------------------------------------------------------------------------------------------------
 */

function foreachWithIndexDiscardLoop<A, R, E, B>(
  iterator: Iterator<A>,
  f: (i: number, a: A) => XIO<R, E, B>,
  i = 0,
): XIO<R, E, void> {
  const next = iterator.next()
  return next.done === true
    ? unit
    : pipe(
        f(i, next.value),
        chain(() => foreachWithIndexDiscardLoop(iterator, f, i + 1)),
      )
}

function repeatNLoop<R, E, A>(xio: XIO<R, E, A>, n: number): XIO<R, E, A> {
  return pipe(
    xio,
    chain((a) =>
      n <= 0
        ? succeed(a)
        : pipe(
            yieldNow,
            chain(() => repeatNLoop(xio, n - 1)),
          ),
    ),
  )
}
