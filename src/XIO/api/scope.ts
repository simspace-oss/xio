import type * as Ex from "../../Exit"
import type { Has } from "../../Tag"
import type { XIO } from "../definition"

import { pipe } from "fp-ts/function"

import * as Scope from "../../Scope"
import {
  ask,
  askService,
  bind,
  bracket,
  chain,
  chainFirst,
  Do,
  provide,
  uninterruptible,
} from "./core"

export const scope = askService(Scope.Tag)

/**
 * A more powerful variant of `addFinalizer` that allows the finalizer to
 * depend on the `Exit` value that the scope is closed with.
 */
export const addFinalizerExit = <R>(
  finalizer: (exit: Ex.Exit<unknown, unknown>) => XIO<R, never, void>,
): XIO<R & Has<Scope.Scope>, never, void> =>
  pipe(
    Do,
    bind("environment", () => ask<R>()),
    bind("scope", () => scope),
    chain(({ environment, scope }) =>
      scope.addFinalizerExit((exit) => pipe(finalizer(exit), provide(environment))),
    ),
  )

/**
 * Adds a finalizer to the scope of this effect. The finalizer is guaranteed
 * to be run when the scope is closed.
 */
export const addFinalizer = <R>(
  finalizer: XIO<R, never, void>,
): XIO<R & Has<Scope.Scope>, never, void> => addFinalizerExit(() => finalizer)

/**
 * A more powerful variant of `acquireRelease` that allows the `release`
 * effect to depend on the `Exit` value specified when the scope is closed.
 */
export const acquireReleaseExit = <R, E, A, R1>(
  acquire: XIO<R, E, A>,
  release: (a: A, exit: Ex.Exit<unknown, unknown>) => XIO<R1, never, void>,
): XIO<R & R1 & Has<Scope.Scope>, E, A> =>
  uninterruptible(
    pipe(
      acquire,
      chainFirst((a) => addFinalizerExit((exit) => release(a, exit))),
    ),
  )

/**
 * Constructs a scoped resource from an `acquire` and `release` effect. If
 * `acquire` successfully completes execution then `release` will be added to
 * the finalizers associated with the scope of this effect and is guaranteed
 * to be run when the scope is closed.
 *
 * The `acquire` and `release` effects will be run uninterruptibly.
 */
export const acquireRelease = <R, E, A, R1>(
  acquire: XIO<R, E, A>,
  release: (a: A) => XIO<R1, never, void>,
): XIO<R & R1 & Has<Scope.Scope>, E, A> => acquireReleaseExit(acquire, (a) => release(a))

/**
 * Scopes all resources used in this effect to the lifetime of the effect,
 * ensuring that their finalizers are run as soon as this effect completes
 * execution, whether by success, failure, or interruption.
 */
export const scoped = <R, E, A>(xio: XIO<R & Has<Scope.Scope>, E, A>): XIO<R, E, A> =>
  pipe(
    Scope.make,
    chain((scope) => scope.use(xio)),
  )

/**
 * Scopes all resources acquired by `resource` to the lifetime of `use`
 * without effecting the scope of any resources acquired by `use`.
 */
export const using =
  <A, R1, E1, B>(use: (a: A) => XIO<R1, E1, B>) =>
  <R, E>(resource: XIO<R & Has<Scope.Scope>, E, A>): XIO<R & R1, E | E1, B> =>
    bracket(
      Scope.make,
      (scope) => pipe(scope.extend(resource), chain(use)),
      (scope, exit) => scope.close(exit),
    )
