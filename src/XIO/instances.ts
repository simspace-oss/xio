import type { XIO } from "./definition"
import type { Alt3 } from "fp-ts/Alt"
import type { Applicative3, Applicative3C } from "fp-ts/Applicative"
import type { Apply3 } from "fp-ts/Apply"
import type { FromIO3 } from "fp-ts/FromIO"
import type { Functor3 } from "fp-ts/Functor"
import type { Bifunctor3 } from "fp-ts/lib/Bifunctor"
import type { FromTask3 } from "fp-ts/lib/FromTask"
import type { Monad3 } from "fp-ts/Monad"
import type { Pointed3 } from "fp-ts/Pointed"
import type { Semigroup } from "fp-ts/Semigroup"

import * as E from "fp-ts/Either"
import { pipe } from "fp-ts/lib/function"

import { apPar, zipWithPar } from "./api/apply-concurrent"
import {
  alt,
  ap,
  bimap,
  chain,
  either,
  fail,
  flatten,
  fromIO,
  fromTask,
  map,
  mapError,
  succeed,
} from "./api/core"

export const URI = "@simspace/xio/XIO"
export type URI = typeof URI

declare module "fp-ts/HKT" {
  interface URItoKind3<R, E, A> {
    readonly [URI]: XIO<R, E, A>
  }
}

export const Pointed: Pointed3<URI> = {
  URI,
  of: succeed,
}

export const Functor: Functor3<URI> = {
  URI,
  map: (fa, f) => map(f)(fa),
}

export const Bifunctor: Bifunctor3<URI> = {
  ...Functor,
  bimap: (fea, f, g) => bimap(f, g)(fea),
  mapLeft: (fea, f) => mapError(f)(fea),
}

export const Apply: Apply3<URI> = {
  ...Functor,
  ap: (fab, fa) => ap(fa)(fab),
}

export const ApplyPar: Apply3<URI> = {
  ...Functor,
  ap: (fab, fa) => apPar(fa)(fab),
}

export const Applicative: Applicative3<URI> = {
  ...Pointed,
  ...Apply,
}

export const ApplicativePar: Applicative3<URI> = {
  ...Pointed,
  ...ApplyPar,
}

export const Monad: Monad3<URI> = {
  ...Applicative,
  chain: (fa, f) => chain(f)(fa),
}

export const Alt: Alt3<URI> = {
  ...Functor,
  alt: (fa, that) => alt(that)(fa),
}

export const FromIO: FromIO3<URI> = {
  URI,
  fromIO,
}

export const FromTask: FromTask3<URI> = {
  ...FromIO,
  fromTask,
}

export const getApplicativeParValidation = <E>(S: Semigroup<E>): Applicative3C<URI, E> => ({
  _E: undefined as unknown as E,
  ...Functor,
  ap: (fab, fa) =>
    pipe(
      either(fab),
      zipWithPar(either(fa), (f, a) => {
        if (E.isLeft(f) && E.isLeft(a)) {
          return fail(S.concat(f.left, a.left))
        }
        if (E.isLeft(f)) {
          return fail(f.left)
        }
        if (E.isLeft(a)) {
          return fail(a.left)
        }
        return succeed(f.right(a.right))
      }),
      flatten,
    ),
  of: succeed,
})
