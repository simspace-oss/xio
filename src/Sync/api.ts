import type { Has, Tag } from "../Tag"
import type { Sync } from "./definition"

import * as E from "fp-ts/Either"
import { flow, identity, pipe, unsafeCoerce } from "fp-ts/function"

import * as C from "../Cause"
import { Chain, Defer, Fail, Match, Modify, Provide, Read, Succeed, SucceedNow } from "./definition"

/**
 * Returns a `Z` that succeeds with a pure value
 */
export const succeed = <A, S1 = unknown, S2 = never>(value: A): Sync<S1, S2, unknown, never, A> =>
  new SucceedNow(value)

export const right = succeed

/**
 * Returns a `Z` that executes a synchronous effect, and succeeds with its returned value
 */
export const fromIO = <A>(effect: () => A): Sync<unknown, never, unknown, never, A> =>
  new Succeed(effect)

/**
 * Returns a `Z` that executes a synchronous effect, and succeeds with its returned value.
 * If the effect throws, it's thrown value will be mapped by function `onThrow` and returned
 * as typed failure.
 */
export const tryCatch = <E, A>(
  effect: () => A,
  onThrow: (error: unknown) => E,
): Sync<unknown, never, unknown, E, A> =>
  defer(() => {
    try {
      return succeed(effect())
    } catch (e) {
      return fail(onThrow(e))
    }
  })

/**
 * Returns a `Z` that defers execution of a `Z`. Useful for recursion
 */
export const defer = <S1, S2, R, E, A>(make: () => Sync<S1, S2, R, E, A>): Sync<S1, S2, R, E, A> =>
  new Defer(make)

/**
 * Returns a `Z` that fails with the given cause
 */
export const failCause = <E>(cause: C.Cause<E>): Sync<unknown, never, unknown, E, never> =>
  new Fail(cause)

/**
 * Returns a `Z` that fails with the given cause
 */
export const failCauseLazy = <E>(
  cause: () => C.Cause<E>,
): Sync<unknown, never, unknown, E, never> => defer(() => failCause(cause()))

/**
 * Returns a `Z` that fails with the given value
 */
export const fail: <E>(value: E) => Sync<unknown, never, unknown, E, never> = flow(
  C.fail,
  failCause,
)

export const left = fail

/**
 * Returns a `Z` that fails with the given lazy value
 */
export const failLazy = <E>(value: () => E): Sync<unknown, never, unknown, E, never> =>
  defer(() => fail(value()))

/**
 * Returns a `Z` that fails with the given defect
 */
export const halt: (value: unknown) => Sync<unknown, never, unknown, never, never> = flow(
  C.halt,
  failCause,
)

/**
 * Returns a `Z` that fails with the given defect
 */
export const haltLazy = (value: () => unknown): Sync<unknown, never, unknown, never, never> =>
  defer(() => halt(value()))

/**
 * Constructs a computation from the specified modify function
 */
export const modify = <S1, S2, A>(
  f: (s: S1) => readonly [A, S2],
): Sync<S1, S2, unknown, never, A> => new Modify(f)

/**
 * Constructs a computation that returns the initial state unchanged.
 */
export const get = <S>(): Sync<S, S, unknown, never, S> => modify((s) => [s, s])

/**
 * Constructs a computation that uses the initial state to produce an output
 */
export const gets = <S, A>(f: (s: S) => A): Sync<S, S, unknown, never, A> =>
  modify((s) => [f(s), s])

/**
 * Constructs a computation that effectfully uses the initial state to produce an output
 */
export const getsE = <S, R, E, A>(f: (s: S) => Sync<S, S, R, E, A>): Sync<S, S, R, E, A> =>
  pipe(get<S>(), chain(f))

/**
 * Like `map`, but also allows the state to be modified.
 */
export const transform = <S2, A, S3, B>(
  f: (s: S2, a: A) => readonly [B, S3],
): (<S1, R, E>(ma: Sync<S1, S2, R, E, A>) => Sync<S1, S3, R, E, B>) =>
  chain((a) => modify((s) => f(s, a)))

/**
 * Constructs a computation from the specified update function.
 */
export const update = <S1, S2>(f: (s: S1) => S2): Sync<S1, S2, unknown, never, void> =>
  modify((s) => [undefined, f(s)])

/**
 * Returns a `Z` that will execute an effect, then pass its value to the continuation function `f`
 */
export const chain =
  <S2, A, S3, R1, E1, B>(f: (a: A) => Sync<S2, S3, R1, E1, B>) =>
  <S1, R, E>(ma: Sync<S1, S2, R, E, A>): Sync<S1, S3, R & R1, E | E1, B> =>
    new Chain(ma, f)

/**
 * Returns a `Z` that will have its success transformed by `f`
 */
export const map = <A, B>(
  f: (a: A) => B,
): (<S1, S2, R, E>(fa: Sync<S1, S2, R, E, A>) => Sync<S1, S2, R, E, B>) =>
  chain((a) => succeed(f(a)))

/**
 * Returns an effect with its error channel mapped using the specified
 * function. This can be used to lift a "smaller" error into a "larger" error.
 */
export const mapError = <E, E1>(
  f: (e: E) => E1,
): (<S1, S2, R, A>(fa: Sync<S1, S2, R, E, A>) => Sync<S1, S2, R, E1, A>) =>
  matchE(flow(f, fail), succeed)

export const matchCauseE =
  <S1, S2, R, E, A, S0, S3, R1, E1, B, S4, R2, E2, C>(
    onFailure: (e: C.Cause<E>) => Sync<S0, S3, R1, E1, B>,
    onSuccess: (a: A) => Sync<S2, S4, R2, E2, C>,
  ) =>
  (fa: Sync<S1, S2, R, E, A>): Sync<S0 & S1, S3 | S4, R & R1 & R2, E1 | E2, B | C> =>
    new Match(fa, onFailure, onSuccess)

/**
 * Recovers from errors by accepting one computation to execute for the case
 * of an error, and one computation to execute for the case of success.
 */
export const matchE = <S1, S5, S2, R, E, A, S3, R1, E1, B, S4, R2, E2, C>(
  onFailure: (e: E) => Sync<S5, S3, R1, E1, B>,
  onSuccess: (a: A) => Sync<S2, S4, R2, E2, C>,
): ((fa: Sync<S1, S2, R, E, A>) => Sync<S1 & S5, S3 | S4, R & R1 & R2, E1 | E2, B | C>) =>
  matchCauseE(flow(C.failureOrCause, E.match(onFailure, failCause)), onSuccess)

export const match = <E, A, B, C>(
  onFailure: (e: E) => B,
  onSuccess: (a: A) => C,
): (<S1, S2, R>(ma: Sync<S1, S2, R, E, A>) => Sync<S1, S2, R, never, B | C>) =>
  matchE(flow(onFailure, succeed), flow(onSuccess, succeed))

/**
 * Transforms the updated state of this computation with the specified
 * function.
 */
export const mapState = <S2, S3>(
  f: (s: S2) => S3,
): (<S1, R, E, A>(ma: Sync<S1, S2, R, E, A>) => Sync<S1, S3, R, E, A>) =>
  transform((s, a) => [a, f(s)])

/**
 * Constructs a computation that may fail from the specified modify function.
 */
export const modifyEither = <S1, S2, E, A>(
  f: (s: S1) => E.Either<E, readonly [A, S2]>,
): Sync<S1, S2, unknown, E, A> =>
  pipe(
    get<S1>(),
    map(f),
    chain(
      E.matchW(fail, ([a, s2]) =>
        pipe(
          succeed(a),
          mapState(() => s2),
        ),
      ),
    ),
  )

/**
 * Provides a computation with its input state
 */
export const put = <S>(s: S): Sync<unknown, S, unknown, never, void> => modify(() => [undefined, s])

/**
 * Constructs an effectful computation from the environment
 */
export const asksE = <R0, S1, S2, R, E, A>(
  f: (environment: R0) => Sync<S1, S2, R, E, A>,
): Sync<S1, S2, R0 & R, E, A> => new Read(f)

/**
 * Constructs a computation from the environment
 */
export const asks = <R, A>(f: (environment: R) => A): Sync<unknown, never, R, never, A> =>
  asksE(flow(f, succeed))

export const ask = <R>(): Sync<unknown, never, R, never, R> => asks(identity)

/**
 * Returns a `Z` with its entire environment provided
 */
export const provide =
  <R>(environment: R) =>
  <S1, S2, E, A>(z: Sync<S1, S2, R, E, A>): Sync<S1, S2, unknown, E, A> =>
    new Provide(z, environment)

/**
 * Returns a `Z` with a part of its environment provided
 */
export const providePartial = <R0>(
  environment: R0,
): (<S1, S2, R, E, A>(z: Sync<S1, S2, R0 & R, E, A>) => Sync<S1, S2, R, E, A>) =>
  local((r) => ({ ...r, ...environment }))

/**
 * Returns a `Z` that has its environment mapped by the specified function `f`
 */
export const local =
  <R0, R>(f: (environment: R0) => R) =>
  <S1, S2, E, A>(z: Sync<S1, S2, R, E, A>): Sync<S1, S2, R0, E, A> =>
    asksE((environment: R0) => pipe(z, provide(f(environment))))

/**
 * Combines this computation with the specified computation, passing the
 * updated state from this computation to that computation and combining the
 * results of both using the specified function.
 */
export const zipWith = <S2, A, S3, R1, E1, B, C>(
  fb: Sync<S2, S3, R1, E1, B>,
  f: (a: A, b: B) => C,
): (<S1, R, E>(fa: Sync<S1, S2, R, E, A>) => Sync<S1, S3, R & R1, E | E1, C>) =>
  chain((a) =>
    pipe(
      fb,
      map((b) => f(a, b)),
    ),
  )

export const ap = <S2, A, S3, R1, E1>(
  fa: Sync<S2, S3, R1, E1, A>,
): (<S1, R, E, B>(fab: Sync<S1, S2, R, E, (a: A) => B>) => Sync<S1, S3, R & R1, E | E1, B>) =>
  zipWith(fa, (f, a) => f(a))

/**
 * Combines the first computation with the specified computation, passing the
 * updated state from the first computation to the second computation and returning the
 * result of the first computation.
 */
export const apFirst = <S, R1, E1, B>(
  fb: Sync<S, S, R1, E1, B>,
): (<R, E, A>(fa: Sync<S, S, R, E, A>) => Sync<S, S, R & R1, E | E1, A>) => zipWith(fb, (a) => a)

/**
 * Combines the first computation with the specified computation, passing the
 * updated state from the first computation to the second computation and returning the
 * result of the second computation.
 */
export const apSecond = <S, R1, E1, B>(
  fb: Sync<S, S, R1, E1, B>,
): (<R, E, A>(fa: Sync<S, S, R, E, A>) => Sync<S, S, R & R1, E | E1, B>) => zipWith(fb, (_, b) => b)

export const askService = <A>(tag: Tag<A>): Sync<unknown, never, Has<A>, never, A> =>
  asks((environment) => tag.unsafeGet(environment))

export const asksServiceE = <T, S1, S2, R, E, A>(
  tag: Tag<T>,
  f: (service: T) => Sync<S1, S2, R, E, A>,
): Sync<S1, S2, R & Has<T>, E, A> => asksE((environment: Has<T>) => f(tag.unsafeGet(environment)))

export const asksService = <T, A>(
  tag: Tag<T>,
  f: (service: T) => A,
): Sync<unknown, never, Has<T>, never, A> => asks((environment) => f(tag.unsafeGet(environment)))

export const provideService = <T>(
  tag: Tag<T>,
  service: T,
): (<S1, S2, R, E, A>(z: Sync<S1, S2, R & Has<T>, E, A>) => Sync<S1, S2, R, E, A>) =>
  local((environment) => ({ ...environment, ...tag.of(service) }))

export const Do = succeed({})

export const bindTo = <N extends string>(
  name: N,
): (<S1, S2, R, E, A>(ma: Sync<S1, S2, R, E, A>) => Sync<S1, S2, R, E, { readonly [K in N]: A }>) =>
  map((a) => unsafeCoerce({ [name]: a }))

/**
 * Returns a `Z` that will execute an effect, then pass its value to the continuation function `f`,
 * using `name` to bind the successful result of the continuation to a field in an object.
 */
export const bind = <S2, A, N extends string, S3, R1, E1, B>(
  name: Exclude<N, keyof A>,
  f: (a: A) => Sync<S2, S3, R1, E1, B>,
): (<S1, R, E>(
  z: Sync<S1, S2, R, E, A>,
) => Sync<S1, S3, R & R1, E | E1, { readonly [K in N | keyof A]: K extends keyof A ? A[K] : B }>) =>
  chain((a) =>
    pipe(
      f(a),
      map((b) => unsafeCoerce({ ...a, [name]: b })),
    ),
  )

export const apS = <S, A, N extends string, R1, E1, B>(
  name: Exclude<N, keyof A>,
  fb: Sync<S, S, R1, E1, B>,
): (<R, E>(
  z: Sync<S, S, R, E, A>,
) => Sync<S, S, R & R1, E | E1, { readonly [K in N | keyof A]: K extends keyof A ? A[K] : B }>) =>
  zipWith(fb, (a, b) => unsafeCoerce({ ...a, [name]: b }))

export const either: <S1, S2, R, E, A>(
  z: Sync<S1, S2, R, E, A>,
) => Sync<S1, S2, R, never, E.Either<E, A>> = match(E.left, E.right)

export const sandbox: <S1, S2, R, E, A>(
  z: Sync<S1, S2, R, E, A>,
) => Sync<S1, S2, R, C.Cause<E>, A> = matchCauseE(fail, succeed)

export const swap: <S1, S2, R, E, A>(z: Sync<S1, S2, R, E, A>) => Sync<S1, S2, R, A, E> = matchE(
  succeed,
  fail,
)
