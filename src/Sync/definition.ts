import type { Cause } from "../Cause"

import * as XIO from "../XIO/definition"

export const SyncTypeId = Symbol.for("@simspace/xio/Sync")
export type SyncTypeId = typeof SyncTypeId

export abstract class Sync<S1, S2, R, E, A> {
  readonly [XIO.XIOTypeId]: XIO.XIOTypeId = XIO.XIOTypeId;
  readonly [SyncTypeId]: SyncTypeId = SyncTypeId
  readonly _tag = XIO.Tag.Sync
  readonly _S1!: (_: S1) => void
  readonly _S2!: () => S2
  readonly _R!: (_: R) => void
  readonly _E!: () => E
  readonly _A!: () => A
}

export type MonoState<S, A> = Sync<S, S, unknown, never, A>
export type State<S1, S2, A> = Sync<S1, S2, unknown, never, A>
export type Reader<R, A> = Sync<unknown, unknown, R, never, A>
export type ReaderEither<R, E, A> = Sync<unknown, unknown, R, E, A>
export type Either<E, A> = Sync<unknown, unknown, unknown, E, A>
export type IO<A> = Sync<unknown, unknown, unknown, never, A>

export const enum Tag {
  SucceedNow,
  Succeed,
  Defer,
  Fail,
  Modify,
  Chain,
  Match,
  Read,
  Provide,
  Tell,
  Listen,
  MapLog,
}

export class SucceedNow<A> extends Sync<unknown, never, unknown, never, A> {
  readonly _syncTag = Tag.SucceedNow
  constructor(readonly value: A) {
    super()
  }
}

export class Succeed<A> extends Sync<unknown, never, unknown, never, A> {
  readonly _syncTag = Tag.Succeed
  constructor(readonly effect: () => A) {
    super()
  }
}

export class Defer<S1, S2, R, E, A> extends Sync<S1, S2, R, E, A> {
  readonly _syncTag = Tag.Defer
  constructor(readonly make: () => Sync<S1, S2, R, E, A>) {
    super()
  }
}

export class Fail<E> extends Sync<unknown, never, unknown, E, never> {
  readonly _syncTag = Tag.Fail
  constructor(readonly cause: Cause<E>) {
    super()
  }
}

export class Modify<S1, S2, A> extends Sync<S1, S2, unknown, never, A> {
  readonly _syncTag = Tag.Modify
  constructor(readonly run: (s1: S1) => readonly [A, S2]) {
    super()
  }
}

export class Chain<S1, S2, R, E, A, S3, R1, E1, B> extends Sync<S1, S3, R1 & R, E1 | E, B> {
  readonly _syncTag = Tag.Chain
  constructor(readonly ma: Sync<S1, S2, R, E, A>, readonly f: (a: A) => Sync<S2, S3, R1, E1, B>) {
    super()
  }
}

export class Match<S1, S2, S5, R, E, A, S3, R1, E1, B, S4, R2, E2, C> extends Sync<
  S1 & S5,
  S3 | S4,
  R & R1 & R2,
  E1 | E2,
  B | C
> {
  readonly _syncTag = Tag.Match
  constructor(
    readonly z: Sync<S1, S2, R, E, A>,
    readonly onFailure: (e: Cause<E>) => Sync<S5, S3, R1, E1, B>,
    readonly onSuccess: (a: A) => Sync<S2, S4, R2, E2, C>,
  ) {
    super()
  }
}

export class Read<R0, S1, S2, R, E, A> extends Sync<S1, S2, R0 & R, E, A> {
  readonly _syncTag = Tag.Read
  constructor(readonly asks: (environment: R0) => Sync<S1, S2, R, E, A>) {
    super()
  }
}

export class Provide<S1, S2, R, E, A> extends Sync<S1, S2, unknown, E, A> {
  readonly _syncTag = Tag.Provide
  constructor(readonly ma: Sync<S1, S2, R, E, A>, readonly environment: R) {
    super()
  }
}

export type Concrete =
  | SucceedNow<unknown>
  | Fail<unknown>
  | Modify<unknown, unknown, unknown>
  | Chain<unknown, unknown, unknown, unknown, unknown, unknown, unknown, unknown, unknown>
  | Match<
      unknown,
      unknown,
      unknown,
      unknown,
      unknown,
      unknown,
      unknown,
      unknown,
      unknown,
      unknown,
      unknown,
      unknown,
      unknown,
      unknown
    >
  | Read<unknown, unknown, unknown, unknown, unknown, unknown>
  | Provide<unknown, unknown, unknown, unknown, unknown>
  | Defer<unknown, unknown, unknown, unknown, unknown>
  | Succeed<unknown>
