import type { Cause } from "../Cause"

import { flow, identity, pipe } from "fp-ts/function"

import * as C from "../Cause"
import * as Ex from "../Exit"
import { Stack } from "../internal/Stack"
import * as Sync from "./internal/Sync"

type Erased = Sync.Sync<unknown, unknown, unknown, unknown, unknown>
type ErasedContinuation = (_: unknown) => Erased

class MatchFrame {
  readonly _tag = "MatchFrame"
  constructor(readonly failure: ErasedContinuation, readonly apply: ErasedContinuation) {}
}

class ApplyFrame {
  readonly _tag = "ApplyFrame"
  constructor(readonly apply: ErasedContinuation) {}
}

type Frame = MatchFrame | ApplyFrame

/**
 * Runs this computation with the specified initial state, returning either all
 * the failures that occurred, or the both the updated state and the result
 */
export const runAll =
  <S1>(s: S1) =>
  <S2, E, A>(ma: Sync.Sync<S1, S2, unknown, E, A>): Ex.Exit<E, readonly [S2, A]> => {
    const stack: Stack<Frame> = new Stack()
    let s0: unknown = s
    let result: unknown = null
    const environment = new Stack<unknown>()
    let failed = false
    let current = ma as Erased | undefined

    function unsafeUnwindStack() {
      let unwinding = true
      while (unwinding) {
        const next = stack.pop()

        if (next == null) {
          unwinding = false
        } else {
          if (next._tag === "MatchFrame") {
            unwinding = false
            stack.push(new ApplyFrame(next.failure))
          }
        }
      }
    }

    while (current != null) {
      try {
        while (current != null) {
          const currZ = current as Sync.Concrete
          switch (currZ._syncTag) {
            case Sync.Tag.Chain: {
              const nested = currZ.ma as Sync.Concrete
              const continuation = currZ.f
              switch (nested._syncTag) {
                case Sync.Tag.SucceedNow: {
                  current = continuation(nested.value)
                  break
                }
                case Sync.Tag.Succeed: {
                  current = continuation(nested.effect())
                  break
                }
                case Sync.Tag.Modify: {
                  const updated = nested.run(s0)
                  result = updated[0]
                  s0 = updated[1]
                  current = continuation(result)
                  break
                }
                default: {
                  current = nested
                  stack.push(new ApplyFrame(continuation))
                  break
                }
              }
              break
            }
            case Sync.Tag.Succeed: {
              result = currZ.effect()
              const nextInstruction = stack.pop()
              if (nextInstruction) {
                current = nextInstruction.apply(result)
              } else {
                current = undefined
              }
              break
            }
            case Sync.Tag.Defer: {
              current = currZ.make()
              break
            }
            case Sync.Tag.SucceedNow: {
              result = currZ.value
              const nextInstr = stack.pop()
              if (nextInstr) {
                current = nextInstr.apply(result)
              } else {
                current = undefined
              }
              break
            }
            case Sync.Tag.Fail: {
              unsafeUnwindStack()
              const nextInst = stack.pop()
              if (nextInst) {
                current = nextInst.apply(currZ.cause)
              } else {
                failed = true
                result = currZ.cause
                current = undefined
              }
              break
            }
            case Sync.Tag.Match: {
              current = currZ.z
              const state = s0
              stack.push(
                new MatchFrame(
                  (cause) =>
                    new Sync.Chain(new Sync.Modify(() => [undefined, state]), () =>
                      currZ.onFailure(cause as Cause<unknown>),
                    ),
                  currZ.onSuccess,
                ),
              )
              break
            }
            case Sync.Tag.Read: {
              current = currZ.asks(environment.peekOrElse({}))
              break
            }
            case Sync.Tag.Provide: {
              environment.push(currZ.environment)
              current = pipe(
                currZ.ma,
                Sync.matchCauseE(
                  (cause) =>
                    new Sync.Chain(
                      new Sync.SucceedNow(environment.pop()),
                      () => new Sync.Fail(cause),
                    ),
                  (a) =>
                    new Sync.Chain(
                      new Sync.SucceedNow(environment.pop()),
                      () => new Sync.SucceedNow(a),
                    ),
                ),
              )
              break
            }
            case Sync.Tag.Modify: {
              const updated = currZ.run(s0)
              s0 = updated[1]
              result = updated[0]
              const nextInst = stack.pop()
              if (nextInst) {
                current = nextInst.apply(result)
              } else {
                current = undefined
              }
              break
            }
          }
        }
      } catch (e) {
        current = new Sync.Fail(C.halt(e))
      }
    }

    if (failed) {
      return Ex.failCause(result as Cause<E>)
    }

    return Ex.succeed([s0 as S2, result as A])
  }

/**
 * Runs a computation, returning the result, or all failures that occurred
 */
export const run: <E, A>(z: Sync.Either<E, A>) => Ex.Exit<E, A> = flow(
  runAll(undefined),
  Ex.map(([, a]) => a),
)

/**
 * Runs a computation with the specified initial state, returning the result,
 * or all failures that occurred
 */
export const evalState = <S1>(
  s: S1,
): (<S2, E, A>(z: Sync.Sync<S1, S2, unknown, E, A>) => Ex.Exit<E, A>) =>
  flow(
    runAll(s),
    Ex.map(([, a]) => a),
  )

/**
 * Runs a computation with the specified initial state, returning the updated state,
 * or all failures that occurred
 */
export const execState = <S1>(
  s: S1,
): (<S2, E, A>(z: Sync.Sync<S1, S2, unknown, E, A>) => Ex.Exit<E, S2>) =>
  flow(
    runAll(s),
    Ex.map(([s]) => s),
  )

export class UnckeckedSyncError extends Error {
  constructor(name: string, readonly underlying: C.Cause<unknown>) {
    super(`Unchecked error arising from ${name}.`)
  }
}

/**
 * Runs a computation, returning the result
 *
 * @note This function will throw if a unchecked error is encountered, use with caution
 */
export const unsafeRun: <A>(z: Sync.IO<A>) => A = flow(
  run,
  Ex.match((cause) => {
    throw new UnckeckedSyncError("Sync.unsafeRunResult", cause)
  }, identity),
)

/**
 * Runs a computation with the specified initial state, returning the updated state
 *
 * @note This function will throw if a unchecked error is encountered, use with caution
 */
export const unsafeEvalState = <S1>(s: S1): (<S2, A>(z: Sync.State<S1, S2, A>) => A) =>
  flow(
    evalState(s),
    Ex.match((cause) => {
      throw new UnckeckedSyncError("Sync.unsafeRunState", cause)
    }, identity),
  )

/**
 * Runs a computation with the specified initial state, returning the updated state
 *
 * @note This function will throw if a unchecked error is encountered, use with caution
 */
export const unsafeExecState = <S1>(s: S1): (<S2, A>(z: Sync.State<S1, S2, A>) => S2) =>
  flow(
    execState(s),
    Ex.match((cause) => {
      throw new UnckeckedSyncError("Sync.unsafeRunState", cause)
    }, identity),
  )

/**
 * Runs a computation with the specified initial state, returning the result,
 * and the updated state
 *
 * @note This function will throw if a unchecked error is encountered, use with caution
 */
export const unsafeRunState = <S1>(
  s: S1,
): (<S2, A>(z: Sync.Sync<S1, S2, unknown, never, A>) => readonly [S2, A]) =>
  flow(
    runAll(s),
    Ex.match((cause) => {
      throw new UnckeckedSyncError("Sync.unsafeRun", cause)
    }, identity),
  )
