import type { ReaderEither } from "./definition"
import type { Applicative3 } from "fp-ts/Applicative"
import type { Apply3 } from "fp-ts/Apply"
import type { Functor3 } from "fp-ts/Functor"
import type { Monad3 } from "fp-ts/lib/Monad"
import type { Pointed3 } from "fp-ts/Pointed"

import { ap, chain, map, succeed } from "./api"

export const URI = "@simspace/xio/Sync/ReaderEither"
export type URI = typeof URI

declare module "fp-ts/HKT" {
  interface URItoKind3<R, E, A> {
    readonly [URI]: ReaderEither<R, E, A>
  }
}

export const Pointed: Pointed3<URI> = {
  URI,
  of: succeed,
}

export const Functor: Functor3<URI> = {
  URI,
  map: (fa, f) => map(f)(fa),
}

export const Apply: Apply3<URI> = {
  ...Functor,
  ap: (fab, fa) => ap(fa)(fab),
}

export const Applicative: Applicative3<URI> = {
  ...Apply,
  ...Pointed,
}

export const Monad: Monad3<URI> = {
  ...Applicative,
  chain: (fa, f) => chain(f)(fa),
}
