import type * as FId from "./FiberId"

import * as Equ from "@simspace/collections/Equatable"
import * as HM from "@simspace/collections/HashMap"
import * as L from "@simspace/collections/List"
import { pipe } from "fp-ts/lib/function"
import * as N from "fp-ts/number"
import * as O from "fp-ts/Option"

import * as FR from "./FiberRef"

export const FiberRefsTypeId = Symbol.for("@simspace/xio/FiberRefs")
export type FiberRefsTypeId = typeof FiberRefsTypeId

export interface FiberRefs {
  readonly _typeId: FiberRefsTypeId
}

/** @internal */
export class FiberRefs implements FiberRefs {
  readonly _typeId: FiberRefsTypeId = FiberRefsTypeId
  constructor(
    readonly fiberRefLocals: HM.HashMap<FR.FiberRef<unknown>, L.Cons<[FId.Runtime, unknown]>>,
  ) {}
}

/** @internal */
export function join(parent: FiberRefs, parentId: FId.Runtime, child: FiberRefs): FiberRefs {
  const parentFiberRefs = parent.fiberRefLocals
  const childFiberRefs = child.fiberRefLocals
  const fiberRefLocals = pipe(
    childFiberRefs,
    HM.reduceWithIndex(parentFiberRefs, (fiberRef, parentFiberRefs, childStack) => {
      const parentStack = pipe(
        parentFiberRefs,
        HM.get(fiberRef),
        O.getOrElse(() => L.empty()),
      )
      const ancestor = pipe(
        findAncestor(parentStack, childStack),
        O.getOrElse(() => FR.initial(fiberRef)),
      )
      const child = childStack.head[1]

      const patch = pipe(fiberRef, FR.diff(ancestor, child))

      const oldValue = pipe(
        L.head(parentStack),
        O.map((_) => _[1]),
        O.getOrElse(() => FR.initial(fiberRef)),
      )
      const newValue = pipe(fiberRef, FR.patch(patch, oldValue))

      if (oldValue === newValue) {
        return parentFiberRefs
      }

      let newStack: L.Cons<[FId.Runtime, unknown]>

      if (L.isEmpty(parentStack)) {
        newStack = L.cons([parentId, newValue])
      } else {
        const [parentFiberId] = parentStack.head
        if (Equ.strictEqualsUnknown(parentFiberId, parentId)) {
          newStack = L.cons([parentFiberId, newValue], parentStack.tail)
        } else {
          newStack = L.cons([parentId, newValue], parentStack)
        }
      }

      return pipe(parentFiberRefs, HM.set(fiberRef, newStack))
    }),
  )
  return new FiberRefs(fiberRefLocals)
}

function compareFiberId(left: FId.Runtime, right: FId.Runtime): number {
  const compare = N.Ord.compare(left.startTimeMillis, right.startTimeMillis)
  return compare === 0 ? N.Ord.compare(left.id, right.id) : compare
}

function findAncestor<A>(
  parentStack: L.List<[FId.Runtime, A]>,
  childStack: L.List<[FId.Runtime, A]>,
): O.Option<A> {
  while (true) {
    if (L.isNonEmpty(parentStack) && L.isNonEmpty(childStack)) {
      const [parentFiberId] = parentStack.head
      const [childFiberId, childValue] = childStack.head
      const compare = compareFiberId(parentFiberId, childFiberId)
      if (compare < 0) {
        childStack = childStack.tail
      } else if (compare > 0) {
        parentStack = parentStack.tail
      } else {
        return O.some(childValue)
      }
    } else {
      return O.none
    }
  }
}
