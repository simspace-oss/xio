import type { FiberId } from "../FiberId"
import type { Cause } from "./definition"
import type { Eq } from "fp-ts/Eq"
import type { Predicate } from "fp-ts/lib/Predicate"

import { EqStrict } from "@simspace/collections/Equatable"
import * as HS from "@simspace/collections/HashSet"
import * as V from "@simspace/collections/Vector"
import * as E from "fp-ts/Either"
import { fromEquals } from "fp-ts/Eq"
import { flow, pipe } from "fp-ts/function"
import * as O from "fp-ts/Option"
import * as Refinement from "fp-ts/Refinement"

import * as Ev from "../Eval"
import { Stack } from "../internal/Stack"
import { Both, Empty, eqCause, Fail, Halt, Interrupt, Then } from "./definition"

/**
 * Constructs an empty Cause
 */
export const empty = <E = never>(): Cause<E> => new Empty()

/**
 * Constructs a Cause that describes a failure with a value of type E
 */
export const fail = <E = never>(value: E): Cause<E> => new Fail(value)

/**
 * Constructs a Cause that describes an untyped failure
 */
export const halt = <E = never>(value: unknown): Cause<E> => new Halt(value)

/**
 * Constructs a Cause that describes an interruption
 */
export const interrupt = <E = never>(id: FiberId): Cause<E> => new Interrupt(id)

/**
 * Constructs a Cause that describes sequential failures
 */
export const then = <E = never, F = never>(left: Cause<E>, right: Cause<F>): Cause<E | F> =>
  new Then<E | F>(left, right)

/**
 * Constructs a Cause that describes concurrent failures
 */
export const both = <E = never, F = never>(left: Cause<E>, right: Cause<F>): Cause<E | F> =>
  new Both<E | F>(left, right)

export const isEmptyType = <E>(cause: Cause<E>): cause is Empty => cause._tag === "Empty"

export const isHaltType = <E>(cause: Cause<E>): cause is Halt => cause._tag === "Halt"

export const isFailType = <E>(cause: Cause<E>): cause is Fail<E> => cause._tag === "Fail"

export const isInterruptType = <E>(cause: Cause<E>): cause is Interrupt =>
  cause._tag === "Interrupt"

export const isBothType = <E>(cause: Cause<E>): cause is Both<E> => cause._tag === "Both"

export const isThenType = <E>(cause: Cause<E>): cause is Then<E> => cause._tag === "Then"

/**
 * Performs the catamorphism of `Cause`
 */
export const fold =
  <E, A>(cases: {
    Empty: () => A
    Fail: (value: E) => A
    Halt: (value: unknown) => A
    Interrupt: (id: FiberId) => A
    Then: (left: A, right: A) => A
    Both: (left: A, right: A) => A
  }) =>
  (cause: Cause<E>): A => {
    return Ev.unsafeRunEval(go(cause))

    function go(cause: Cause<E>): Ev.Eval<A> {
      return Ev.defer(() => {
        switch (cause._tag) {
          case "Empty": {
            return Ev.now(cases.Empty())
          }
          case "Fail": {
            return Ev.now(cases.Fail(cause.value))
          }
          case "Halt": {
            return Ev.now(cases.Halt(cause.value))
          }
          case "Interrupt": {
            return Ev.now(cases.Interrupt(cause.id))
          }
          case "Then": {
            return pipe(go(cause.left), Ev.zipWith(go(cause.right), cases.Then))
          }
          case "Both": {
            return pipe(go(cause.left), Ev.zipWith(go(cause.right), cases.Both))
          }
        }
      })
    }
  }

/**
 * Finds something in a Cause
 */
export const findMap =
  <E, B>(f: (a: Cause<E>) => O.Option<B>) =>
  (cause: Cause<E>): O.Option<B> => {
    const stack = new Stack<Cause<E>>()
    let current: Cause<E> | null = cause
    while (true) {
      const b = f(current)
      if (O.isSome(b)) {
        return b
      }

      switch (current._tag) {
        case "Then":
        case "Both": {
          stack.push(current.right)
          current = current.left
          break
        }
        default: {
          if (stack.hasNext) {
            current = stack.pop()!
            break
          } else {
            return O.none
          }
        }
      }
    }
  }

/**
 * Folds over a Cause to statefully compute a value.
 */
export const foldLeft =
  <A, B>(b: B, f: (b: B, a: Cause<A>) => O.Option<B>) =>
  (cause: Cause<A>): B => {
    const stack = new Stack<Cause<A>>()
    let current: Cause<A> | null = cause
    let acc = b
    while (current !== null) {
      acc = pipe(
        f(acc, current),
        O.getOrElse(() => acc),
      )
      switch (current._tag) {
        case "Both":
        case "Then": {
          stack.push(current.right)
          current = current.left
          break
        }
        default: {
          if (stack.hasNext) {
            current = stack.pop()!
          } else {
            current = null
          }
          break
        }
      }
    }
    return acc
  }

/**
 * Determines if a Cause contains or is equal to the specified cause.
 */
export const contains =
  <E1>(that: Cause<E1>) =>
  <E>(cause: Cause<E>): boolean =>
    cause === that ||
    pipe(
      cause,
      foldLeft(false as boolean, (acc, cause) =>
        acc ? O.some(acc) : O.some(eqCause(EqStrict)(cause, that)),
      ),
    )

/**
 * Transforms the error type of this Cause with the specified function
 */
export const map = <A, B>(f: (a: A) => B): ((fa: Cause<A>) => Cause<B>) =>
  fold({
    Empty: empty,
    Fail: flow(f, fail),
    Halt: halt,
    Interrupt: interrupt,
    Then: then,
    Both: both,
  })

/**
 * Transforms each error value in this cause to a new cause with the specified
 * function and then flattens the nested causes into a single cause.
 */
export const chain = <E, A>(f: (e: E) => Cause<A>): ((cause: Cause<E>) => Cause<A>) =>
  fold({
    Empty: empty,
    Fail: f,
    Halt: halt,
    Interrupt: interrupt,
    Then: then,
    Both: both,
  })

/**
 * Determines whether or not the `Cause` is empty.
 *
 * A `Cause` can be empty if it is of the constructor `Empty`,
 * or if all of its constituents are of the constructor `Empty`.
 */
export const isEmpty: <E>(cause: Cause<E>) => boolean = fold({
  Empty: () => true,
  Fail: () => false,
  Halt: () => false,
  Interrupt: () => false,
  Then: (l, r) => l && r,
  Both: (l, r) => l && r,
})

/**
 * Produces a list of all recoverable errors `E` in the `Cause`.
 */
export const failures: <E>(cause: Cause<E>) => V.Vector<E> = foldLeft(
  V.empty(),
  (failures, cause) =>
    pipe(
      cause,
      O.fromPredicate(isFailType),
      O.map((cause) => V.append(cause.value)(failures)),
    ),
)

/**
 * Finds the first typed failure in a Cause and returns it, or None if
 * the Cause does not contain a typed failure
 */
export const failureOption: <E>(cause: Cause<E>) => O.Option<E> = findMap(
  flow(
    O.fromPredicate(isFailType),
    O.map((c) => c.value),
  ),
)

export const isFailure: <E>(cause: Cause<E>) => boolean = flow(failureOption, O.isSome)

/**
 * Finds the first typed failure in a Cause and returns it on the left,
 * or returns the original Cause on the right, which is known to only contain
 * Halt or Interrupt
 */
export const failureOrCause = <E>(cause: Cause<E>): E.Either<E, Cause<never>> =>
  pipe(
    failureOption(cause),
    O.match(() => E.right(cause as Cause<never>), E.left),
  )

/**
 * Discards all typed failures kept on this `Cause`.
 */
export const stripFailures: <E>(cause: Cause<E>) => Cause<never> = fold({
  Empty: empty,
  Fail: () => empty(),
  Halt: halt,
  Interrupt: interrupt,
  Then: then,
  Both: both,
})

/**
 * Extracts a list of non-recoverable errors from the `Cause`.
 */
export const defects: <E>(cause: Cause<E>) => V.Vector<unknown> = foldLeft(
  V.empty(),
  (defects, cause) =>
    pipe(
      cause,
      O.fromPredicate(isHaltType),
      O.map((cause) => V.append(cause.value)(defects)),
    ),
)

/**
 * Finds the first FiberId associated with an interruption, or `None` if the Cause
 * does not describe an interruption
 */
export const interruptOption: <E>(cause: Cause<E>) => O.Option<FiberId> = findMap(
  flow(
    O.fromPredicate(isInterruptType),
    O.map((cause) => cause.id),
  ),
)

/**
 * Determines if the `Cause` contains an interruption.
 */
export const isInterrupted: <E>(cause: Cause<E>) => boolean = flow(interruptOption, O.isSome)

/**
 * Determines if the `Cause` contains only interruptions and not any `Halt` or
 * `Fail` causes.
 */
export const isInterruptedOnly: <E>(cause: Cause<E>) => boolean = flow(
  findMap(O.fromPredicate(pipe(isHaltType, Refinement.or(isFailType), Refinement.not))),
  O.match(
    () => true,
    () => false,
  ),
)

/**
 * Returns a set of interruptors, fibers that interrupted the fiber described
 * by this `Cause`.
 */
export const interruptors: <E>(cause: Cause<E>) => HS.HashSet<FiberId> = foldLeft(
  HS.makeDefault(),
  (acc, cause) =>
    pipe(
      cause,
      O.fromPredicate(isInterruptType),
      O.map((cause) => pipe(acc, HS.add(cause.id))),
    ),
)

export const haltOption: <E>(cause: Cause<E>) => O.Option<unknown> = findMap(
  flow(
    O.fromPredicate(isHaltType),
    O.map((cause) => cause.value),
  ),
)

export const isHalted: <E>(cause: Cause<E>) => boolean = flow(haltOption, O.isSome)

/**
 * Remove all `Fail` and `Interrupt` nodes from this `Cause`, return only
 * `Halt` defects.
 */
export const keepDefects = <E>(cause: Cause<E>): O.Option<Cause<never>> =>
  pipe(
    cause,
    fold<E, O.Option<Cause<never>>>({
      Empty: () => O.none,
      Fail: () => O.none,
      Halt: (defect) => O.some(halt(defect)),
      Interrupt: () => O.none,
      Both: (left, right) => {
        if (O.isSome(left) && O.isSome(right)) {
          return O.some(both(left.value, right.value))
        } else if (O.isSome(left)) {
          return O.some(left.value)
        } else if (O.isSome(right)) {
          return O.some(right.value)
        } else {
          return O.none
        }
      },
      Then: (left, right) => {
        if (O.isSome(left) && O.isSome(right)) {
          return O.some(then(left.value, right.value))
        } else if (O.isSome(left)) {
          return O.some(left.value)
        } else if (O.isSome(right)) {
          return O.some(right.value)
        } else {
          return O.none
        }
      },
    }),
  )

/**
 * Remove all `Halt` causes that the specified predicate is defined at,
 * returning `Some` with the remaining causes or `None` if there are no
 * remaining causes.
 */
export const stripSomeDefects =
  (p: Predicate<unknown>) =>
  <E>(cause: Cause<E>): O.Option<Cause<E>> =>
    pipe(
      cause,
      fold<E, O.Option<Cause<E>>>({
        Empty: () => O.some(empty()),
        Fail: flow(fail, O.some),
        Halt: (defect) => (p(defect) ? O.none : O.some(halt(defect))),
        Interrupt: flow(interrupt, O.some),
        Both: (left, right) => {
          if (O.isSome(left) && O.isSome(right)) {
            return O.some(both(left.value, right.value))
          } else if (O.isSome(left)) {
            return O.some(left.value)
          } else if (O.isSome(right)) {
            return O.some(right.value)
          } else {
            return O.none
          }
        },
        Then: (left, right) => {
          if (O.isSome(left) && O.isSome(right)) {
            return O.some(then(left.value, right.value))
          } else if (O.isSome(left)) {
            return O.some(left.value)
          } else if (O.isSome(right)) {
            return O.some(right.value)
          } else {
            return O.none
          }
        },
      }),
    )

export function getEq<A>(E: Eq<A>): Eq<Cause<A>> {
  return fromEquals(eqCause(E))
}
