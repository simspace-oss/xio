import type { FiberId } from "../FiberId"
import type { Eq } from "fp-ts/Eq"

import * as Equ from "@simspace/collections/Equatable"

import { hasTypeId } from "../internal/util"

export const CauseTypeId = Symbol.for("@simspace/xio/Cause")
export type CauseTypeId = typeof CauseTypeId

/**
 * `Cause` describes an error of type `E`
 */
export type Cause<E> = Empty | Fail<E> | Halt | Interrupt | Then<E> | Both<E>

export class Empty {
  readonly [CauseTypeId]: CauseTypeId = CauseTypeId
  readonly _tag = "Empty";
  [Equ.equatableSymbol](that: unknown): boolean {
    return isCause(that) && eqCause(Equ.EqStrict)(this, that)
  }
}

/**
 * `Fail` describes an error that has only one `E` associated with it
 */
export class Fail<E> {
  readonly [CauseTypeId]: CauseTypeId = CauseTypeId
  readonly _tag = "Fail"
  constructor(readonly value: E) {}
  [Equ.equatableSymbol](that: unknown): boolean {
    return isCause(that) && eqCause(Equ.EqStrict)(this, that)
  }
}

/**
 * `Halt` describes an unchecked error, i.e. an error caused by an uncaught exception
 */
export class Halt {
  readonly [CauseTypeId]: CauseTypeId = CauseTypeId
  readonly _tag = "Halt"
  constructor(readonly value: unknown) {}
  [Equ.equatableSymbol](that: unknown): boolean {
    return isCause(that) && eqCause(Equ.EqStrict)(this, that)
  }
}

/**
 * `Interrupt` describes the case of a `Fiber` being interrupted
 */
export class Interrupt {
  readonly [CauseTypeId]: CauseTypeId = CauseTypeId
  readonly _tag = "Interrupt"
  constructor(readonly id: FiberId) {}
  [Equ.equatableSymbol](that: unknown): boolean {
    return isCause(that) && eqCause(Equ.EqStrict)(this, that)
  }
}

/**
 * `Then` describes sequential errors
 */
export class Then<E> {
  readonly [CauseTypeId]: CauseTypeId = CauseTypeId
  readonly _tag = "Then"
  constructor(readonly left: Cause<E>, readonly right: Cause<E>) {}
  [Equ.equatableSymbol](that: unknown): boolean {
    return isCause(that) && eqCause(Equ.EqStrict)(this, that)
  }
}

/**
 * `Both` describes concurrent errors
 */
export class Both<E> {
  readonly [CauseTypeId]: CauseTypeId = CauseTypeId
  readonly _tag = "Both"
  constructor(readonly left: Cause<E>, readonly right: Cause<E>) {}
  [Equ.equatableSymbol](that: unknown): boolean {
    return isCause(that) && eqCause(Equ.EqStrict)(this, that)
  }
}

export function isCause(u: unknown): u is Cause<unknown> {
  return hasTypeId<unknown, Cause<unknown>>(u, CauseTypeId)
}

// Eq

const eqEmpty =
  <A>(E: Eq<A>) =>
  (l: Cause<A>, r: Cause<A>): boolean => {
    if (l._tag === "Then" && l.right._tag === "Empty") {
      return eqCause(E)(l.left, r)
    }
    if (l._tag === "Then" && l.left._tag === "Empty") {
      return eqCause(E)(l.right, r)
    }
    if (l._tag === "Both" && l.right._tag === "Empty") {
      return eqCause(E)(l.left, r)
    }
    if (l._tag === "Both" && l.left._tag === "Empty") {
      return eqCause(E)(l.right, r)
    }
    return false
  }

export const eqCause =
  <A>(E: Eq<A>) =>
  (l: Cause<A>, r: Cause<A>): boolean => {
    if (r === l) {
      return true
    }
    switch (l._tag) {
      case "Empty": {
        switch (r._tag) {
          case "Empty":
            return true
          case "Then":
          case "Both":
            return eqCause(E)(l, r.left) && eqCause(E)(l, r.right)
          default:
            return false
        }
      }
      case "Fail": {
        switch (r._tag) {
          case "Fail":
            return E.equals(l.value, r.value)
          case "Then":
          case "Both":
            return sym(eqEmpty(E))(l, r)
          default:
            return false
        }
      }
      case "Halt": {
        switch (r._tag) {
          case "Halt":
            return Equ.strictEqualsUnknown(l.value, r.value)
          case "Then":
          case "Both":
            return sym(eqEmpty(E))(l, r)
          default:
            return false
        }
      }
      case "Interrupt": {
        switch (r._tag) {
          case "Interrupt":
            return Equ.strictEqualsUnknown(l.id, r.id)
          case "Then":
          case "Both":
            return sym(eqEmpty(E))(l, r)
          default:
            return false
        }
      }
      case "Then":
        return (
          eqThen(E)(l, r) ||
          sym(assocThen(E))(l, r) ||
          sym(distThen(E))(l, r) ||
          sym(eqEmpty(E))(l, r)
        )
      case "Both":
        return (
          eqBoth(E)(l, r) ||
          sym(assocBoth(E))(l, r) ||
          sym(distBoth(E))(l, r) ||
          commBoth(E)(l, r) ||
          sym(eqEmpty(E))(l, r)
        )
    }
  }

const eqThen =
  <A>(E: Eq<A>) =>
  (l: Cause<A>, r: Cause<A>): boolean => {
    if (l._tag === "Then" && r._tag === "Then") {
      return eqCause(E)(l.left, r.left) && eqCause(E)(l.right, r.right)
    }
    return false
  }

const assocThen =
  <A>(E: Eq<A>) =>
  (l: Cause<A>, r: Cause<A>): boolean => {
    if (
      l._tag === "Then" &&
      l.left._tag === "Then" &&
      r._tag === "Then" &&
      r.right._tag === "Then"
    ) {
      return (
        eqCause(E)(l.left.left, r.left) &&
        eqCause(E)(l.left.right, r.right.left) &&
        eqCause(E)(l.right, r.right.right)
      )
    }
    return false
  }

const distThen =
  <A>(E: Eq<A>) =>
  (l: Cause<A>, r: Cause<A>): boolean => {
    if (l._tag === "Then" && l.right._tag === "Both" && r._tag === "Both") {
      return (
        eqCause(E)(new Then(l.left, l.right.right), r.left) &&
        eqCause(E)(new Then(l.left, l.right.right), r.right)
      )
    }
    if (l._tag === "Then" && l.left._tag === "Both" && r._tag === "Both") {
      return (
        eqCause(E)(new Then(l.left.left, l.right), r.right) &&
        eqCause(E)(new Then(l.left.right, l.right), r.right)
      )
    }
    return false
  }

const eqBoth =
  <A>(E: Eq<A>) =>
  (l: Cause<A>, r: Cause<A>): boolean => {
    if (l._tag === "Both" && r._tag === "Both") {
      return eqCause(E)(l.left, r.left) && eqCause(E)(l.right, r.right)
    }
    return false
  }

const assocBoth =
  <A>(E: Eq<A>) =>
  (l: Cause<A>, r: Cause<A>): boolean => {
    if (
      l._tag === "Both" &&
      l.left._tag === "Both" &&
      r._tag === "Both" &&
      r.right._tag === "Both"
    ) {
      return (
        eqCause(E)(l.left.left, r.left) &&
        eqCause(E)(l.left.right, r.right.left) &&
        eqCause(E)(l.right, r.right.right)
      )
    }
    return false
  }

const distBoth =
  <A>(E: Eq<A>) =>
  (l: Cause<A>, r: Cause<A>): boolean => {
    if (l._tag === "Both" && r._tag === "Then" && r.right._tag === "Both") {
      return (
        eqCause(E)(l.left, new Then(r.left, r.right.left)) &&
        eqCause(E)(l.right, new Then(r.left, r.right.right))
      )
    }
    if (l._tag === "Both" && r._tag === "Then" && r.left._tag === "Both") {
      return (
        eqCause(E)(l.left, new Then(r.left.left, r.right)) &&
        eqCause(E)(l.right, new Then(r.left.right, r.right))
      )
    }
    return false
  }

const commBoth =
  <A>(E: Eq<A>) =>
  (l: Cause<A>, r: Cause<A>): boolean => {
    if (l._tag === "Both" && r._tag === "Both") {
      return eqCause(E)(l.left, r.right) && eqCause(E)(l.right, r.left)
    }
    return false
  }

function sym<A>(f: (x: Cause<A>, y: Cause<A>) => boolean): (x: Cause<A>, y: Cause<A>) => boolean {
  return (x, y) => f(x, y) || f(y, x)
}
