import type { Cause } from "./definition"

import { isObject } from "@simspace/collections/internal/guards"
import { pipe } from "fp-ts/function"
import * as O from "fp-ts/Option"
import * as RA from "fp-ts/ReadonlyArray"
import * as RNEA from "fp-ts/ReadonlyNonEmptyArray"

import * as FId from "../FiberId"
import { fold } from "./api"

class Failure {
  readonly _tag = "Failure"
  constructor(readonly lines: ReadonlyArray<string>) {}
}

class Parallel {
  readonly _tag = "Parallel"
  constructor(readonly all: ReadonlyArray<Sequential>) {}
}

class Sequential {
  readonly _tag = "Sequential"
  constructor(readonly all: ReadonlyArray<Step>) {}
}

type Step = Parallel | Failure

type Segment = Sequential | Parallel | Failure

const prefixBlock = (
  values: ReadonlyArray<string>,
  p1: string,
  p2: string,
): ReadonlyArray<string> =>
  RA.isNonEmpty(values)
    ? pipe(RNEA.unprepend(values), ([head, tail]) => [
        `${p1}${head}`,
        ...tail.map((s) => `${p2}${s}`),
      ])
    : []

const renderInterrupt = (id: FId.FiberId): Sequential =>
  new Sequential([new Failure([`An interrupt was produced by ${FId.render(id)}.`])])

const lines = (s: string) => s.split("\n").map((s) => s.replace("\r", ""))

const renderError = (error: Error): ReadonlyArray<string> => lines(error.stack ?? String(error))

const renderHalt = (error: ReadonlyArray<string>): Sequential =>
  new Sequential([new Failure(["An unchecked error was produced.", "", ...error])])

const renderFailure = (error: ReadonlyArray<string>): Sequential =>
  new Sequential([new Failure(["A checked error was not handled.", "", ...error])])

const renderToStringLines = (u: unknown): ReadonlyArray<string> => {
  if (isObject(u) && "toString" in u && typeof u.toString === "function") {
    return lines(u.toString())
  }
  return lines(JSON.stringify(u, null, 2))
}

const causeToSequential = fold({
  Empty: () => new Sequential([]),
  Fail: (value) =>
    value instanceof Error
      ? renderFailure(renderError(value))
      : renderFailure(renderToStringLines(value)),
  Halt: (value) =>
    value instanceof Error
      ? renderHalt(renderError(value))
      : renderHalt(renderToStringLines(value)),
  Interrupt: renderInterrupt,
  Then: (left, right) => new Sequential(left.all.concat(right.all)),
  Both: (left, right) => new Sequential([new Parallel([left, right])]),
})

const format = (segment: Segment): ReadonlyArray<string> => {
  switch (segment._tag) {
    case "Failure":
      return prefixBlock(segment.lines, "-", " ")
    case "Parallel":
      return [
        "══╦".repeat(segment.all.length - 1) + "══╗",
        ...segment.all.reduceRight(
          (acc, current) => [
            ...prefixBlock(acc, "  ║", "  ║"),
            ...prefixBlock(format(current), "  ", "  "),
          ],
          [] as ReadonlyArray<string>,
        ),
      ]
    case "Sequential":
      return segment.all.flatMap((seq) => ["║", ...prefixBlock(format(seq), "╠", "║"), "▼"])
  }
}

export const render = (cause: Cause<unknown>): string => {
  const seq = causeToSequential(cause)
  if (seq.all.length === 1 && seq.all[0]._tag === "Failure") {
    return seq.all[0].lines.join("\n")
  }

  return pipe(
    seq,
    format,
    RA.updateAt(0, "╥"),
    O.getOrElseW(() => []),
  ).join("\n")
}
