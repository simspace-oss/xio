import type { Has, Tag } from "./Tag"
import type { Union } from "ts-toolbelt"

/**
 * Takes an environment, and an item to add to that environment, and returns their union as an intersection type
 */
export const add =
  <A>(a: A) =>
  <Environment>(environment: Environment): Environment & A =>
    Object.assign({}, environment, a)

/**
 * Takes an environment, and a service to add to that environment, and returns their union as an intersection type
 */
export const addService =
  <S>(tag: Tag<S>, service: S) =>
  <Environment>(environment: Environment): Environment & Has<S> =>
    Object.assign({}, environment, tag.of(service))

/**
 * Takes a list of environments, and returns the union of those environments as an intersection type
 */
export const all = <Environments extends ReadonlyArray<unknown>>(
  ...environments: Environments
): Union.IntersectOf<Environments[number]> => {
  const out = {}
  for (const env of environments) {
    Object.assign(out, env)
  }
  return out as any
}
