import type * as Ref from "./internal/Ref"
import type { State } from "./internal/State"

/**
 * An asynchronous semaphore, which is a generalization of a mutex. Semaphores
 * have a certain number of permits, which can be held and released
 * concurrently by different parties. Attempts to acquire more permits than
 * available result in the acquiring fiber being suspended until the specified
 * number of permits become available.
 */
export type Semaphore = Ref.Ref<State>
