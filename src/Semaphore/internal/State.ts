import type * as F from "../../Future"
import type * as XIO from "./XIO"
import type * as Q from "@simspace/collections/Queue"
import type * as E from "fp-ts/Either"

export class Acquisition {
  constructor(readonly awaitAcquire: XIO.UIO<void>, readonly release: XIO.UIO<void>) {}
}

export type State = E.Either<Q.Queue<Entry>, number>

function State(_: E.Either<Q.Queue<Entry>, number>): State {
  return _
}

type Entry = [F.Future<never, void>, number]
