import type { Semaphore } from "./definition"
import type { State } from "./internal/State"

import * as Q from "@simspace/collections/Queue"
import * as E from "fp-ts/Either"
import { flow, identity, pipe, tuple } from "fp-ts/function"
import * as O from "fp-ts/Option"

import * as F from "../Future"
import * as Ref from "./internal/Ref"
import { Acquisition } from "./internal/State"
import * as XIO from "./internal/XIO"

export const unsafeMake = (permits: number): Semaphore => Ref.unsafeMake(E.right(permits))

/**
 * Constructs a new Semaphore with the specified number of permits
 */
export const make = (permits: number): XIO.UIO<Semaphore> => XIO.fromIO(() => unsafeMake(permits))

/**
 * The number of permits currently available
 */
export const available = (s: Semaphore): XIO.UIO<number> =>
  pipe(Ref.get(s), XIO.map(E.match(() => 0, identity)))

/**
 * Acquires a permit, executes the action, and releases the permit right after.
 */
export const withPermit = (s: Semaphore): (<R, E, A>(xio: XIO.XIO<R, E, A>) => XIO.XIO<R, E, A>) =>
  withPermits(s, 1)

/**
 * Acquires `n` permits, executes the action and releases the permits right after.
 */
export const withPermits =
  (s: Semaphore, n: number) =>
  <R, E, A>(xio: XIO.XIO<R, E, A>): XIO.XIO<R, E, A> =>
    XIO.bracket(
      prepare(s, n),
      (acquisition) =>
        pipe(
          acquisition.awaitAcquire,
          XIO.chain(() => xio),
        ),
      (acquisition) => acquisition.release,
    )

function prepare(semaphore: Semaphore, permits: number): XIO.UIO<Acquisition> {
  if (permits === 0) {
    return XIO.succeed(new Acquisition(XIO.unit, XIO.unit))
  } else {
    return pipe(
      F.make<never, void>(),
      XIO.chain((future) =>
        pipe(
          semaphore,
          Ref.modify(
            E.match(
              (queue) =>
                tuple(
                  new Acquisition(F.await(future), restore(semaphore, future, permits)),
                  E.left(pipe(queue, Q.enqueue(tuple(future, permits)))),
                ),
              (permits0) => {
                if (permits0 >= permits) {
                  return tuple(
                    new Acquisition(XIO.unit, releaseN0(semaphore, permits)),
                    E.right(permits0 - permits),
                  )
                } else {
                  return tuple(
                    new Acquisition(F.await(future), restore(semaphore, future, permits)),
                    E.left(Q.single(tuple(future, permits - permits0))),
                  )
                }
              },
            ),
          ),
        ),
      ),
    )
  }
}

function restore(sem: Semaphore, future: F.Future<never, void>, permits: number): XIO.UIO<void> {
  return pipe(
    sem,
    Ref.modify(
      E.match(
        (queue) =>
          pipe(
            queue,
            Q.find(([future0]) => future === future0),
            O.match(
              () => tuple(releaseN0(sem, permits), E.left(queue)),
              ([_, permits0]) =>
                tuple(
                  releaseN0(sem, permits - permits0),
                  pipe(
                    queue,
                    Q.filter(([future0]) => future0 !== future),
                    E.left,
                  ),
                ),
            ),
          ),
        (permits0) => tuple(XIO.unit, E.right(permits + permits0)),
      ),
    ),
    XIO.flatten,
  )
}

function releaseN0(sem: Semaphore, toRelease: number): XIO.UIO<void> {
  return pipe(sem, Ref.modify(releaseN0Loop(toRelease, XIO.unit)), XIO.uninterruptible)
}

const releaseN0Loop =
  (permits: number, acc: XIO.UIO<void>) =>
  (state: State): [XIO.UIO<void>, State] => {
    return pipe(
      state,
      E.match(
        flow(
          Q.dequeue,
          O.match(
            () => tuple(acc, E.right(permits)),
            ([[future, permits0], queue]) => {
              if (permits > permits0) {
                return releaseN0Loop(
                  permits - permits0,
                  pipe(
                    acc,
                    XIO.chainFirst(() => F.succeed<void>(undefined)(future)),
                  ),
                )(E.left(queue))
              } else if (permits === permits0) {
                return tuple(
                  pipe(
                    acc,
                    XIO.chainFirst(() => F.succeed<void>(undefined)(future)),
                  ),
                  E.left(queue),
                )
              } else {
                return tuple(acc, pipe(queue, Q.prepend(tuple(future, permits0 - permits)), E.left))
              }
            },
          ),
        ),
        (permits0) => tuple(acc, E.right(permits + permits0)),
      ),
    )
  }
