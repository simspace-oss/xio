import * as E from "fp-ts/Either"

import { Clock } from "./definition"
import * as XIO from "./internal/ClockLive-XIO"

export const ClockLive: Clock = new (class extends Clock {
  readonly currentTime: XIO.UIO<number> = XIO.fromIO(() => Date.now())
  sleep(duration: number): XIO.UIO<void> {
    return XIO.asyncInterrupt((cb) => {
      const handle = setTimeout(() => {
        cb(XIO.unit)
      }, duration)
      return E.left(
        XIO.fromIO(() => {
          clearTimeout(handle)
        }),
      )
    })
  }
})()
