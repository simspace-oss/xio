import * as XIO from "./internal/XIO"

export const currentTime: XIO.UIO<number> = XIO.clockWith((clock) => clock.currentTime)

export const sleep = (duration: number): XIO.UIO<void> =>
  XIO.clockWith((clock) => clock.sleep(duration))
