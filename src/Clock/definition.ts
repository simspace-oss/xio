import type * as XIO from "./internal/XIO"

import { tag } from "../Tag"

export abstract class Clock {
  abstract readonly currentTime: XIO.UIO<number>
  abstract sleep(duration: number): XIO.UIO<void>
}

export const ClockTag = tag<Clock>("@simspace/xio/Clock/Tag")
