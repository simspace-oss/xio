export const enum Tag {
  Sequential,
  Concurrent,
  ConcurrentBounded,
}

export interface Sequential {
  readonly _tag: Tag.Sequential
}

export const Sequential: ExecutionStrategy = {
  _tag: Tag.Sequential,
}

export interface Concurrent {
  readonly _tag: Tag.Concurrent
}

export const Concurrent: ExecutionStrategy = {
  _tag: Tag.Concurrent,
}

export interface ConcurrentBounded {
  readonly _tag: Tag.ConcurrentBounded
  readonly fiberBound: number
}

export const ConcurrentBounded = (fiberBound: number): ExecutionStrategy => ({
  _tag: Tag.ConcurrentBounded,
  fiberBound,
})

export type ExecutionStrategy = Sequential | Concurrent | ConcurrentBounded

export const match_ = <A>(
  es: ExecutionStrategy,
  cases: { Sequential: () => A; Concurrent: () => A; ConcurrentBounded: (bound: number) => A },
): A => {
  switch (es._tag) {
    case Tag.Sequential:
      return cases.Sequential()
    case Tag.Concurrent:
      return cases.Concurrent()
    case Tag.ConcurrentBounded:
      return cases.ConcurrentBounded(es.fiberBound)
  }
}

export const match =
  <A>(cases: {
    Sequential: () => A
    Concurrent: () => A
    ConcurrentBounded: (bound: number) => A
  }) =>
  (es: ExecutionStrategy): A =>
    match_(es, cases)
