import type { Clock } from "./Clock/definition"
import type { Has } from "./Tag"

import { ClockLive } from "./Clock/ClockLive"
import { ClockTag } from "./Clock/definition"
import * as FR from "./FiberRef/internal/FiberRef"

export type XIOEnv = Has<Clock> // & Has<Random> & Has<Console>

export const live: XIOEnv = ClockTag.of(ClockLive)

export const services = FR.unsafeMake(live)
