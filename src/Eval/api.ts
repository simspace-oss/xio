import type { Eval } from "./definition"

import { pipe } from "fp-ts/function"

import { Chain, Defer, Now } from "./definition"

export function now<A>(value: A): Eval<A> {
  return new Now(value)
}

export function defer<A>(value: () => Eval<A>): Eval<A> {
  return new Defer(value)
}

export function chain<A, B>(f: (a: A) => Eval<B>) {
  return (ma: Eval<A>): Eval<B> => new Chain(ma, f)
}

export function map<A, B>(f: (a: A) => B): (fa: Eval<A>) => Eval<B> {
  return chain((a) => now(f(a)))
}

export function zipWith<A, B, C>(fb: Eval<B>, f: (a: A, b: B) => C): (fa: Eval<A>) => Eval<C> {
  return chain((a) =>
    pipe(
      fb,
      map((b) => f(a, b)),
    ),
  )
}
