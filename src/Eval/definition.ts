export const EvalTypeId = Symbol.for("@simspace/xio/Eval")
export type EvalTypeId = typeof EvalTypeId

export const enum Tag {
  Now,
  Defer,
  Chain,
}

export abstract class Eval<A> {
  readonly [EvalTypeId]: EvalTypeId = EvalTypeId
  readonly _A!: () => A
}

/**
 * @internal
 */
export class Now<A> extends Eval<A> {
  readonly _tag = Tag.Now
  constructor(readonly value: A) {
    super()
  }
}

/**
 * @internal
 */
export class Defer<A> extends Eval<A> {
  readonly _tag = Tag.Defer
  constructor(readonly ma: () => Eval<A>) {
    super()
  }
}

/**
 * @internal
 */
export class Chain<A, B> extends Eval<B> {
  readonly _tag = Tag.Chain
  constructor(readonly ma: Eval<A>, readonly f: (a: A) => Eval<B>) {
    super()
  }
}

/**
 * @internal
 */
export type Concrete = Now<unknown> | Defer<unknown> | Chain<unknown, unknown>
