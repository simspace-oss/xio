import type { Concrete, Eval } from "./definition"

import { Stack } from "../internal/Stack"
import { Tag } from "./definition"

const UNSET = Symbol()

export function unsafeRunEval<A>(effect: Eval<A>): A {
  const stack = new Stack<(a: unknown) => Eval<unknown>>()
  let current = effect as Concrete | undefined
  let value: unknown = UNSET

  while (current !== undefined) {
    switch (current._tag) {
      case Tag.Chain: {
        const nested = current.ma as Concrete
        const continuation = current.f
        switch (nested._tag) {
          case Tag.Now: {
            current = continuation(nested.value) as Concrete
            break
          }
          default: {
            current = nested
            stack.push(continuation)
            break
          }
        }
        break
      }
      case Tag.Now: {
        value = current.value
        if (stack.hasNext) {
          current = stack.pop()!(value) as Concrete
        } else {
          current = undefined
        }
        break
      }
      case Tag.Defer: {
        current = current.ma() as Concrete
        break
      }
    }
  }

  if (value === UNSET) {
    throw new Error("BUG: value in unsafeRunEval was never set")
  }

  return value as A
}
