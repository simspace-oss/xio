import * as Equ from "@simspace/collections/Equatable"
import * as H from "@simspace/collections/Hashable"

import { hasTypeId } from "./internal/util"

/**
 * A type-level symbol for containing the Has<S> phantom property
 */
export declare const HasSymbol: unique symbol

export interface Has<S> {
  [HasSymbol]: {
    _S: () => S
  }
}

export const TagTypeId = Symbol.for("@simspace/xio/Tag")
export type TagTypeId = typeof TagTypeId

export class Tag<S> {
  readonly [TagTypeId]: TagTypeId = TagTypeId
  readonly _A!: (_: never) => S

  readonly id: symbol
  constructor(private descriptor: string) {
    this.id = Symbol.for(descriptor)
  }

  of(service: S): Has<S> {
    return { [this.id]: service } as unknown as Has<S>
  }

  unsafeGet<R extends Has<S>>(r: R): S {
    if (this.id in r) {
      return (r as any)[this.id]
    }
    throw new Error(`Defect in environment: Could not find tag ${this.descriptor}`)
  }

  [H.hashableSymbol](): number {
    return H.combine(H.symbol(this[TagTypeId]), H.symbol(this.id))
  }

  [Equ.equatableSymbol](that: unknown): boolean {
    return isTag(that) && this.id === that.id
  }
}

export function isTag(u: unknown): u is Tag<unknown> {
  return hasTypeId(u, TagTypeId)
}

export function tag<S>(id: string): Tag<S> {
  return new Tag<S>(id)
}
