import type { Ref } from "../definition"

import { RefTypeId } from "../definition"
import * as XIO from "./XIO"

/**
 * @internal
 */
export class Atomic<A> implements Ref<A> {
  readonly [RefTypeId]: RefTypeId = RefTypeId
  readonly _tag = "Atomic"
  readonly _A!: () => A
  constructor(public value: A) {}

  get(): XIO.UIO<A> {
    return XIO.fromIO(() => this.value)
  }

  set(a: A): XIO.UIO<void> {
    return XIO.fromIO(() => {
      this.value = a
    })
  }

  modify<B>(f: (a: A) => readonly [B, A]): XIO.UIO<B> {
    return XIO.fromIO(() => {
      const result = f(this.value)
      this.value = result[1]
      return result[0]
    })
  }
}

/**
 * @internal
 */
export function concreteRef<A>(ref: Ref<A>): asserts ref is Atomic<A> {
  //
}
