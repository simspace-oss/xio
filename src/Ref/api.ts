import type { Ref } from "./definition"

import { identity, pipe } from "fp-ts/function"
import * as O from "fp-ts/Option"

import { Atomic, concreteRef } from "./internal/Atomic"
import * as XIO from "./internal/XIO"

/**
 * Creates a new `Ref` with the specified value.
 */
export const make = <A>(a: A): XIO.UIO<Ref<A>> => XIO.fromIO(() => unsafeMake(a))

/**
 * Reads the value from the `Ref`
 */
export const get = <A>(ref: Ref<A>): XIO.UIO<A> => {
  concreteRef(ref)
  return ref.get()
}

/**
 * Writes a new value to the `Ref`
 */
export const set =
  <A, B extends A>(a: B) =>
  (ref: Ref<A>): XIO.UIO<void> => {
    concreteRef(ref)
    return ref.set(a)
  }

/**
 * Atomically modifies the `Ref` with the specified function, which computes a
 * return value for the modification. This is a more powerful version of
 * `update`.
 */
export const modify =
  <A, B>(f: (a: A) => [B, A]) =>
  (ref: Ref<A>): XIO.UIO<B> => {
    concreteRef(ref)
    return ref.modify(f)
  }

/**
 * Atomically modifies the `Ref` with the specified function.
 */
export const update = <A>(f: (a: A) => A): ((ref: Ref<A>) => XIO.UIO<void>) =>
  modify((a) => [undefined, f(a)])

/**
 * Atomically writes the specified value to the `Ref`, returning the value
 * immediately before modification.
 */
export const getAndSet = <A, B extends A>(a: B): ((ref: Ref<A>) => XIO.UIO<A>) =>
  modify((v) => [v, a])

/**
 * Atomically modifies the `Ref` with the specified function, returning the
 * value immediately before modification.
 */
export const getAndUpdate = <A>(f: (a: A) => A): ((ref: Ref<A>) => XIO.UIO<A>) =>
  modify((v) => [v, f(v)])

/**
 * Atomically modifies the `Ref` with the specified partial function. If the
 * function is undefined on the current value it doesn't change it.
 */
export const updateSome = <A>(f: (a: A) => O.Option<A>): ((ref: Ref<A>) => XIO.UIO<void>) =>
  modify((a) =>
    pipe(
      f(a),
      O.match(
        () => [undefined, a],
        (result) => [undefined, result],
      ),
    ),
  )

/**
 * Atomically modifies the `Ref` with the specified function and returns the
 * updated value.
 */
export const updateAndGet = <A>(f: (a: A) => A): ((ref: Ref<A>) => XIO.UIO<A>) =>
  modify((a) => {
    const result = f(a)
    return [result, result]
  })

/**
 * Atomically modifies the `Ref` with the specified partial function. If the
 * function is `None` on the current value it returns the old value without
 * changing it.
 */
export const updateSomeAndGet = <A>(f: (a: A) => O.Option<A>): ((ref: Ref<A>) => XIO.UIO<A>) =>
  modify((a) =>
    pipe(
      f(a),
      O.match(
        () => [a, a],
        (result) => [result, result],
      ),
    ),
  )

/**
 * Atomically modifies the `Ref` with the specified partial function,
 * returning the value immediately before modification. If the function is
 * `None` on the current value it doesn't change it.
 */
export const getAndUpdateSome = <A>(f: (a: A) => O.Option<A>): ((ref: Ref<A>) => XIO.UIO<A>) =>
  modify((a) =>
    pipe(
      f(a),
      O.match(
        () => [a, a],
        (result) => [a, result],
      ),
    ),
  )

/**
 * Atomically modifies the `Ref` with the specified partial function, which
 * computes a return value for the modification if the function is defined on
 * the current value otherwise it returns a default value. This is a more
 * powerful version of `updateSome`.
 */
export const modifySome = <A, B>(
  def: B,
  f: (a: A) => O.Option<[B, A]>,
): ((ref: Ref<A>) => XIO.UIO<B>) =>
  modify((a) =>
    pipe(
      f(a),
      O.match(() => [def, a], identity),
    ),
  )

export function unsafeMake<A>(a: A): Ref<A> {
  return new Atomic(a)
}
