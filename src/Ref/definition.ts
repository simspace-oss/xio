import type { _A } from "../internal/types"

export const RefTypeId = Symbol.for("@simspace/xio/Ref")
export type RefTypeId = typeof RefTypeId

/**
 * A `Ref` is a purely functional description of a mutable reference. The
 * fundamental operations of a `Ref` are `set` and `get`. `set` sets the
 * reference to a new value. `get` gets the current value of the reference.
 */
export interface Ref<A> {
  readonly [RefTypeId]: RefTypeId
  readonly _A: () => A
}
