import type { Synchronized } from "./definition"

import { pipe } from "fp-ts/function"
import * as O from "fp-ts/Option"

import * as S from "../../Semaphore"
import * as XIO from "../internal/XIO"
import * as Ref from "./internal/Ref"
import { concreteSynchronized, SynchronizedInternal } from "./internal/Synchronized"

/**
 * Creates a new `Ref.Synchronized` with the specified value
 */
export const makeSynchronized = <A>(a: A): XIO.UIO<Synchronized<A>> =>
  pipe(
    XIO.Do,
    XIO.bind("ref", () => Ref.make(a) as XIO.UIO<Ref.Atomic<A>>),
    XIO.bind("semaphore", () => S.make(1)),
    XIO.map(({ ref, semaphore }) => new SynchronizedInternal(ref, semaphore)),
  )

/**
 * Atomically modifies the `Ref.Synchronized` with the specified function,
 * which computes a return value for the modification. This is a more
 * powerful version of `update`.
 */
export const modifyE =
  <A, R, E, B>(f: (a: A) => XIO.XIO<R, E, [B, A]>) =>
  (ref: Synchronized<A>): XIO.XIO<R, E, B> => {
    concreteSynchronized(ref)
    return ref.modifyE(f)
  }

export const getAndUpdateE = <R, E, A>(
  f: (a: A) => XIO.XIO<R, E, A>,
): ((ref: Synchronized<A>) => XIO.XIO<R, E, A>) =>
  modifyE((v) =>
    pipe(
      f(v),
      XIO.map((result) => [v, result]),
    ),
  )

export const getAndUpdateSomeE = <R, E, A>(
  f: (a: A) => O.Option<XIO.XIO<R, E, A>>,
): ((ref: Synchronized<A>) => XIO.XIO<R, E, A>) =>
  modifyE((v) =>
    pipe(
      f(v),
      O.getOrElseW(() => XIO.succeed(v)),
      XIO.map((result) => [v, result]),
    ),
  )

export const modifySomeE = <A, R, E, B>(
  def: B,
  f: (a: A) => O.Option<XIO.XIO<R, E, [B, A]>>,
): ((ref: Synchronized<A>) => XIO.XIO<R, E, B>) =>
  modifyE((v) =>
    pipe(
      f(v),
      O.getOrElseW(() => XIO.succeed([def, v] as [B, A])),
    ),
  )

export const updateE = <R, E, A>(
  f: (a: A) => XIO.XIO<R, E, A>,
): ((ref: Synchronized<A>) => XIO.XIO<R, E, void>) =>
  modifyE((a) =>
    pipe(
      f(a),
      XIO.map((result) => [undefined, result]),
    ),
  )

export const updateAndGetE = <R, E, A>(
  f: (a: A) => XIO.XIO<R, E, A>,
): ((ref: Synchronized<A>) => XIO.XIO<R, E, A>) =>
  modifyE((a) =>
    pipe(
      f(a),
      XIO.map((result) => [result, result]),
    ),
  )

export const updateSomeE = <R, E, A>(
  f: (a: A) => O.Option<XIO.XIO<R, E, A>>,
): ((ref: Synchronized<A>) => XIO.XIO<R, E, void>) =>
  modifyE((v) =>
    pipe(
      f(v),
      O.getOrElseW(() => XIO.succeed(v)),
      XIO.map((result) => [undefined, result]),
    ),
  )

export const updateSomeAndGetE = <R, E, A>(
  f: (a: A) => O.Option<XIO.XIO<R, E, A>>,
): ((ref: Synchronized<A>) => XIO.XIO<R, E, A>) =>
  modifyE((v) =>
    pipe(
      f(v),
      O.getOrElseW(() => XIO.succeed(v)),
      XIO.map((result) => [result, result]),
    ),
  )
