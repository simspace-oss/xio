import type * as Ref from "./internal/Ref"

export const SynchronizedTypeId = Symbol.for("@simspace/xio/Ref/Synchronized")
export type SynchronizedTypeId = typeof SynchronizedTypeId

/**
 * A `Ref.Synchronized` is a purely functional description of a mutable
 * reference. The fundamental operations of a `Ref.Synchronized` are `set` and
 * `get`. `set` sets the reference to a new value. `get` gets the current
 * value of the reference.
 *
 * Unlike an ordinary `Ref`, a `Ref.Synchronized` allows performing effects
 * within update operations, at some cost to performance. Writes will
 * semantically block other writers, while multiple readers can read
 * simultaneously.
 */
export interface Synchronized<A> extends Ref.Ref<A> {
  readonly [SynchronizedTypeId]: SynchronizedTypeId
}
