import type { Synchronized } from "../definition"

import { pipe } from "fp-ts/lib/function"

import * as S from "../../../Semaphore"
import * as XIO from "../../internal/XIO"
import { SynchronizedTypeId } from "../definition"
import * as Ref from "./Ref"

/** @internal */
export class SynchronizedInternal<A> implements Synchronized<A> {
  readonly [Ref.RefTypeId]: Ref.RefTypeId = Ref.RefTypeId;
  readonly [SynchronizedTypeId]: SynchronizedTypeId = SynchronizedTypeId
  readonly _A!: () => A
  constructor(readonly ref: Ref.Atomic<A>, readonly semaphore: S.Semaphore) {}

  get(): XIO.UIO<A> {
    return this.ref.get()
  }

  set(a: A): XIO.UIO<void> {
    return S.withPermit(this.semaphore)(this.ref.set(a))
  }

  modify<B>(f: (a: A) => [B, A]): XIO.UIO<B> {
    return this.modifyE((a) => XIO.succeed(f(a)))
  }

  modifyE<R, E, B>(f: (a: A) => XIO.XIO<R, E, [B, A]>): XIO.XIO<R, E, B> {
    return S.withPermit(this.semaphore)(
      pipe(
        this.get(),
        XIO.chain(f),
        XIO.chain(([b, a]) =>
          pipe(
            this.ref.set(a),
            XIO.map(() => b),
          ),
        ),
      ),
    )
  }
}

export function concreteSynchronized<A>(_: Synchronized<A>): asserts _ is SynchronizedInternal<A> {
  //
}
