/* eslint-disable no-bitwise */
import type * as S from "fp-ts/Show"

import * as Equ from "@simspace/collections/Equatable"
import * as H from "@simspace/collections/Hashable"
import * as HS from "@simspace/collections/HashSet"
import { pipe } from "fp-ts/function"

import { AtomicNumber } from "./internal/AtomicNumber"
import { hasTypeId } from "./internal/util"

export const enum Tag {
  Runtime,
  Composite,
  None,
}

export const FiberIdTypeId = Symbol.for("@simspace/xio/FiberId")
export type FiberIdTypeId = typeof FiberIdTypeId

export class Runtime implements H.Hashable, Equ.Equatable {
  readonly [FiberIdTypeId]: FiberIdTypeId = FiberIdTypeId
  readonly _tag = Tag.Runtime
  constructor(readonly id: number, readonly startTimeMillis: number) {}

  [Equ.equatableSymbol](that: unknown): boolean {
    return (
      isFiberId(that) &&
      that._tag === Tag.Runtime &&
      this.id === that.id &&
      this.startTimeMillis === that.startTimeMillis
    )
  }
  [H.hashableSymbol]() {
    let h = H.symbol(this[FiberIdTypeId])
    h ^= H.number(this._tag)
    h ^= H.number(this.id)
    h ^= H.number(this.startTimeMillis)
    return h
  }
}

export class Composite implements H.Hashable, Equ.Equatable {
  readonly [FiberIdTypeId]: FiberIdTypeId = FiberIdTypeId
  readonly _tag = Tag.Composite
  constructor(readonly fiberIds: HS.HashSet<Runtime>) {}
  [Equ.equatableSymbol](that: unknown): boolean {
    return (
      isFiberId(that) &&
      that._tag === Tag.Composite &&
      Equ.strictEqualsUnknown(this.fiberIds, that.fiberIds)
    )
  }
  [H.hashableSymbol]() {
    let h = H.symbol(this[FiberIdTypeId])
    h ^= H.number(this._tag)
    h ^= H.unknown(this.fiberIds)
    return h
  }
}

export class None implements H.Hashable, Equ.Equatable {
  readonly [FiberIdTypeId]: FiberIdTypeId = FiberIdTypeId
  readonly _tag = Tag.None;

  [Equ.equatableSymbol](that: unknown): boolean {
    return isFiberId(that) && that._tag === Tag.None
  }
  [H.hashableSymbol]() {
    let h = H.symbol(this[FiberIdTypeId])
    h ^= H.number(this._tag)
    return h
  }
}

export type FiberId = Runtime | Composite | None

function isFiberId(u: unknown): u is FiberId {
  return hasTypeId(u, FiberIdTypeId)
}

export function runtime(id: number, startTimeMillis: number): Runtime {
  return new Runtime(id, startTimeMillis)
}

export function composite(fiberIds: HS.HashSet<Runtime>): Composite {
  return new Composite(fiberIds)
}

export const none: None = new None()

export const match =
  <A>(cases: { None: () => A; Runtime: (_: Runtime) => A; Composite: (_: Composite) => A }) =>
  (fiberId: FiberId): A => {
    switch (fiberId._tag) {
      case Tag.None:
        return cases.None()
      case Tag.Runtime:
        return cases.Runtime(fiberId)
      case Tag.Composite:
        return cases.Composite(fiberId)
    }
  }

const fiberCounter = new AtomicNumber(0)

export function unsafeMake(): Runtime {
  return runtime(fiberCounter.getAndIncrement(), Date.now())
}

export function combine(that: FiberId): (self: FiberId) => FiberId {
  return match({
    None: () => that,
    Runtime: (self) =>
      pipe(
        that,
        match({
          None: () => self,
          Runtime: (that): FiberId =>
            pipe(HS.makeDefault<Runtime>(), HS.add(self), HS.add(that), composite),
          Composite: (that): FiberId => pipe(that.fiberIds, HS.add(self), composite),
        }),
      ),
    Composite: (self) =>
      pipe(
        that,
        match({
          None: () => self,
          Runtime: (that) => pipe(self.fiberIds, HS.add(that), composite),
          Composite: (that) => pipe(self.fiberIds, HS.union(that.fiberIds), composite),
        }),
      ),
  })
}

export const ids: (self: FiberId) => HS.HashSet<number> = match({
  None: () => HS.makeDefault(),
  Runtime: ({ id }) => pipe(HS.makeDefault<number>(), HS.add(id)),
  Composite: ({ fiberIds }) =>
    pipe(
      fiberIds,
      HS.map(({ id }) => id),
    ),
})

const showRuntime = ({ id, startTimeMillis }: Runtime): string =>
  `xio-fiber-${id}@${new Date(startTimeMillis)})`

const showComposite = ({ fiberIds }: Composite): string => {
  const items: Array<string> = ["["]
  for (const id of fiberIds) {
    items.push(showRuntime(id))
  }
  items.push("]")
  return items.join(", ")
}

export const render = match({
  None: () => "No FiberId",
  Runtime: showRuntime,
  Composite: showComposite,
})

export const Show: S.Show<FiberId> = {
  show: render,
}
