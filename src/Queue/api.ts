import type { Queue } from "./definition"
import type { Strategy } from "./internal/Strategy"
import type * as O from "fp-ts/Option"

import * as V from "@simspace/collections/Vector"
import { flow, pipe } from "fp-ts/lib/function"

import * as F from "../Future"
import { AtomicBoolean } from "../internal/AtomicBoolean"
import * as MQ from "../internal/MutableQueue"
import { concrete, Queue as QueueInternal } from "./internal/Queue"
import { BackPressure, Dropping, Sliding } from "./internal/Strategy"
import * as XIO from "./internal/XIO"

/**
 * Makes a new bounded queue. When the capacity of the queue is reached, any
 * additional calls to `offer` will be suspended until there is more room in
 * the queue.
 *
 * @note
 *   when possible use only power of 2 capacities; this will provide better
 *   performance
 */
export const bounded = <A>(requestedCapacity: number): XIO.UIO<Queue<A>> =>
  pipe(
    XIO.fromIO(() => MQ.bounded<A>(requestedCapacity)),
    XIO.chain((q) => createQueue(q, new BackPressure())),
  )

/**
 * Makes a new bounded queue with the dropping strategy. When the capacity of
 * the queue is reached, new elements will be dropped.
 *
 * @note
 *   when possible use only power of 2 capacities; this will provide better
 *   performance
 */
export const dropping = <A>(requestedCapacity: number): XIO.UIO<Queue<A>> =>
  pipe(
    XIO.fromIO(() => MQ.bounded<A>(requestedCapacity)),
    XIO.chain((q) => createQueue(q, new Dropping())),
  )

/**
 * Makes a new bounded queue with sliding strategy. When the capacity of the
 * queue is reached, new elements will be added and the old elements will be
 * dropped.
 *
 * @note
 *   when possible use only power of 2 capacities; this will provide better
 *   performance
 */
export const sliding = <A>(requestedCapacity: number): XIO.UIO<Queue<A>> =>
  pipe(
    XIO.fromIO(() => MQ.bounded<A>(requestedCapacity)),
    XIO.chain((q) => createQueue(q, new Sliding())),
  )

/**
 * Makes a new unbounded queue.
 */
export const unbounded = <A>(): XIO.UIO<Queue<A>> =>
  pipe(
    XIO.fromIO(() => MQ.unbounded<A>()),
    XIO.chain((q) => createQueue(q, new Dropping())),
  )

export const take = <A>(queue: Queue<A>): XIO.UIO<A> => {
  concrete(queue)
  return queue.take
}

export const takeUpTo =
  (max: number) =>
  <A>(queue: Queue<A>): XIO.UIO<V.Vector<A>> => {
    concrete(queue)
    return queue.takeUpTo(max)
  }

export const takeAll = <A>(queue: Queue<A>): XIO.UIO<V.Vector<A>> => {
  concrete(queue)
  return queue.takeAll
}

export const offer =
  <A>(a: A) =>
  (queue: Queue<A>): XIO.UIO<boolean> => {
    concrete(queue)
    return queue.offer(a)
  }

export const offerAll =
  <A>(as: Iterable<A>) =>
  (queue: Queue<A>): XIO.UIO<boolean> => {
    concrete(queue)
    return queue.offerAll(as)
  }

export const poll: <A>(queue: Queue<A>) => XIO.UIO<O.Option<A>> = flow(takeUpTo(1), XIO.map(V.head))

export const size = <A>(queue: Queue<A>): XIO.UIO<number> => {
  concrete(queue)
  return queue.size
}

export const shutdown = <A>(queue: Queue<A>): XIO.UIO<void> => {
  concrete(queue)
  return queue.shutdown
}

export const awaitShutdown = <A>(queue: Queue<A>): XIO.UIO<void> => {
  concrete(queue)
  return queue.awaitShutdown
}

function createQueue<A>(queue: MQ.MutableQueue<A>, strategy: Strategy<A>): XIO.UIO<Queue<A>> {
  return pipe(
    F.make<never, void>(),
    XIO.map((f) => new QueueInternal(queue, MQ.unbounded(), f, new AtomicBoolean(false), strategy)),
  )
}
