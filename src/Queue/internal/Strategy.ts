/* eslint-disable @typescript-eslint/strict-boolean-expressions */
import type { AtomicBoolean } from "../../internal/AtomicBoolean"
import type { MutableQueue } from "../../internal/MutableQueue"

import { pipe } from "fp-ts/function"

import * as F from "../../Future"
import { unbounded, unsafeDequeueAll } from "../../internal/MutableQueue"
import * as XIO from "../internal/XIO"

export abstract class Strategy<A> {
  abstract handleSurplus(
    as: Iterable<A>,
    queue: MutableQueue<A>,
    takers: MutableQueue<F.Future<never, A>>,
    isShutdown: AtomicBoolean,
  ): XIO.UIO<boolean>

  abstract unsafeOnQueueEmptySpace(
    queue: MutableQueue<A>,
    takers: MutableQueue<F.Future<never, A>>,
  ): void

  abstract surplusSize: number

  abstract shutdown: XIO.UIO<void>

  unsafeCompleteTakers(queue: MutableQueue<A>, takers: MutableQueue<F.Future<never, A>>): void {
    while (!queue.isEmpty) {
      const taker = takers.dequeue(null!)
      if (taker === null) {
        break
      }
      const a = queue.dequeue(null!)
      if (a === null) {
        const dequeued = unsafeDequeueAll(takers)
        dequeued.unshift(taker)
        takers.enqueueAll(dequeued)
      } else {
        unsafeCompleteFuture(taker, a)
        this.unsafeOnQueueEmptySpace(queue, takers)
      }
    }
  }
}

export class BackPressure<A> extends Strategy<A> {
  // A is an item to add
  // Future<never, boolean> is the future completing the whole offerAll
  // boolean indicates if it's the last item to offer (future should be completed once this item is added)
  private putters = unbounded<[A, F.Future<never, boolean>, boolean]>()

  private unsafeRemove(f: F.Future<never, boolean>): void {
    this.putters.enqueueAll(unsafeDequeueAll(this.putters).filter(([, f0]) => f0 !== f))
  }

  handleSurplus(
    as: Iterable<A>,
    queue: MutableQueue<A>,
    takers: MutableQueue<F.Future<never, A>>,
    isShutdown: AtomicBoolean,
  ): XIO.UIO<boolean> {
    return pipe(
      XIO.fiberId,
      XIO.chain((fiberId) => {
        const f = F.unsafeMake<never, boolean>(fiberId)
        return pipe(
          XIO.defer(() => {
            this.unsafeOffer(as, f)
            this.unsafeOnQueueEmptySpace(queue, takers)
            this.unsafeCompleteTakers(queue, takers)
            if (isShutdown.get()) {
              return XIO.interrupt
            } else {
              return F.await(f)
            }
          }),
          XIO.onInterrupt(XIO.fromIO(() => this.unsafeRemove(f))),
        )
      }),
    )
  }

  private unsafeOffer(as: Iterable<A>, f: F.Future<never, boolean>): void {
    const iterator = as[Symbol.iterator]()
    let a = iterator.next()
    if (!a.done) {
      let nextA: IteratorResult<A>
      while (!a.done) {
        nextA = iterator.next()
        if (nextA.done) {
          this.putters.enqueue([a.value, f, true])
        } else {
          this.putters.enqueue([a.value, f, false])
        }
        a = nextA
      }
    }
  }

  unsafeOnQueueEmptySpace(queue: MutableQueue<A>, takers: MutableQueue<F.Future<never, A>>): void {
    while (!queue.isFull) {
      const putter = this.putters.dequeue(null!)
      if (putter === null) {
        break
      }
      const enqueued = queue.enqueue(putter[0])
      if (enqueued && putter[2]) {
        unsafeCompleteFuture(putter[1], true)
      } else if (!enqueued) {
        const putters = unsafeDequeueAll(this.putters)
        putters.unshift(putter)
        this.putters.enqueueAll(putters)
      }
      this.unsafeCompleteTakers(queue, takers)
    }
  }

  get surplusSize() {
    return this.putters.size
  }

  shutdown: XIO.UIO<void> = pipe(
    XIO.Do,
    XIO.bind("fiberId", () => XIO.fiberId),
    XIO.bind("putters", () => XIO.fromIO(() => unsafeDequeueAll(this.putters))),
    XIO.chain(({ fiberId, putters }) =>
      XIO.foreachPar(putters, ([_, p, lastItem]) =>
        lastItem ? F.interruptAs(fiberId)(p) : XIO.unit,
      ),
    ),
  )
}

export class Dropping<A> extends Strategy<A> {
  handleSurplus(
    _as: Iterable<A>,
    _queue: MutableQueue<A>,
    _takers: MutableQueue<F.Future<never, A>>,
    _isShutdown: AtomicBoolean,
  ): XIO.UIO<boolean> {
    return XIO.succeed(false)
  }

  unsafeOnQueueEmptySpace(
    _queue: MutableQueue<A>,
    _takers: MutableQueue<F.Future<never, A>>,
  ): void {
    return undefined
  }

  surplusSize = 0

  shutdown: XIO.UIO<void> = XIO.unit
}

export class Sliding<A> extends Strategy<A> {
  handleSurplus(
    as: Iterable<A>,
    queue: MutableQueue<A>,
    takers: MutableQueue<F.Future<never, A>>,
    _isShutdown: AtomicBoolean,
  ): XIO.UIO<boolean> {
    return XIO.fromIO(() => {
      unsafeSlidingEnqueue(as, queue)
      this.unsafeCompleteTakers(queue, takers)
      return true
    })
  }

  unsafeOnQueueEmptySpace(
    _queue: MutableQueue<A>,
    _takers: MutableQueue<F.Future<never, A>>,
  ): void {
    return undefined
  }

  surplusSize = 0

  shutdown: XIO.UIO<void> = XIO.unit
}

export function unsafeCompleteFuture<A>(d: F.Future<never, A>, a: A): void {
  F.unsafeDone(XIO.succeed(a))(d)
}

function unsafeSlidingEnqueue<A>(as: Iterable<A>, queue: MutableQueue<A>) {
  const iterator = as[Symbol.iterator]()
  let a = iterator.next()
  if (!a.done && queue.capacity > 0) {
    while (true) {
      queue.dequeue(null!)
      const enqueued = queue.enqueue(a.value)
      a = iterator.next()
      if (enqueued && !a.done) {
        continue
      } else if (enqueued && a.done) {
        break
      }
    }
  }
}
