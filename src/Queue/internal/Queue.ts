import type { AtomicBoolean } from "../../internal/AtomicBoolean"
import type { MutableQueue } from "../../internal/MutableQueue"
import type { Queue as QueueDefinition } from "../definition"
import type { Strategy } from "./Strategy"

import * as V from "@simspace/collections/Vector"
import { pipe } from "fp-ts/lib/function"
import * as RA from "fp-ts/ReadonlyArray"

import * as F from "../../Future"
import { unsafeDequeueAll, unsafeRemove } from "../../internal/MutableQueue"
import { QueueTypeId } from "../definition"
import { unsafeCompleteFuture } from "./Strategy"
import * as XIO from "./XIO"

export class Queue<A> implements QueueDefinition<A> {
  readonly [QueueTypeId]: QueueTypeId = QueueTypeId
  readonly _A!: () => A
  constructor(
    readonly queue: MutableQueue<A>,
    readonly takers: MutableQueue<F.Future<never, A>>,
    readonly shutdownHook: F.Future<never, void>,
    readonly shutdownFlag: AtomicBoolean,
    readonly strategy: Strategy<A>,
  ) {}

  private removeTaker(taker: F.Future<never, A>): XIO.UIO<void> {
    return XIO.fromIO(() => unsafeRemove(this.takers, taker))
  }

  get capacity() {
    return this.queue.capacity
  }

  offer(a: A): XIO.UIO<boolean> {
    return XIO.defer(() => {
      if (this.shutdownFlag.get()) {
        return XIO.interrupt
      } else {
        let noRemaining = false
        if (this.queue.isEmpty) {
          const taker = this.takers.dequeue(null!)
          if (taker !== null) {
            unsafeCompleteFuture(taker, a)
            noRemaining = true
          }
        }
        if (noRemaining) {
          return XIO.succeed(true)
        } else {
          const succeeded = this.queue.enqueue(a)
          this.strategy.unsafeCompleteTakers(this.queue, this.takers)
          if (succeeded) {
            return XIO.succeed(true)
          } else {
            return this.strategy.handleSurplus([a], this.queue, this.takers, this.shutdownFlag)
          }
        }
      }
    })
  }

  offerAll(as: Iterable<A>): XIO.UIO<boolean> {
    return XIO.defer(() => {
      if (this.shutdownFlag.get()) {
        return XIO.interrupt
      } else {
        const items = Array.from(as)
        let pTakers: Array<F.Future<never, A>> = []
        if (this.queue.isEmpty) {
          pTakers = this.takers.dequeueUpTo(items.length)
        }
        const [forTakers, remaining] = RA.splitAt(pTakers.length)(items)
        for (let i = 0; i < forTakers.length && i < pTakers.length; i++) {
          unsafeCompleteFuture(pTakers[i], forTakers[i])
        }
        if (remaining.length === 0) {
          return XIO.succeed(true)
        } else {
          const surplus = this.queue.enqueueAll(remaining)
          this.strategy.unsafeCompleteTakers(this.queue, this.takers)
          if (surplus.length === 0) {
            return XIO.succeed(true)
          } else {
            return this.strategy.handleSurplus(surplus, this.queue, this.takers, this.shutdownFlag)
          }
        }
      }
    })
  }

  get awaitShutdown() {
    return F.await(this.shutdownHook)
  }

  get size() {
    return XIO.defer(() => {
      if (this.shutdownFlag.get()) {
        return XIO.interrupt
      } else {
        return XIO.succeed(this.queue.size - this.takers.size + this.strategy.surplusSize)
      }
    })
  }

  get shutdown(): XIO.UIO<void> {
    return pipe(
      XIO.fiberId,
      XIO.chain((fiberId) =>
        XIO.defer(() => {
          this.shutdownFlag.set(true)
          return XIO.whenXIO(F.succeed<void>(undefined)(this.shutdownHook))(
            pipe(
              XIO.foreachDiscardPar(unsafeDequeueAll(this.takers), F.interruptAs(fiberId)),
              XIO.chain(() => this.strategy.shutdown),
            ),
          )
        }),
      ),
    )
  }

  get take(): XIO.UIO<A> {
    return pipe(
      XIO.fiberId,
      XIO.chain((fiberId) =>
        XIO.defer(() => {
          if (this.shutdownFlag.get()) {
            return XIO.interrupt
          } else {
            const deq = this.queue.dequeue(null!)
            if (deq === null) {
              const f = F.unsafeMake<never, A>(fiberId)
              return pipe(
                XIO.defer(() => {
                  this.takers.enqueue(f)
                  this.strategy.unsafeCompleteTakers(this.queue, this.takers)
                  if (this.shutdownFlag.get()) {
                    return XIO.interrupt
                  } else {
                    return F.await(f)
                  }
                }),
                XIO.onInterrupt(this.removeTaker(f)),
              )
            } else {
              this.strategy.unsafeOnQueueEmptySpace(this.queue, this.takers)
              return XIO.succeed(deq)
            }
          }
        }),
      ),
    )
  }

  takeUpTo(max: number): XIO.UIO<V.Vector<A>> {
    return XIO.defer(() => {
      if (this.shutdownFlag.get()) {
        return XIO.interrupt
      } else {
        return XIO.fromIO(() => {
          const as = this.queue.dequeueUpTo(max)
          this.strategy.unsafeOnQueueEmptySpace(this.queue, this.takers)
          return V.from(as)
        })
      }
    })
  }

  get takeAll(): XIO.UIO<V.Vector<A>> {
    return XIO.defer(() => {
      if (this.shutdownFlag.get()) {
        return XIO.interrupt
      } else {
        return XIO.fromIO(() => {
          const as = unsafeDequeueAll(this.queue)
          this.strategy.unsafeOnQueueEmptySpace(this.queue, this.takers)
          return V.from(as)
        })
      }
    })
  }
}

export function concrete<A>(_: QueueDefinition<A>): asserts _ is Queue<A> {
  //
}
