import type { _A } from "../internal/types"

export const QueueTypeId = Symbol.for("@simspace/xio/Queue")
export type QueueTypeId = typeof QueueTypeId

/**
 * A `Queue` is a lightweight, asynchronous queue into which values can be
 * enqueued and of which elements can be dequeued.
 */
export interface Queue<A> {
  readonly [QueueTypeId]: QueueTypeId
  readonly _A: () => A
}
