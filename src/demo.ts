import type { Has } from "./Tag"

import * as E from "fp-ts/Either"
import { pipe } from "fp-ts/function"
import * as O from "fp-ts/Option"
import { inspect } from "util"

import * as Fi from "./Fiber"
import * as Ref from "./Ref"
import { tag } from "./Tag"
import * as XIO from "./XIO"

export interface CancellationEnv {
  readonly currentFiber: Ref.Ref<O.Option<Fi.Fiber<unknown, unknown>>>
}

const CancellationEnvTag = tag<CancellationEnv>("@simspace/xio/demo/CancellationEnv")

export function cancelWhenReenvoked<R, E, A>(
  effect: XIO.XIO<R, E, A>,
): XIO.XIO<R & Has<CancellationEnv>, E, A> {
  return pipe(
    XIO.Do,
    XIO.bind("ref", () => XIO.asksService(CancellationEnvTag, (env) => env.currentFiber)),
    XIO.chainFirst(({ ref }) =>
      pipe(Ref.get(ref), XIO.chain(O.match(() => XIO.unit, Fi.interrupt))),
    ),
    XIO.bind("fiber", () => XIO.fork(effect)),
    XIO.chain(({ ref, fiber }) =>
      pipe(
        ref,
        Ref.set(O.some(fiber)),
        XIO.chain(() => pipe(Fi.await(fiber), XIO.chain(XIO.done))),
      ),
    ),
  )
}

const delay =
  (n: number) =>
  <R, E, A>(xio: XIO.XIO<R, E, A>): XIO.XIO<R, E, A> =>
    pipe(
      XIO.sleep(n),
      XIO.chain(() => xio),
    )

const effect = XIO.asyncInterrupt<unknown, never, void>((k) => {
  const handle = setTimeout(() => {
    k(XIO.fromIO(() => console.log("Done in effect")))
  }, 1000)
  return E.left(
    XIO.fromIO(() => {
      clearTimeout(handle)
    }),
  )
})

const program = pipe(
  XIO.Do,
  XIO.bind("fiber1", () => XIO.fork(cancelWhenReenvoked(effect))),
  XIO.bind("fiber2", () => XIO.fork(pipe(cancelWhenReenvoked(effect), delay(500)))),
  XIO.chain(({ fiber1, fiber2 }) => XIO.foreachPar([fiber1, fiber2], Fi.await)),
  XIO.map((exits) => Array.from(exits)),
)

pipe(
  program,
  XIO.provideService(CancellationEnvTag, { currentFiber: Ref.unsafeMake(O.none) }),
  XIO.unsafeRunWith((exit) => console.log(inspect(exit, { depth: 10, colors: true }))),
)
