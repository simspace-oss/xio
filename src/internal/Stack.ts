export class Stack<A> {
  constructor(private stack?: StackInternal<A>) {}

  push(value: A) {
    this.stack = mkStack(value, this.stack)
  }

  peek(): A | undefined {
    if (this.stack) {
      return this.stack.value
    }
    return undefined
  }

  peekOrElse(v: A): A {
    if (this.stack) {
      return this.stack.value
    }
    return v
  }

  pop(): A | undefined {
    if (this.stack) {
      const value = this.stack.value
      this.stack = this.stack.previous
      return value
    }
    return undefined
  }

  get hasNext(): boolean {
    return this.stack !== undefined
  }
}

interface StackInternal<A> {
  value: A
  previous?: StackInternal<A>
}

function mkStack<A>(value: A, previous?: StackInternal<A>): StackInternal<A> {
  return {
    value,
    previous,
  }
}
