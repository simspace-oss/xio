export type _A<X> = [X] extends [{ _A: () => infer A }] ? A : never
export type _E<X> = [X] extends [{ _E: () => infer E }] ? E : never
export type _R<X> = [X] extends [{ _R: (_: infer R) => void }] ? R : never

export type Spreadable = Record<any, any>
