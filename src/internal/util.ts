import { isObject } from "@simspace/collections/internal/guards"

export function hasTypeId<A, B extends A>(a: A, typeId: symbol): a is B {
  return isObject(a) && typeId in a
}

export function assert(condition: boolean, message?: string): asserts condition {
  if (!condition) {
    throw new Error(message)
  }
}
