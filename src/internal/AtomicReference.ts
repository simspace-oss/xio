export class AtomicReference<A> {
  constructor(protected value: A) {}

  get(): A {
    return this.value
  }

  set(value: A) {
    this.value = value
  }

  getAndSet(value: A): A {
    const previous = this.value
    this.value = value
    return previous
  }

  compareAndSet(old: A, value: A): boolean {
    if (this.get() === old) {
      this.set(value)
      return true
    }
    return false
  }
}
