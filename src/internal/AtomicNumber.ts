import { AtomicReference } from "./AtomicReference"

export class AtomicNumber extends AtomicReference<number> {
  constructor(value: number) {
    super(value)
  }

  getAndIncrement(): number {
    const previous = this.value
    this.value += 1
    return previous
  }

  incrementAndGet(): number {
    this.value += 1
    return this.value
  }
}
