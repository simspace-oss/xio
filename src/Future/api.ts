import type * as C from "../Cause"
import type * as Ex from "../Exit"
import type { FiberId } from "../FiberId"
import type { Future } from "./definition"

import * as L from "@simspace/collections/List"
import * as E from "fp-ts/Either"
import { pipe } from "fp-ts/function"

import { concrete, Future as FutureInternal } from "./internal/Future"
import * as State from "./internal/State"
import * as XIO from "./internal/XIO"

export function unsafeMake<E, A>(fiberId: FiberId): Future<E, A> {
  return new FutureInternal(new State.Pending(L.nil()), fiberId)
}

/**
 * Makes a new future to be completed by the fiber with the specified id.
 */
export function makeAs<E, A>(fiberId: FiberId): XIO.UIO<Future<E, A>> {
  return XIO.fromIO(() => unsafeMake(fiberId))
}

/**
 * Makes a new future to be completed by the fiber creating the future.
 */
export function make<E, A>(): XIO.UIO<Future<E, A>> {
  return pipe(
    XIO.fiberId,
    XIO.chain((fiberId) => makeAs(fiberId)),
  )
}

/**
 * Retrieves the value of the future, suspending the fiber running the action
 * until the result is available.
 */
function wait<E, A>(future: Future<E, A>): XIO.XIO<unknown, E, A> {
  return XIO.asyncInterrupt<unknown, E, A>((k) => {
    concrete(future)
    switch (future.state._tag) {
      case State.Tag.Pending: {
        future.state = new State.Pending(pipe(future.state.joiners, L.prepend(k)))
        return E.left(interruptJoiner(future, k))
      }
      case State.Tag.Done: {
        return E.right(future.state.value)
      }
    }
  })
}

export { wait as await }

/**
 * Completes the future with the specified effect. If the future has already
 * been completed, the function will produce false.
 *
 * Note that since the future is completed with an effect, the effect will be
 * evaluated each time the value of the promise is retrieved through
 * combinators such as `await`, potentially producing different results if the
 * effect produces different results on subsequent evaluations. In this case
 * te meaning of the "exactly once" guarantee of `Future` is that the future
 * can be completed with exactly one effect. For a version that completes the
 * promise with the result of an effect see `complete`.
 */
export const completeWith =
  <E, A, E1 extends E, A1 extends A>(xio: XIO.XIO<unknown, E1, A1>) =>
  (future: Future<E, A>): XIO.UIO<boolean> => {
    concrete(future)
    return XIO.fromIO(() => {
      switch (future.state._tag) {
        case State.Tag.Pending: {
          const state = future.state
          future.state = new State.Done(xio)
          pipe(
            state.joiners,
            L.forEach((j) => j(xio)),
          )
          return true
        }
        case State.Tag.Done: {
          return false
        }
      }
    })
  }

/**
 * Kills the future with the specified error, which will be propagated to all
 * fibers waiting on the value of the future.
 */
export const halt =
  (value: unknown) =>
  <E, A>(future: Future<E, A>): XIO.UIO<boolean> =>
    pipe(future, completeWith(XIO.halt(value)))

/**
 * Exits the future with the specified exit, which will be propagated to all
 * fibers waiting on the value of the future.
 */
export const done = <E, A>(exit: Ex.Exit<E, A>): ((future: Future<E, A>) => XIO.UIO<boolean>) =>
  completeWith(XIO.done(exit))

/**
 * Fails the future with the specified error, which will be propagated to all
 * fibers waiting on the value of the future.
 */
export const fail =
  <E, E1 extends E>(e: E1) =>
  <A>(future: Future<E, A>): XIO.UIO<boolean> =>
    pipe(future, completeWith(XIO.fail(e)))

/**
 * Fails the future with the specified cause, which will be propagated to all
 * fibers waiting on the value of the future.
 */
export const failCause =
  <E>(cause: C.Cause<E>) =>
  <A>(future: Future<E, A>): XIO.UIO<boolean> =>
    pipe(future, completeWith(XIO.failCause(cause)))

/**
 * Completes the future with interruption. This will interrupt all fibers
 * waiting on the value of the future as by the fiber calling this function.
 */
export const interrupt = <E, A>(future: Future<E, A>): XIO.UIO<boolean> =>
  pipe(
    XIO.fiberId,
    XIO.chain((id) => pipe(future, completeWith(XIO.interruptAs(id)))),
  )

/**
 * Completes the future with interruption. This will interrupt all fibers
 * waiting on the value of the future as by the specified fiber.
 */
export const interruptAs =
  (id: FiberId) =>
  <E, A>(future: Future<E, A>) =>
    pipe(future, completeWith(XIO.interruptAs(id)))

/**
 * Completes the future with the specified value.
 */
export const succeed =
  <A>(a: A) =>
  <E>(future: Future<E, A>): XIO.UIO<boolean> =>
    pipe(future, completeWith(XIO.succeed(a)))

export const complete =
  <R, E, A>(xio: XIO.XIO<R, E, A>) =>
  (future: Future<E, A>): XIO.XIO<R, never, boolean> =>
    XIO.uninterruptibleMask((restore) =>
      pipe(
        restore(xio),
        XIO.result,
        XIO.chain((exit) => done(exit)(future)),
      ),
    )

export const unsafeDone =
  <E, A>(xio: XIO.XIO<unknown, E, A>) =>
  (future: Future<E, A>) => {
    concrete(future)
    if (future.state._tag === State.Tag.Pending) {
      const state = future.state
      future.state = new State.Done(xio)
      pipe(
        state.joiners,
        L.reverse,
        L.forEach((f) => {
          f(xio)
        }),
      )
    }
  }

function interruptJoiner<E, A>(
  future: Future<E, A>,
  joiner: (xio: XIO.XIO<unknown, E, A>) => void,
): XIO.UIO<void> {
  return XIO.fromIO(() => {
    concrete(future)
    if (future.state._tag === State.Tag.Pending) {
      future.state = new State.Pending(
        pipe(
          future.state.joiners,
          L.filter((j) => j !== joiner),
        ),
      )
    }
  })
}
