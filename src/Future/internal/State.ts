import type * as XIO from "./XIO"
import type * as L from "@simspace/collections/List"

export const enum Tag {
  Pending,
  Done,
}

export class Pending<E, A> {
  readonly _tag = Tag.Pending
  constructor(readonly joiners: L.List<(_: XIO.XIO<unknown, E, A>) => void>) {}
}

export class Done<E, A> {
  readonly _tag = Tag.Done
  constructor(readonly value: XIO.XIO<unknown, E, A>) {}
}

export type State<E, A> = Pending<E, A> | Done<E, A>
