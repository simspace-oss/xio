import type { FiberId } from "../../FiberId"
import type { Future as FutureDefinition } from "../definition"
import type { State } from "./State"

import { FutureTypeId } from "../definition"

export class Future<E, A> implements FutureDefinition<E, A> {
  readonly [FutureTypeId]: typeof FutureTypeId = FutureTypeId
  readonly _E!: () => E
  readonly _A!: () => A

  constructor(public state: State<E, A>, readonly blockingOn: FiberId) {}
}

export function concrete<E, A>(_: FutureDefinition<E, A>): asserts _ is Future<E, A> {
  //
}
