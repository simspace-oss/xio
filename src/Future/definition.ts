import type { _A, _E } from "../internal/types"

export const FutureTypeId = Symbol.for("@simspace/xio/Future")
export type FutureTypeId = typeof FutureTypeId

/**
 * A `Future` represents an asynchronous variable, that can
 * be set exactly once, with the ability for an arbitrary number of fibers to
 * suspend (by calling `await`) and automatically resume when the variable is
 * set.
 *
 * Futures can be used for building primitive actions whose completions require
 * the coordinated action of multiple fibers, and for building higher-level
 * concurrent or asynchronous structures.
 */
export interface Future<E, A> {
  readonly [FutureTypeId]: FutureTypeId
  readonly _E: () => E
  readonly _A: () => A
}
