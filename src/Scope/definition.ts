import type { Exit } from "../Exit"
import type { Has } from "../Tag"
import type * as RM from "./internal/ReleaseMap"

import { pipe } from "fp-ts/lib/function"

import { tag } from "../Tag"
import * as XIO from "./internal/XIO"

export const ScopeTypeId = Symbol.for("@simspace/xio/Scope")
export type ScopeTypeId = typeof ScopeTypeId

export class Scope {
  readonly [ScopeTypeId]: ScopeTypeId = ScopeTypeId
  constructor(
    readonly fork: XIO.UIO<Closeable>,
    readonly addFinalizerExit: (finalizer: RM.Finalizer) => XIO.UIO<void>,
  ) {}

  /**
   * Extends the scope of an `XIO` that needs a scope into this scope by
   * providing it to the workflow but not closing the scope when the workflow
   * completes execution. This allows extending a scoped value into a larger
   * scope.
   */
  extend = <R, E, A>(xio: XIO.XIO<R & Has<Scope>, E, A>): XIO.XIO<R, E, A> =>
    pipe(xio, XIO.provideService(Tag, this))
}

export const CloseableScopeTypeId = Symbol.for("@simspace/xio/Scope.Closeable")
export type CloseableScopeTypeId = typeof CloseableScopeTypeId

export class Closeable extends Scope {
  readonly [CloseableScopeTypeId]: CloseableScopeTypeId = CloseableScopeTypeId
  constructor(
    readonly fork: XIO.UIO<Closeable>,
    readonly addFinalizerExit: (finalizer: RM.Finalizer) => XIO.UIO<void>,
    readonly close: (exit: Exit<unknown, unknown>) => XIO.UIO<void>,
  ) {
    super(fork, addFinalizerExit)
  }

  /**
   * Uses the scope by providing it to an `XIO` that needs a scope,
   * guaranteeing that the scope is closed with the result of that effect as
   * soon as the effect completes execution, whether by success, failure, or
   * interruption.
   */
  use = <R, E, A>(xio: XIO.XIO<R & Has<Scope>, E, A>): XIO.XIO<R, E, A> =>
    pipe(
      this.extend(xio),
      XIO.onExit((exit) => this.close(exit)),
    )
}

export const Tag = tag<Scope>("@simspace/xio/Scope/Tag")
