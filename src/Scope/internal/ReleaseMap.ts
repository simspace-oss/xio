import type { Exit } from "../../Exit"

import * as HM from "@simspace/collections/HashMap"
import { identity, pipe, tuple } from "fp-ts/function"
import * as O from "fp-ts/Option"

import * as ES from "../../ExecutionStrategy"
import * as Ex from "../../Exit"
import * as Ref from "../../Ref"
import * as XIO from "./XIO"

export type Finalizer = (_: Exit<unknown, unknown>) => XIO.UIO<unknown>

export type ReleaseMap = Ref.Ref<State>

type State = Exited | Running

/** @internal */
export class Exited {
  readonly _tag = "Exited"
  constructor(
    readonly nextKey: number,
    readonly exit: Exit<unknown, unknown>,
    readonly update: (_: Finalizer) => Finalizer,
  ) {}
}

/** @internal */
export class Running {
  readonly _tag = "Running"
  constructor(
    readonly nextKey: number,
    readonly finalizers: HM.HashMap<number, Finalizer>,
    readonly update: (_: Finalizer) => Finalizer,
  ) {}
}

export const unsafeMake = (): ReleaseMap =>
  Ref.unsafeMake(new Running(0, HM.makeDefault(), identity))

export const make: XIO.UIO<ReleaseMap> = XIO.fromIO(() => unsafeMake())

export const addIfOpen =
  (finalizer: Finalizer) =>
  (releaseMap: ReleaseMap): XIO.UIO<O.Option<number>> =>
    pipe(
      releaseMap,
      Ref.modify((s) => {
        switch (s._tag) {
          case "Exited":
            return tuple(
              pipe(
                finalizer(s.exit),
                XIO.map(() => O.none),
              ),
              new Exited(s.nextKey + 1, s.exit, s.update),
            )
          case "Running":
            return tuple(
              XIO.fromIO(() => O.some(s.nextKey)),
              new Running(
                s.nextKey + 1,
                pipe(s.finalizers, HM.set(s.nextKey, finalizer)),
                s.update,
              ),
            )
        }
      }),
      XIO.flatten,
    )

export const add =
  (finalizer: Finalizer) =>
  (releaseMap: ReleaseMap): XIO.UIO<Finalizer> =>
    pipe(
      releaseMap,
      addIfOpen(finalizer),
      XIO.map(
        O.match(
          (): Finalizer => () => XIO.unit,
          (k) => (e) => pipe(releaseMap, release(k, e)),
        ),
      ),
    )

export const release =
  (key: number, exit: Ex.Exit<unknown, unknown>) =>
  (releaseMap: ReleaseMap): XIO.UIO<void> =>
    pipe(
      releaseMap,
      Ref.modify((s) => {
        switch (s._tag) {
          case "Exited":
            return tuple(XIO.unit, s)
          case "Running":
            return tuple(
              pipe(
                s.finalizers,
                HM.get(key),
                O.match(
                  () => XIO.unit,
                  (fin) => s.update(fin)(exit),
                ),
              ),
              new Running(s.nextKey, pipe(s.finalizers, HM.remove(key)), s.update),
            )
        }
      }),
      XIO.flatten,
    ) as XIO.UIO<void>

export const remove =
  (key: number) =>
  (releaseMap: ReleaseMap): XIO.UIO<O.Option<Finalizer>> =>
    pipe(
      releaseMap,
      Ref.modify((s) => {
        switch (s._tag) {
          case "Exited": {
            return tuple(O.none, s)
          }
          case "Running": {
            return tuple(
              pipe(s.finalizers, HM.get(key)),
              new Running(s.nextKey, pipe(s.finalizers, HM.remove(key)), s.update),
            )
          }
        }
      }),
    )

export const replace =
  (key: number, finalizer: Finalizer) =>
  (releaseMap: ReleaseMap): XIO.UIO<O.Option<Finalizer>> =>
    pipe(
      releaseMap,
      Ref.modify((s) => {
        switch (s._tag) {
          case "Exited":
            return tuple(
              pipe(
                finalizer(s.exit),
                XIO.map(() => O.none),
              ),
              s,
            )
          case "Running":
            return tuple(
              XIO.fromIO(() => pipe(s.finalizers, HM.get(key))),
              new Running(s.nextKey, pipe(s.finalizers, HM.set(key, finalizer)), s.update),
            )
        }
      }),
      XIO.flatten,
    )

export const releaseAll =
  (exit: Ex.Exit<unknown, unknown>, executionStrategy: ES.ExecutionStrategy) =>
  (releaseMap: ReleaseMap): XIO.UIO<void> =>
    pipe(
      releaseMap,
      Ref.modify((s) => {
        switch (s._tag) {
          case "Exited":
            return [XIO.unit, s]
          case "Running":
            return ES.match_(executionStrategy, {
              Sequential: () =>
                tuple(
                  pipe(
                    XIO.foreach(s.finalizers, ([_, fin]) => pipe(s.update(fin)(exit), XIO.result)),
                    XIO.chain((results) =>
                      pipe(
                        Ex.collectAll(results),
                        O.match(
                          () => Ex.unit,
                          Ex.map(() => undefined),
                        ),
                        XIO.done,
                      ),
                    ),
                  ),
                  new Exited(s.nextKey, exit, s.update),
                ),
              Concurrent: () =>
                tuple(
                  pipe(
                    XIO.foreachPar(s.finalizers, ([_, fin]) =>
                      pipe(exit, s.update(fin), XIO.result),
                    ),
                    XIO.chain((results) =>
                      pipe(
                        Ex.collectAllPar(results),
                        O.match(
                          () => Ex.unit,
                          Ex.map(() => undefined),
                        ),
                        XIO.done,
                      ),
                    ),
                  ),
                  new Exited(s.nextKey, exit, s.update),
                ),
              ConcurrentBounded: (bound) =>
                tuple(
                  pipe(
                    XIO.foreachPar(s.finalizers, ([_, fin]) =>
                      pipe(exit, s.update(fin), XIO.result),
                    ),
                    XIO.chain((results) =>
                      pipe(
                        Ex.collectAllPar(results),
                        O.match(
                          () => Ex.unit,
                          Ex.map(() => undefined),
                        ),
                        XIO.done,
                      ),
                    ),
                    XIO.withConcurrency(bound),
                  ),
                  new Exited(s.nextKey, exit, s.update),
                ),
            })
        }
      }),
      XIO.flatten,
    )
