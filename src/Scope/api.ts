import type { ExecutionStrategy } from "../ExecutionStrategy"
import type { Has } from "../Tag"
import type { Scope } from "./definition"

import { pipe } from "fp-ts/function"

import { Sequential } from "../ExecutionStrategy"
import { Closeable, Tag } from "./definition"
import * as RM from "./internal/ReleaseMap"
import * as XIO from "./internal/XIO"

/**
 * Accesses a scope in the environment and adds a finalizer to it.
 * The finalizer is guaranteed to be run when the scope is closed.
 */
export const addFinalizerExit = (finalizer: RM.Finalizer): XIO.XIO<Has<Scope>, never, void> =>
  XIO.asksServiceXIO(Tag, (scope) => scope.addFinalizerExit(finalizer))

/**
 * A simplified version of `addFinalizerExit` when the `finalizer` does not
 * depend on the `Exit` value that the scope is closed with.
 */
export const addFinalizer = (finalizer: XIO.UIO<void>): XIO.XIO<Has<Scope>, never, void> =>
  addFinalizerExit(() => finalizer)

/**
 * Makes a scope. Finalizers added to this scope will be run according to the
 * specified `ExecutionStrategy`.
 */
export const makeWith = (executionStrategy: ExecutionStrategy): XIO.UIO<Closeable> =>
  pipe(
    RM.make,
    XIO.map(
      (releaseMap) =>
        new Closeable(
          XIO.uninterruptible(
            pipe(
              XIO.Do,
              XIO.bind("scope", () => make),
              XIO.bind("finalizer", ({ scope }) =>
                pipe(
                  releaseMap,
                  RM.add((exit) => scope.close(exit)),
                ),
              ),
              XIO.chainFirst(({ scope, finalizer }) => scope.addFinalizerExit(finalizer)),
              XIO.map(({ scope }) => scope),
            ),
          ),
          (finalizer) =>
            pipe(
              releaseMap,
              RM.add(finalizer),
              XIO.map(() => undefined),
            ),
          (exit) =>
            XIO.defer(() =>
              pipe(
                releaseMap,
                RM.releaseAll(exit, executionStrategy),
                XIO.map(() => undefined),
              ),
            ),
        ),
    ),
  )

/**
 * Makes a scope. Finalizers added to this scope will be run sequentially in
 * the reverse of the order in which they were added when this scope is
 * closed.
 */
export const make = makeWith(Sequential)
