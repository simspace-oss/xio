import type { FiberId } from "../FiberId"
import type { InterruptStatus } from "../InterruptStatus"
import type { FiberStatus } from "./internal/FiberStatus"
import type { HashSet } from "@simspace/collections/HashSet"

export interface FiberDescriptor {
  readonly id: FiberId
  readonly status: FiberStatus
  readonly interruptors: HashSet<FiberId>
  readonly interruptStatus: InterruptStatus
}
