import type * as Ex from "../Exit"
import type { FiberId } from "../FiberId"
import type { Fiber } from "./definition"

import { pipe } from "fp-ts/function"

import * as FId from "../FiberId"
import { concrete, Synthetic } from "./internal/Fiber"
import * as XIO from "./internal/XIO"

/**
 * Awaits the fiber, which suspends the awaiting fiber until the result of the
 * fiber has been determined.
 */
export function wait<E, A>(fiber: Fiber<E, A>): XIO.XIO<unknown, never, Ex.Exit<E, A>> {
  concrete(fiber)
  return fiber.await
}

export { wait as await }

export const id = <E, A>(fiber: Fiber<E, A>): FiberId => {
  concrete(fiber)
  return fiber.id
}

/**
 * Inherits values from all FiberRef instances into current fiber. This
 * will resume immediately.
 */
export const inheritRefs = <E, A>(fiber: Fiber<E, A>): XIO.UIO<void> => {
  concrete(fiber)
  return fiber.inheritRefs
}

/**
 * Interrupts the fiber from whichever fiber is calling this function. If the
 * fiber has already exited, the returned effect will resume immediately.
 * Otherwise, the effect will resume when the fiber exits.
 */
export function interrupt<E, A>(fiber: Fiber<E, A>): XIO.UIO<Ex.Exit<E, A>> {
  concrete(fiber)
  return pipe(
    XIO.fiberId,
    XIO.chain((fiberId) => fiber.interruptAs(fiberId)),
  )
}

/**
 * Interrupts the fiber as if interrupted from the specified fiber. If the
 * fiber has already exited, the returned effect will resume immediately.
 * Otherwise, the effect will resume when the fiber exits.
 */
export const interruptAs =
  (fiberId: FiberId) =>
  <E, A>(fiber: Fiber<E, A>): XIO.UIO<Ex.Exit<E, A>> => {
    concrete(fiber)
    return fiber.interruptAs(fiberId)
  }

/**
 * Joins the fiber, which suspends the joining fiber until the result of the
 * fiber has been determined. Attempting to join a fiber that has erred will
 * result in a catchable error. Joining an interrupted fiber will result in an
 * "inner interruption" of this fiber, unlike interruption triggered by
 * another fiber, "inner interruption" can be caught and recovered.
 */
export const join = <E, A>(fiber: Fiber<E, A>): XIO.XIO<unknown, E, A> => {
  concrete(fiber)
  return pipe(
    wait(fiber),
    XIO.chain(XIO.done),
    XIO.chainFirst(() => fiber.inheritRefs),
  )
}

export const done = <E, A>(exit: Ex.Exit<E, A>): Fiber<E, A> =>
  new Synthetic(FId.none, XIO.succeed(exit), () => XIO.succeed(exit), XIO.unit)
