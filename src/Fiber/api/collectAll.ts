import type { Fiber } from "../definition"

import * as V from "@simspace/collections/Vector"
import { flow, pipe } from "fp-ts/function"

import * as C from "../../Cause"
import * as Ex from "../../Exit"
import * as FId from "../../FiberId"
import { id, inheritRefs, interruptAs, wait } from "../api"
import { Synthetic } from "../internal/Fiber"
import * as XIO from "./internal/XIO"

export function collectAll<E, A>(fibers: Iterable<Fiber<E, A>>): Fiber<E, V.Vector<A>> {
  return new Synthetic(
    pipe(
      V.from(fibers),
      V.reduce(FId.none as FId.FiberId, (fiberId, fiber) => pipe(fiberId, FId.combine(id(fiber)))),
    ),
    pipe(XIO.foreachPar(fibers, flow(wait, XIO.chain(XIO.done))), XIO.result),
    (fiberId) =>
      pipe(
        XIO.foreach(fibers, interruptAs(fiberId)),
        XIO.map(
          V.reduce(Ex.succeed(V.empty()), (acc, ex) =>
            pipe(
              acc,
              Ex.zipWithCause(ex, (results, a) => V.append(a)(results), C.both),
            ),
          ),
        ),
      ),
    XIO.foreachDiscard(fibers, inheritRefs),
  )
}
