import type { Fiber } from "../definition"

import { pipe } from "fp-ts/function"

import { wait } from "../api"
import { collectAll } from "./collectAll"
import * as XIO from "./internal/XIO"

export function awaitAll(fibers: Iterable<Fiber<unknown, unknown>>): XIO.UIO<void> {
  return pipe(collectAll(fibers), wait, XIO.asUnit)
}
