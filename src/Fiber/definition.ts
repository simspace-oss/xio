export const FiberTypeId = Symbol.for("@simspace/xio/Fiber")
export type FiberTypeId = typeof FiberTypeId

export interface Fiber<E, A> {
  readonly [FiberTypeId]: FiberTypeId
  readonly _E: () => E
  readonly _A: () => A
}

export const RuntimeTypeId = Symbol.for("@simspacr/xio/Fiber/Runtime")
export type RuntimeTypeId = typeof RuntimeTypeId

export interface Runtime<E, A> extends Fiber<E, A> {
  readonly [RuntimeTypeId]: RuntimeTypeId
}

export const SyntheticTypeId = Symbol.for("@simspacr/xio/Fiber/Synthetic")
export type SyntheticTypeId = typeof SyntheticTypeId

export interface Synthetic<E, A> extends Fiber<E, A> {
  readonly [SyntheticTypeId]: SyntheticTypeId
}
