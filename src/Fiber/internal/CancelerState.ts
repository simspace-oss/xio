import type { XIO } from "../../XIO/definition"

export const enum Tag {
  Empty,
  Pending,
  Registered,
}

export interface Empty {
  readonly _tag: Tag.Empty
}

export const Empty: CancelerState = {
  _tag: Tag.Empty,
}

export interface Pending {
  readonly _tag: Tag.Pending
}

export const Pending: CancelerState = {
  _tag: Tag.Pending,
}

export interface Registered {
  readonly _tag: Tag.Registered
  readonly asyncCanceler: XIO<never, unknown, unknown>
}

export function Registered(asyncCanceler: XIO<never, unknown, unknown>): CancelerState {
  return {
    _tag: Tag.Registered,
    asyncCanceler,
  }
}

export type CancelerState = Empty | Pending | Registered
