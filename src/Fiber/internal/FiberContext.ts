import type { RuntimeConfig } from "../../XIO"
import type { FiberDescriptor } from "../FiberDescriptor"
import type { Fiber } from "./Fiber"
import type { FiberScope } from "./FiberScope"

import * as HM from "@simspace/collections/HashMap"
import * as HS from "@simspace/collections/HashSet"
import * as L from "@simspace/collections/List"
import * as E from "fp-ts/Either"
import { pipe } from "fp-ts/function"
import * as O from "fp-ts/Option"

import * as C from "../../Cause"
import * as Ex from "../../Exit"
import * as FId from "../../FiberId"
import * as FR from "../../FiberRef"
import { currentEnvironment, currentScheduler, forkScopeOverride } from "../../FiberRef/primitives"
import { FiberRefs, join } from "../../FiberRefs"
import { AtomicBoolean } from "../../internal/AtomicBoolean"
import { Stack } from "../../internal/Stack"
import { hasTypeId } from "../../internal/util"
import * as InterruptStatus from "../../InterruptStatus"
import * as Sync from "../../Sync"
import * as CancellerState from "./CancelerState"
import { Runtime } from "./Fiber"
import * as Scope from "./FiberScope"
import * as State from "./FiberState"
import * as Status from "./FiberStatus"
import * as XIO from "./XIO"

export const FiberContextTypeId = Symbol.for("@simspace/xio/FiberContext")
export type FiberContextTypeId = typeof FiberContextTypeId

type FiberRefLocals = HM.HashMap<FR.FiberRef<unknown>, L.Cons<[FId.Runtime, unknown]>>

type ErasedEffect = XIO.XIO<never, unknown, unknown>

type ErasedContinuation = (a: unknown) => ErasedEffect

const enum FrameTag {
  InterruptExit,
  HandlerFrame,
  ApplyFrame,
  MatchFrame,
  Finalizer,
}

class InterruptExit {
  readonly _tag = FrameTag.InterruptExit
  constructor(readonly apply: ErasedContinuation) {}
}

class HandlerFrame {
  readonly _tag = FrameTag.HandlerFrame
  constructor(readonly apply: ErasedContinuation) {}
}

class ApplyFrame {
  readonly _tag = FrameTag.ApplyFrame
  constructor(readonly apply: ErasedContinuation) {}
}

class MatchFrame {
  readonly _tag = FrameTag.MatchFrame
  constructor(
    readonly onFailure: (cause: C.Cause<unknown>) => ErasedEffect,
    readonly apply: ErasedContinuation,
  ) {}
}

class Finalizer {
  readonly _tag = FrameTag.Finalizer
  constructor(
    readonly finalizer: XIO.XIO<unknown, never, unknown>,
    readonly apply: ErasedContinuation,
  ) {}
}

export type Frame = InterruptExit | MatchFrame | HandlerFrame | ApplyFrame | Finalizer

export class FiberContext<E, A> extends Runtime<E, A> {
  constructor(
    private fiberId: FId.Runtime,
    private readonly runtimeConfig: RuntimeConfig,
    private interruptStatus: Stack<boolean>,
    private fiberRefLocals: FiberRefLocals,
    private _children: Set<FiberContext<unknown, unknown>>,
  ) {
    super()
  }

  readonly [FiberContextTypeId]: FiberContextTypeId = FiberContextTypeId

  private asyncEpoch = 0
  private stack: Stack<Frame> = new Stack()
  private state: State.FiberState<E, A> = State.initial()
  private nextEffect: ErasedEffect | null = null

  get id() {
    return this.fiberId
  }

  get scope(): Scope.FiberScope {
    return Scope.unsafeMake(this)
  }

  get status(): XIO.UIO<Status.FiberStatus> {
    return new XIO.FromIO(() => this.state.status)
  }

  get inheritRefs() {
    const childFiberRefs = new FiberRefs(this.fiberRefLocals)
    if (HM.size(childFiberRefs.fiberRefLocals) === 0) {
      return XIO.unit
    } else {
      return XIO.updateFiberRefs((parentFiberId, parentFiberRefs) =>
        join(parentFiberRefs, parentFiberId, childFiberRefs),
      )
    }
  }

  interruptAs(fiberId: FId.FiberId): XIO.XIO<unknown, never, Ex.Exit<E, A>> {
    return this.unsafeInterruptAs(fiberId)
  }

  get await(): XIO.XIO<unknown, never, Ex.Exit<E, A>> {
    return new XIO.Async<unknown, never, Ex.Exit<E, A>>((k) => {
      const cb: State.Callback<never, Ex.Exit<E, A>> = (x) => k(XIO.done(x))
      const result = this.unsafeAddObserver(cb)
      if (result === null) {
        return E.left(new XIO.FromIO(() => this.unsafeRemoveObserver(cb)))
      } else {
        return E.right(new XIO.Succeed(result))
      }
    }, this.fiberId)
  }

  private runUntil(yieldOpCount: number) {
    try {
      let current = this.nextEffect as XIO.Concrete | null
      this.nextEffect = null
      while (current !== null) {
        try {
          let opCount = 0
          while (current !== null) {
            if (!this.unsafeShouldInterrupt()) {
              const message = this.unsafeDrainMailbox()
              if (message !== null) {
                const oldEffect: XIO.XIO<unknown, unknown, unknown> = current
                current = new XIO.Chain(message, () => oldEffect)
              } else if (opCount === yieldOpCount) {
                this.unsafeRunLater(current)
                current = null
              } else {
                switch (current._tag) {
                  case XIO.Tag.Chain: {
                    const nested = current.xio as XIO.Concrete
                    const k = current.f

                    switch (nested._tag) {
                      case XIO.Tag.SucceedNow: {
                        current = k(nested.value) as XIO.Concrete
                        break
                      }
                      case XIO.Tag.Succeed: {
                        current = k(nested.value()) as XIO.Concrete
                        break
                      }
                      default: {
                        this.stack.push(new ApplyFrame(current.f))
                        current = current.xio as XIO.Concrete
                        break
                      }
                    }
                    break
                  }
                  case XIO.Tag.Succeed: {
                    current = this.unsafeNextEffect(current.value())
                    break
                  }
                  case XIO.Tag.SucceedNow: {
                    current = this.unsafeNextEffect(current.value)
                    break
                  }
                  case XIO.Tag.Fail: {
                    const cause = current.cause()
                    const discardedFolds = this.unsafeUnwindStack()
                    const strippedCause = discardedFolds ? C.stripFailures(cause) : cause
                    const suppressed = this.unsafeClearSuppressed()
                    const fullCause = C.contains(suppressed)(strippedCause)
                      ? strippedCause
                      : C.then(strippedCause, suppressed)
                    if (this.stack.hasNext) {
                      this.unsafeSetInterrupting(false)
                      current = this.unsafeNextEffect(fullCause)
                    } else {
                      this.unsafeSetInterrupting(true)
                      current = this.unsafeTryDone(Ex.failCause(fullCause as C.Cause<E>))
                    }
                    break
                  }
                  case XIO.Tag.Match: {
                    this.stack.push(new MatchFrame(current.onFailure, current.onSuccess))
                    current = current.xio as XIO.Concrete
                    break
                  }
                  case XIO.Tag.Async: {
                    const epoch = this.asyncEpoch
                    this.asyncEpoch = epoch + 1
                    this.unsafeEnterAsync(epoch, current.blockingOn)
                    const r = current.register(this.unsafeCreateAsyncResume(epoch))
                    switch (r._tag) {
                      case "Left": {
                        this.unsafeSetAsyncCanceler(epoch, r.left)
                        if (this.unsafeShouldInterrupt()) {
                          if (this.unsafeExitAsync(epoch)) {
                            this.unsafeSetInterrupting(true)
                            current = new XIO.Chain(
                              r.left,
                              () => new XIO.Fail(() => this.unsafeClearSuppressed()),
                            ) as XIO.Concrete
                          } else {
                            current = null
                          }
                        } else {
                          current = null
                        }
                        break
                      }
                      case "Right": {
                        if (!this.unsafeExitAsync(epoch)) {
                          current = null
                        } else {
                          current = r.right as XIO.Concrete
                        }
                        break
                      }
                    }
                    break
                  }
                  case XIO.Tag.Defer: {
                    current = current.xio() as XIO.Concrete
                    break
                  }
                  case XIO.Tag.Ensuring: {
                    this.unsafeAddFinalizer(current.finalizer)
                    current = current.xio as XIO.Concrete
                    break
                  }
                  case XIO.Tag.Fork: {
                    current = this.unsafeNextEffect(this.unsafeFork(current.xio, current.scope))
                    break
                  }
                  case XIO.Tag.Yield: {
                    current = null
                    this.unsafeRunLater(XIO.unit)
                    break
                  }
                  case XIO.Tag.GetInterruptStatus: {
                    current = current.f(
                      InterruptStatus.fromBoolean(this.unsafeIsInterruptible()),
                    ) as XIO.Concrete
                    break
                  }
                  case XIO.Tag.SetInterruptStatus: {
                    const boolFlag = InterruptStatus.isInterruptible(current.flag)
                    if (this.interruptStatus.peekOrElse(true) !== boolFlag) {
                      this.interruptStatus.push(boolFlag)
                      this.unsafeRestoreInterruptStatus()
                    }
                    current = current.xio as XIO.Concrete
                    break
                  }
                  case XIO.Tag.Descriptor: {
                    current = current.f(this.unsafeGetDescriptor()) as XIO.Concrete
                    break
                  }
                  case XIO.Tag.FiberRefModifyAll: {
                    const [result, newValue] = current.f(
                      this.fiberId,
                      new FiberRefs(this.fiberRefLocals),
                    )
                    this.fiberRefLocals = newValue.fiberRefLocals
                    current = this.unsafeNextEffect(result)
                    break
                  }
                  case XIO.Tag.FiberRefModify: {
                    const [result, newValue] = current.f(this.unsafeGetRef(current.fiberRef))
                    this.unsafeSetRef(current.fiberRef, newValue)
                    current = this.unsafeNextEffect(result)
                    break
                  }
                  case XIO.Tag.FiberRefLocally: {
                    const xio = current
                    const fiberRef = xio.fiberRef
                    const oldValue = this.unsafeGetRef(fiberRef)
                    this.unsafeSetRef(fiberRef, xio.localValue)
                    this.unsafeAddFinalizer(
                      new XIO.FromIO(() => this.unsafeSetRef(fiberRef, oldValue)),
                    )
                    current = xio.xio as XIO.Concrete
                    break
                  }
                  case XIO.Tag.FiberRefDelete: {
                    this.unsafeDeleteRef(current.fiberRef)
                    current = this.unsafeNextEffect(undefined)
                    break
                  }
                  case XIO.Tag.FiberRefWith: {
                    current = current.f(this.unsafeGetRef(current.fiberRef)) as XIO.Concrete
                    break
                  }
                  case XIO.Tag.Read: {
                    current = current.reader(this.unsafeGetRef(currentEnvironment)) as XIO.Concrete
                    break
                  }
                  case XIO.Tag.Provide: {
                    const oldEnvironment = this.unsafeGetRef(currentEnvironment)
                    this.unsafeSetRef(currentEnvironment, current.environment)
                    this.unsafeAddFinalizer(
                      XIO.fromIO(() => {
                        this.unsafeSetRef(currentEnvironment, oldEnvironment)
                      }),
                    )
                    current = current.xio as XIO.Concrete
                    break
                  }
                  case XIO.Tag.GetForkScope: {
                    const xio = current
                    current = xio.f(
                      pipe(
                        this.unsafeGetRef(forkScopeOverride),
                        O.getOrElse(() => this.scope),
                      ),
                    ) as XIO.Concrete
                    break
                  }
                  case XIO.Tag.OverrideForkScope: {
                    const xio = current
                    const oldForkScopeOverride = this.unsafeGetRef(forkScopeOverride)
                    this.unsafeSetRef(forkScopeOverride, xio.forkScope)
                    this.unsafeAddFinalizer(
                      new XIO.FromIO(() =>
                        this.unsafeSetRef(forkScopeOverride, oldForkScopeOverride),
                      ),
                    )
                    current = xio.xio as XIO.Concrete
                    break
                  }
                  case XIO.Tag.RaceWith: {
                    current = this.unsafeRace(current)
                    break
                  }
                  case XIO.Tag.Sync: {
                    current = XIO.done(Sync.run(current)) as XIO.Concrete
                    break
                  }
                }
              }
            } else {
              current = new XIO.Fail(() => this.unsafeClearSuppressed())
              this.unsafeSetInterrupting(true)
            }
            opCount += 1
          }
        } catch (e) {
          this.unsafeSetInterrupting(true)
          current = new XIO.Fail(() => C.halt(e))
        }
      }
    } finally {
      //
    }
  }

  private interruptExit = new InterruptExit((v) => {
    if (this.unsafeIsInterruptible()) {
      this.interruptStatus.pop()
      return new XIO.Succeed(v)
    } else {
      return new XIO.FromIO(() => {
        this.interruptStatus.pop()
        return v
      })
    }
  })

  unsafeRunNow(xio: ErasedEffect) {
    this.nextEffect = xio
    this.runUntil(this.runtimeConfig.yieldOpCount)
  }

  unsafeRunLater(xio: ErasedEffect) {
    this.unsafeGetRef(currentScheduler).scheduleTask(() => {
      this.nextEffect = xio
      this.runUntil(this.runtimeConfig.yieldOpCount)
    })
  }

  unsafePoll(): O.Option<Ex.Exit<E, A>> {
    switch (this.state._tag) {
      case State.Tag.Executing: {
        return O.none
      }
      case State.Tag.Done: {
        return O.some(this.state.value)
      }
    }
  }

  private unsafeDisableInterruption(): void {
    this.interruptStatus.push(false)
  }

  private unsafeRestoreInterruptStatus(): void {
    this.stack.push(this.interruptExit)
  }

  private unsafeIsInterruptible() {
    return this.interruptStatus.peekOrElse(true)
  }

  private unsafeAddFinalizer(finalizer: XIO.XIO<unknown, never, unknown>): void {
    this.stack.push(
      new Finalizer(finalizer, (v) => {
        this.unsafeDisableInterruption()
        this.unsafeRestoreInterruptStatus()
        return new XIO.Chain(finalizer, () => new XIO.Succeed(v))
      }),
    )
  }

  private unsafeIsInterrupted() {
    return HS.size(this.state.interruptors) > 0
  }

  private unsafeIsInterrupting() {
    return this.state.status.isInterrupting
  }

  private unsafeShouldInterrupt() {
    return (
      this.unsafeIsInterrupted() && this.unsafeIsInterruptible() && !this.unsafeIsInterrupting()
    )
  }

  private unsafeAddSuppressedCause(cause: C.Cause<never>): void {
    if (!C.isEmpty(cause)) {
      if (this.state._tag === State.Tag.Executing) {
        this.state.suppressed = C.then(this.state.suppressed, cause)
      }
    }
  }

  /**
   * Successively pops frames off the stack, looking for the first error handler.
   * Along the way, exits interruptible / uninterruptible regions, as well as executes
   * finalizers.
   */
  private unsafeUnwindStack() {
    let unwinding = true
    let discardedFolds = false
    while (unwinding && this.stack.hasNext) {
      const frame = this.stack.pop()!
      switch (frame._tag) {
        case FrameTag.InterruptExit: {
          this.interruptStatus.pop()
          break
        }
        case FrameTag.Finalizer: {
          this.unsafeDisableInterruption()
          this.stack.push(
            new ApplyFrame(
              (cause) =>
                new XIO.Match(
                  frame.finalizer,
                  (finalizerCause) => {
                    this.interruptStatus.pop()
                    this.unsafeAddSuppressedCause(finalizerCause)
                    return new XIO.Fail(() => cause as C.Cause<unknown>)
                  },
                  () => {
                    this.interruptStatus.pop()
                    return new XIO.Fail(() => cause as C.Cause<unknown>)
                  },
                ),
            ),
          )
          unwinding = false
          break
        }
        case FrameTag.MatchFrame: {
          if (!this.unsafeShouldInterrupt()) {
            this.stack.push(new HandlerFrame(frame.onFailure as ErasedContinuation))
            unwinding = false
          } else {
            discardedFolds = true
          }
          break
        }
        default: {
          break
        }
      }
    }
    return discardedFolds
  }

  unsafeAddObserver(k: State.Callback<never, Ex.Exit<E, A>>): Ex.Exit<E, A> | null {
    switch (this.state._tag) {
      case State.Tag.Done: {
        return this.state.value
      }
      case State.Tag.Executing: {
        this.state.observers.add(k)
        return null
      }
    }
  }

  unsafeOnDone(k: State.Callback<never, Ex.Exit<E, A>>): void {
    const r = this.unsafeAddObserver(k)
    if (r !== null) {
      k(Ex.succeed(r))
    }
  }

  private unsafeNotifyObservers(
    v: Ex.Exit<E, A>,
    observers: ReadonlySet<State.Callback<never, Ex.Exit<E, A>>>,
  ): void {
    if (observers.size > 0) {
      const result = Ex.succeed(v)
      observers.forEach((k) => k(result))
    }
  }

  private unsafeRemoveObserver(k: State.Callback<never, Ex.Exit<E, A>>): void {
    if (this.state._tag === State.Tag.Executing) {
      this.state.observers.delete(k)
    }
  }

  private unsafeInterruptAs(fiberId: FId.FiberId): XIO.XIO<unknown, never, Ex.Exit<E, A>> {
    const interruptedCause = C.interrupt(fiberId)
    return new XIO.Defer(() => {
      const oldState = this.state
      if (
        this.state._tag === State.Tag.Executing &&
        this.state.status._tag === Status.Tag.Suspended &&
        this.state.status.isInterruptible &&
        this.state.asyncCanceler._tag === CancellerState.Tag.Registered
      ) {
        const asyncCanceler = this.state.asyncCanceler.asyncCanceler
        const interrupt = new XIO.Fail(() => interruptedCause)
        this.state.status = new Status.Running(true)
        this.state.interruptors = pipe(oldState.interruptors, HS.add(fiberId))
        this.state.asyncCanceler = CancellerState.Empty
        this.unsafeRunLater(new XIO.Chain(asyncCanceler, () => interrupt))
      } else if (this.state._tag === State.Tag.Executing) {
        this.state.interruptors = pipe(this.state.interruptors, HS.add(fiberId))
        this.state.suppressed = C.then(this.state.suppressed, interruptedCause)
      }
      return this.await
    })
  }

  private unsafeSetInterrupting(value: boolean): void {
    if (this.state._tag === State.Tag.Executing) {
      this.state.status = this.state.status.withInterrupting(value)
    }
  }

  private unsafeTryDone(exit: Ex.Exit<E, A>): XIO.Concrete | null {
    switch (this.state._tag) {
      case State.Tag.Done: {
        // Already done
        return null
      }
      case State.Tag.Executing: {
        if (this.state.mailbox !== null) {
          const mailbox = this.state.mailbox
          this.state.mailbox = null
          this.unsafeSetInterrupting(true)
          return new XIO.Chain(mailbox, () => XIO.done(exit))
        } else if (this._children.size === 0) {
          // We are truly "done" because all the children of this fiber have terminated,
          // and there are no more pending effects that we have to execute on the fiber.
          const interruptorsCause = State.interruptorsCause(this.state)

          const newExit = C.isEmpty(interruptorsCause)
            ? exit
            : pipe(
                exit,
                Ex.mapErrorCause((cause) =>
                  C.contains(interruptorsCause)(cause) ? cause : C.then(cause, interruptorsCause),
                ),
              )

          const observers = this.state.observers

          this.state = new State.Done(newExit)

          this.unsafeNotifyObservers(newExit, observers)

          return null
        } else {
          // not done because there are children left to close
          this.unsafeSetInterrupting(true)
          let interruptChildren = XIO.unit

          this._children.forEach((child) => {
            interruptChildren = new XIO.Chain(interruptChildren, () =>
              child.interruptAs(this.fiberId),
            )
          })
          this._children.clear()

          return new XIO.Chain(interruptChildren, () => XIO.done(exit)) as XIO.Concrete
        }
      }
    }
  }

  private unsafeSetAsyncCanceler(epoch: number, asyncCanceler0: ErasedEffect | null): void {
    const asyncCanceler = asyncCanceler0 === null ? XIO.unit : asyncCanceler0
    if (this.state._tag === State.Tag.Executing) {
      if (
        this.state.status._tag === Status.Tag.Suspended &&
        this.state.asyncCanceler._tag === CancellerState.Tag.Pending &&
        this.state.status.asyncEpoch === epoch
      ) {
        this.state.asyncCanceler = CancellerState.Registered(asyncCanceler)
      } else if (
        this.state.status._tag === Status.Tag.Suspended &&
        this.state.asyncCanceler._tag === CancellerState.Tag.Registered &&
        this.state.status.asyncEpoch === epoch
      ) {
        throw new Error("inconsistent state in unsafeSetAsyncCanceler")
      }
    } else {
      return
    }
  }

  private unsafeEnterAsync(epoch: number, blockingOn: FId.FiberId): void {
    if (
      this.state._tag === State.Tag.Executing &&
      this.state.status._tag === Status.Tag.Running &&
      this.state.asyncCanceler._tag === CancellerState.Tag.Empty
    ) {
      this.state.status = new Status.Suspended(
        this.state.status.isInterrupting,
        this.unsafeIsInterruptible() && !this.unsafeIsInterrupting(),
        epoch,
        blockingOn,
      )
      this.state.asyncCanceler = CancellerState.Pending
    } else {
      throw new Error("Attempted to unsafeEnterAsync on a Fiber that is not running")
    }
  }

  private unsafeExitAsync(epoch: number): boolean {
    if (
      this.state._tag === State.Tag.Executing &&
      this.state.status._tag === Status.Tag.Suspended &&
      this.state.status.asyncEpoch === epoch
    ) {
      this.state.status = new Status.Running(this.state.status.isInterrupting)
      this.state.asyncCanceler = CancellerState.Empty
      return true
    }
    return false
  }

  private unsafeCreateAsyncResume(epoch: number) {
    return (xio: ErasedEffect) => {
      if (this.unsafeExitAsync(epoch)) {
        this.unsafeRunLater(xio)
      }
    }
  }

  private unsafeClearSuppressed(): C.Cause<never> {
    switch (this.state._tag) {
      case State.Tag.Executing: {
        const suppressed = this.state.suppressed
        const interruptorsCause = State.interruptorsCause(this.state)
        this.state.suppressed = C.empty()

        if (C.contains(interruptorsCause)(suppressed)) {
          return suppressed
        } else {
          return C.then(suppressed, interruptorsCause)
        }
      }
      case State.Tag.Done: {
        return State.interruptorsCause(this.state)
      }
    }
  }

  private unsafeNextEffect(value: unknown): XIO.Concrete | null {
    if (this.stack.hasNext) {
      const k = this.stack.pop()!
      return k.apply(value) as XIO.Concrete
    } else {
      return this.unsafeTryDone(Ex.succeed(value as A))
    }
  }

  private unsafeDrainMailbox(): XIO.XIO<unknown, never, unknown> | null {
    if (this.state._tag === State.Tag.Executing) {
      const mailbox = this.state.mailbox
      this.state.mailbox = null
      return mailbox
    } else {
      return null
    }
  }

  private unsafeGetRef<A>(fiberRef: FR.FiberRef<A>): A {
    return pipe(
      this.fiberRefLocals,
      HM.get<FR.FiberRef<unknown>>(fiberRef),
      O.map((stack) => stack.head[1]),
      O.getOrElseW(() => FR.initial(fiberRef)),
    ) as A
  }

  private unsafeDeleteRef<A>(fiberRef: FR.FiberRef<A>): void {
    this.fiberRefLocals = pipe(this.fiberRefLocals, HM.remove(fiberRef as FR.FiberRef<unknown>))
  }

  private unsafeSetRef<A>(fiberRef: FR.FiberRef<A>, value: A): void {
    const oldStack = pipe(
      this.fiberRefLocals,
      HM.get<FR.FiberRef<unknown>>(fiberRef),
      O.getOrElse(() => L.empty()),
    )
    let newStack: L.Cons<[FId.Runtime, unknown]>
    if (L.isEmpty(oldStack)) {
      newStack = L.cons([this.fiberId, value])
    } else {
      newStack = L.cons([this.fiberId, value], oldStack.tail)
    }
    this.fiberRefLocals = pipe(
      this.fiberRefLocals,
      HM.set(fiberRef as FR.FiberRef<unknown>, newStack),
    )
  }

  private unsafeFork(xio: ErasedEffect, forkScope: O.Option<FiberScope> = O.none) {
    const childId = FId.unsafeMake()

    const childFiberRefLocals = pipe(
      this.fiberRefLocals,
      HM.mapWithIndex((fiberRef, stack) =>
        L.cons(
          [childId, pipe(fiberRef, FR.patch(FR.fork(fiberRef), stack.head[1]))] as [
            FId.Runtime,
            unknown,
          ],
          stack,
        ),
      ),
    )

    const parentScope = pipe(
      forkScope,
      O.alt(() => this.unsafeGetRef(forkScopeOverride)),
      O.getOrElse(() => this.scope),
    )

    const grandChildren = new Set<FiberContext<unknown, unknown>>()

    const childContext = new FiberContext(
      childId,
      this.runtimeConfig,
      new Stack({ value: this.unsafeIsInterruptible() }),
      childFiberRefLocals,
      grandChildren,
    )

    const childXIO = !parentScope.unsafeAdd(childContext)
      ? XIO.interruptAs(parentScope.fiberId)
      : xio

    childContext.nextEffect = childXIO

    this.unsafeGetRef(currentScheduler).scheduleTask(() =>
      childContext.runUntil(this.runtimeConfig.yieldOpCount),
    )

    return childContext
  }

  unsafeAddChild(child: FiberContext<unknown, unknown>): boolean {
    return this.unsafeEvalOn(
      new XIO.FromIO(() => {
        this._children.add(child)
      }),
    )
  }

  unsafeEvalOn(xio: XIO.XIO<unknown, never, unknown>): boolean {
    if (this.state._tag === State.Tag.Executing) {
      const newMailbox =
        this.state.mailbox === null ? xio : new XIO.Chain(this.state.mailbox, () => xio)
      this.state.mailbox = newMailbox
      return true
    } else {
      return false
    }
  }

  evalOn(effect: XIO.UIO<void>, orElse: XIO.UIO<void>): XIO.UIO<void> {
    return new XIO.Defer(() => {
      if (this.unsafeEvalOn(effect)) {
        return XIO.unit
      } else {
        return orElse
      }
    })
  }

  unsafeGetDescriptor(): FiberDescriptor {
    return {
      id: this.id,
      status: this.state.status,
      interruptors: this.state.interruptors,
      interruptStatus: InterruptStatus.fromBoolean(this.unsafeIsInterruptible()),
    }
  }

  private unsafeRace<R, E, A, R1, E1, B, E2, C, E3, D>(
    race: XIO.RaceWith<R, E, A, R1, E1, B, unknown, E2, C, unknown, E3, D>,
  ): XIO.Concrete {
    const raceIndicator = new AtomicBoolean(true)

    const left = this.unsafeFork(race.left) as FiberContext<E, A>
    const right = this.unsafeFork(race.right) as FiberContext<E1, B>

    return XIO.async<unknown, unknown, unknown>((cb) => {
      const leftRegister = left.unsafeAddObserver(() => {
        this.completeRace(left, right, race.leftWins, raceIndicator, cb)
      })
      if (leftRegister !== null) {
        this.completeRace(left, right, race.leftWins, raceIndicator, cb)
      } else {
        const rightRegister = right.unsafeAddObserver(() => {
          this.completeRace(right, left, race.rightWins, raceIndicator, cb)
        })
        if (rightRegister !== null) {
          this.completeRace(right, left, race.rightWins, raceIndicator, cb)
        }
      }
    }, pipe(left.id, FId.combine(right.id))) as XIO.Concrete
  }

  private completeRace<E, A, E1, B, R2, E2, C, R3, E3, D>(
    winner: Fiber<E, A>,
    loser: Fiber<E1, B>,
    cont: (winner: Fiber<E, A>, loser: Fiber<E1, B>) => XIO.XIO<R2 & R3, E2 | E3, C | D>,
    ab: AtomicBoolean,
    cb: (xio: XIO.XIO<R2 & R3, E2 | E3, C | D>) => void,
  ): void {
    if (ab.compareAndSet(true, false)) {
      cb(cont(winner, loser))
    }
  }
}

export function isFiberContext<A>(
  u: A,
  // @ts-expect-error -- conditional
): u is A extends FiberContext<infer E, infer A>
  ? FiberContext<E, A>
  : FiberContext<unknown, unknown> {
  return hasTypeId(u, FiberContextTypeId)
}
