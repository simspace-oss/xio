import type { Exit } from "../../Exit"
import type * as FId from "../../FiberId"
import type {
  Fiber as FiberDefinition,
  Runtime as RuntimeDefinition,
  Synthetic as SyntheticDefinition,
} from "../definition"
import type { FiberStatus } from "./FiberStatus"
import type * as XIO from "./XIO"

import { FiberTypeId, RuntimeTypeId, SyntheticTypeId } from "../definition"

export abstract class Fiber<E, A> implements FiberDefinition<E, A> {
  readonly [FiberTypeId]: FiberTypeId = FiberTypeId
  readonly _E!: () => E
  readonly _A!: () => A

  abstract readonly id: FId.FiberId
  abstract readonly await: XIO.UIO<Exit<E, A>>
  abstract interruptAs(fiberId: FId.FiberId): XIO.UIO<Exit<E, A>>
  abstract inheritRefs: XIO.UIO<void>
}

export abstract class Runtime<E, A> extends Fiber<E, A> implements RuntimeDefinition<E, A> {
  readonly [RuntimeTypeId]: RuntimeTypeId = RuntimeTypeId
  abstract evalOn(effect: XIO.UIO<void>, orElse: XIO.UIO<void>): XIO.UIO<void>
  abstract override id: FId.Runtime
  abstract status: XIO.UIO<FiberStatus>
}

export class Synthetic<E, A> extends Fiber<E, A> implements SyntheticDefinition<E, A> {
  readonly [SyntheticTypeId]: SyntheticTypeId = SyntheticTypeId
  readonly id: FId.FiberId
  readonly await: XIO.UIO<Exit<E, A>>
  readonly interruptAs: (fiberId: FId.FiberId) => XIO.UIO<Exit<E, A>>
  readonly inheritRefs: XIO.UIO<void>
  constructor(
    id: FId.FiberId,
    wait: XIO.UIO<Exit<E, A>>,
    interruptAs: (fiberid: FId.FiberId) => XIO.UIO<Exit<E, A>>,
    inheritRefs: XIO.UIO<void>,
  ) {
    super()
    this.id = id
    this.await = wait
    this.interruptAs = interruptAs
    this.inheritRefs = inheritRefs
  }
}

export function concrete<E, A>(
  _: FiberDefinition<E, A>,
): asserts _ is Runtime<E, A> | Synthetic<E, A> {
  //
}
