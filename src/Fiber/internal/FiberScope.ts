import type { FiberContext } from "./FiberContext"

import * as FId from "../../FiberId"

export const FiberScopeTypeId = Symbol.for("@simspace/xio/Fiber/FiberScope")
export type FiberScopeTypeId = typeof FiberScopeTypeId

export abstract class FiberScope {
  readonly _typeId: FiberScopeTypeId = FiberScopeTypeId
  abstract readonly fiberId: FId.FiberId
  abstract unsafeAdd(child: FiberContext<unknown, unknown>): boolean
}

export class Global extends FiberScope {
  fiberId = FId.none
  unsafeAdd(_child: FiberContext<unknown, unknown>): boolean {
    return true
  }
}

export const globalScope = new Global()

export class Local extends FiberScope {
  constructor(
    readonly fiberId: FId.FiberId,
    private parentRef: WeakRef<FiberContext<unknown, unknown>>,
  ) {
    super()
  }

  unsafeAdd(child: FiberContext<unknown, unknown>): boolean {
    const parent = this.parentRef.deref()
    if (parent !== undefined) {
      return parent.unsafeAddChild(child)
    } else {
      return false
    }
  }
}

export function unsafeMake<E, A>(fiber: FiberContext<E, A>): FiberScope {
  return new Local(fiber.id, new WeakRef(fiber as FiberContext<unknown, unknown>))
}
