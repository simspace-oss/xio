import type { Cause } from "../../Cause"
import type { Exit } from "../../Exit"
import type { FiberId } from "../../FiberId"
import type { XIO } from "../../XIO/definition"

import * as HS from "@simspace/collections/HashSet"
import { pipe } from "fp-ts/function"

import * as C from "../../Cause"
import * as CS from "./CancelerState"
import * as FS from "./FiberStatus"

export const enum Tag {
  Executing,
  Done,
}

export type Callback<E, A> = (_: Exit<E, A>) => void

export class Executing<E, A> {
  readonly _tag = Tag.Executing
  constructor(
    public status: FS.FiberStatus,
    readonly observers: Set<Callback<never, Exit<E, A>>>,
    public suppressed: C.Cause<never>,
    public interruptors: HS.HashSet<FiberId>,
    public asyncCanceler: CS.CancelerState,
    public mailbox: XIO<unknown, never, unknown> | null,
  ) {}
}

export class Done<E, A> {
  readonly _tag = Tag.Done
  constructor(readonly value: Exit<E, A>) {}
  suppressed: C.Cause<never> = C.empty()
  readonly status: FS.FiberStatus = new FS.Done()
  readonly interruptors: HS.HashSet<FiberId> = HS.makeDefault()
}

export type FiberState<E, A> = Executing<E, A> | Done<E, A>

export function initial<E, A>(): FiberState<E, A> {
  return new Executing(
    new FS.Running(false),
    new Set(),
    C.empty(),
    HS.makeDefault(),
    CS.Empty,
    null,
  )
}

export function interruptorsCause<E, A>(state: FiberState<E, A>): Cause<never> {
  return pipe(
    state.interruptors,
    HS.reduce(C.empty(), (acc, interruptor) => C.then(acc, C.interrupt(interruptor))),
  )
}
