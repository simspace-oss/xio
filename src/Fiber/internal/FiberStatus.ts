import type { FiberId } from "../../FiberId"

export const enum Tag {
  Done,
  Running,
  Suspended,
}

export class Done {
  readonly _tag = Tag.Done
  readonly isInterrupting = false
  withInterrupting(_newInterrupting: boolean): FiberStatus {
    return this
  }
}

export class Running {
  readonly _tag = Tag.Running
  constructor(readonly isInterrupting: boolean) {}

  withInterrupting(newInterrupting: boolean): FiberStatus {
    return new Running(newInterrupting)
  }
}

export class Suspended {
  readonly _tag = Tag.Suspended
  constructor(
    readonly isInterrupting: boolean,
    readonly isInterruptible: boolean,
    readonly asyncEpoch: number,
    readonly blockingOn: FiberId,
  ) {}

  withInterrupting(newInterrupting: boolean): FiberStatus {
    return new Suspended(newInterrupting, this.isInterruptible, this.asyncEpoch, this.blockingOn)
  }
}

export type FiberStatus = Done | Running | Suspended
