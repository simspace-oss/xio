import * as V from "@simspace/collections/Vector"
import { pipe } from "fp-ts/function"

import * as C from "../src/Cause"
import * as Ex from "../src/Exit"
import { assert } from "../src/internal/util"
import * as Sync from "../src/Sync"
import { tag } from "../src/Tag"

const TagString = tag<string>("sync_test_string")
const TagX = tag<{ x: number }>("sync_test_X")
const TagY = tag<{ y: number }>("sync_test_Y")
const TagZ = tag<{ z: number }>("sync_test_Z")

describe("Z", () => {
  test.concurrent("modify", () => {
    const result = pipe(
      Sync.modify((s1: V.Vector<number>) => [
        s1,
        pipe(
          s1,
          V.map((n) => n.toString()),
        ),
      ]),
      Sync.unsafeRunState(V.vector(1, 2, 3)),
    )

    expect(result).toEqual([V.vector("1", "2", "3"), V.vector(1, 2, 3)])
  })

  describe("matchE", () => {
    test.concurrent("failure", () => {
      const failing = pipe(
        Sync.succeed(1),
        Sync.chain((n) => (n % 2 !== 0 ? Sync.fail("fail") : Sync.succeed(n))),
      )
      const result = pipe(
        failing,
        Sync.matchE(
          () =>
            pipe(
              Sync.update((_: number) => _ + 1),
              Sync.apSecond(Sync.succeed(0)),
            ),
          (a) =>
            pipe(
              Sync.update((_: number) => _ + 2),
              Sync.apSecond(Sync.succeed(a)),
            ),
        ),
        Sync.unsafeRunState(10),
      )
      expect(result).toEqual([11, 0])
    })

    test.concurrent("success", () => {
      const failing = pipe(
        Sync.succeed(2),
        Sync.chain((n) => (n % 2 !== 0 ? Sync.fail("fail") : Sync.succeed(n))),
      )
      const result = pipe(
        failing,
        Sync.matchE(
          () =>
            pipe(
              Sync.update((_: number) => _ + 1),
              Sync.apSecond(Sync.succeed(0)),
            ),
          (a) =>
            pipe(
              Sync.update((_: number) => _ + 2),
              Sync.apSecond(Sync.succeed(a)),
            ),
        ),
        Sync.unsafeRunState(10),
      )
      expect(result).toEqual([12, 2])
    })
  })

  describe("RTS", () => {
    test.concurrent("succeed->fail->chain", () => {
      const ex = pipe(
        Sync.succeed(1),
        Sync.chain(() => Sync.fail("oops!")),
        Sync.chain(() => Sync.succeed(0)),
        Sync.runAll(undefined),
      )
      assert(Ex.isFailure(ex))
      expect(pipe(ex.cause, C.contains(C.fail("oops!")))).toEqual(true)
    })

    test.concurrent("simple environment", () => {
      const effect = Sync.askService(TagString)
      expect(pipe(effect, Sync.provideService(TagString, "hello!"), Sync.unsafeRun)).toEqual(
        "hello!",
      )
    })

    test.concurrent("multiple environments", () => {
      const result = pipe(
        Sync.askService(TagX),
        Sync.provideService(TagX, { x: 2 }),
        Sync.chain(({ x }) => Sync.asksServiceE(TagY, ({ y }) => Sync.succeed(x + y))),
        Sync.provideService(TagY, { y: 3 }),
        Sync.unsafeRun,
      )

      expect(result).toEqual(5)
    })

    test.concurrent("provide is scoped correctly", () => {
      const result = pipe(
        Sync.Do,
        Sync.bind("start", () => Sync.askService(TagX)),
        Sync.bind("inner", () =>
          pipe(
            Sync.Do,
            Sync.bind("innerStart", () => Sync.askService(TagY)),
            Sync.bind("innerInner", () =>
              pipe(Sync.askService(TagX), Sync.provideService(TagX, { x: 111 })),
            ),
            Sync.bind("innerEnd", () => Sync.askService(TagY)),
            Sync.map(
              ({ innerStart, innerInner, innerEnd }) =>
                [innerStart.y, innerInner.x, innerEnd.y] as const,
            ),
            Sync.provideService(TagY, { y: 11 }),
          ),
        ),
        Sync.bind("end", () => Sync.askService(TagZ)),
        Sync.map(({ start, inner, end }) => [start.x, inner, end.z] as const),
        Sync.provideService(TagZ, { z: 1 }),
        Sync.provideService(TagX, { x: 1 }),
        Sync.unsafeRun,
      )

      expect(result).toEqual([1, [11, 111, 11], 1])
    })

    test.concurrent("provided environment should be restored on error", () => {
      const result = pipe(
        Sync.fail(undefined),
        Sync.provideService(TagX, { x: 1 }),
        Sync.either,
        Sync.chain(() => Sync.askService(TagX)),
        Sync.provideService(TagX, { x: 0 }),
        Sync.unsafeRun,
      )

      expect(result.x).toEqual(0)
    })

    test.concurrent("throw in succeedLazy", () => {
      const error = new Error("oh noes")
      const effect = Sync.fromIO(() => {
        throw error
      })
      const result = Sync.run(effect)
      assert(Ex.isFailure(result))
      expect(pipe(result.cause, C.contains(C.halt(error)))).toEqual(true)
    })

    test.concurrent("throw in succeedLazy with continuation", () => {
      const error = new Error("oh noes")
      const result = pipe(
        Sync.fromIO(() => {
          throw error
        }),
        Sync.matchCauseE(
          () => Sync.succeed(1),
          () => Sync.succeed(0),
        ),
        Sync.unsafeRun,
      )
      expect(result).toEqual(1)
    })
  })
})
