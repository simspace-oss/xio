import * as Equ from "@simspace/collections/Equatable"
import { pipe } from "fp-ts/function"

import * as C from "../src/Cause"
import * as Ex from "../src/Exit"
import * as Fi from "../src/Fiber"
import * as FId from "../src/FiberId"
import { assert } from "../src/internal/util"
import * as XIO from "../src/XIO"

describe("Fiber", () => {
  test.concurrent("Fiber.join on interrupted fiber is inner interruption", async () => {
    const fiberId = FId.runtime(0, 123)
    const exit = await pipe(
      Fi.done(Ex.interrupt(fiberId)),
      Fi.join,
      XIO.result,
      XIO.unsafeRunPromise,
    )
    assert(Ex.isFailure(exit))
    expect(Equ.strictEqualsUnknown(exit.cause, C.interrupt(fiberId))).toEqual(true)
  })
})
