import type { Has } from "../src/Tag"

import * as V from "@simspace/collections/Vector"
import { pipe } from "fp-ts/function"

import * as Ref from "../src/Ref"
import * as Scope from "../src/Scope"
import * as XIO from "../src/XIO"
import { test } from "./utils"

class Acquire {
  constructor(readonly id: number) {}
}

class Use {
  constructor(readonly id: number) {}
}

class Release {
  constructor(readonly id: number) {}
}

type Action = Acquire | Use | Release

const resource = (
  id: number,
  ref: Ref.Ref<V.Vector<Action>>,
): XIO.XIO<Has<Scope.Scope>, never, number> =>
  pipe(
    ref,
    Ref.update(V.append(new Acquire(id))),
    XIO.map(() => id),
    XIO.uninterruptible,
    XIO.ensuring(Scope.addFinalizer(pipe(ref, Ref.update(V.append(new Release(id)))))),
  )

describe("Scope", () => {
  test("runs finalizers when scope is closed", () =>
    XIO.gen(function* (_) {
      const ref = yield* _(Ref.make<V.Vector<Action>>(V.empty()))
      yield* _(
        XIO.scoped(
          XIO.gen(function* (_) {
            const res = yield* _(resource(1, ref))
            yield* _(pipe(ref, Ref.update(V.append(new Use(res)))))
          }),
        ),
      )
      const actions = yield* _(Ref.get(ref))
      expect(actions).toEqual(V.vector(new Acquire(1), new Use(1), new Release(1)))
    }))

  test("using", () =>
    XIO.gen(function* (_) {
      const ref1 = yield* _(Ref.make<V.Vector<Action>>(V.empty()))
      const ref2 = yield* _(Ref.make<V.Vector<Action>>(V.empty()))
      yield* _(
        pipe(
          resource(1, ref1),
          XIO.using(() =>
            pipe(
              ref1,
              Ref.update(V.append(new Use(1))),
              XIO.chain(() => resource(2, ref2)),
            ),
          ),
          XIO.chain(() => pipe(ref2, Ref.update(V.append(new Use(2))))),
          XIO.scoped,
        ),
      )
      const actions1 = yield* _(Ref.get(ref1))
      const actions2 = yield* _(Ref.get(ref2))
      expect(actions1).toEqual(V.vector(new Acquire(1), new Use(1), new Release(1)))
      expect(actions2).toEqual(V.vector(new Acquire(2), new Use(2), new Release(2)))
    }))
})
