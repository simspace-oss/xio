import { pipe } from "fp-ts/function"

import * as Ev from "../src/Eval"

const iterativeFactorial = (n: bigint): bigint => {
  let result = n
  while (n > 1) {
    result = result * --n
  }
  return result
}

const factorial = (n: bigint): Ev.Eval<bigint> =>
  Ev.defer(() => {
    if (n <= BigInt(1)) {
      return Ev.now(BigInt(1))
    }
    return pipe(
      factorial(n - BigInt(1)),
      Ev.map((next) => n * next),
    )
  })

const pointlessRecursion = (n: number): Ev.Eval<void> =>
  Ev.defer(() => {
    if (n === 0) {
      return Ev.now(undefined)
    }
    return pointlessRecursion(n - 1)
  })

describe("Eval", () => {
  describe("unsafeRunEval", () => {
    it("should be stack safe", () => {
      expect(() => Ev.unsafeRunEval(pointlessRecursion(10000))).not.toThrow()
    })
    it("should evaluate an Eval", () => {
      expect(Ev.unsafeRunEval(factorial(BigInt(100)))).toEqual(iterativeFactorial(BigInt(100)))
    })
  })
})
