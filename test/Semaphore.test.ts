import * as V from "@simspace/collections/Vector"
import { pipe } from "fp-ts/function"

import * as Fi from "../src/Fiber"
import * as S from "../src/Semaphore"
import * as XIO from "../src/XIO"

describe("Semaphore", () => {
  test.concurrent("`acquire` permits sequentially", async () => {
    const n = 20
    const effect = XIO.gen(function* (_) {
      const semaphore = yield* _(S.make(n))
      return yield* _(
        XIO.foreach(V.range(0, n), () => S.withPermit(semaphore)(S.available(semaphore))),
      )
    })

    const result = await XIO.unsafeRunPromise(effect)
    expect(
      pipe(
        result,
        V.every((n) => n === 19),
      ),
    ).toEqual(true)
  })

  test.concurrent("`acquire` permits concurrently", async () => {
    const n = 20
    const effect = XIO.gen(function* (_) {
      const semaphore = yield* _(S.make(n))
      return yield* _(
        XIO.foreachPar(V.range(0, n), () => S.withPermit(semaphore)(S.available(semaphore))),
      )
    })

    const result = await XIO.unsafeRunPromise(effect)
    expect(
      pipe(
        result,
        V.every((n) => n < 20),
      ),
    ).toEqual(true)
  })

  test.concurrent("Semaphores and fibers", async () => {
    const n = 1
    const effect = XIO.gen(function* (_) {
      const semaphore = yield* _(S.make(n))
      yield* _(pipe(S.withPermit(semaphore)(XIO.unit), XIO.fork))
      yield* _(S.withPermit(semaphore)(XIO.unit))
    })
    await pipe(effect, XIO.unsafeRunPromise)
    // Assert that this test completes
    expect(1).toEqual(1)
  })

  test.concurrent("`withPermit` does not leak permits on failure", async () => {
    const n = 1
    const effect = XIO.gen(function* (_) {
      const s = yield* _(S.make(n))
      yield* _(pipe(S.withPermit(s)(XIO.fail("fail")), XIO.ignore))
      return yield* _(S.available(s))
    })

    const result = await XIO.unsafeRunPromise(effect)
    expect(result).toEqual(1)
  })

  test.concurrent("`withPermit` does not leak fibers or permits upon cancellation", async () => {
    const n = 1
    const effect = XIO.gen(function* (_) {
      const s = yield* _(S.make(n))
      const fiber = yield* _(pipe(S.withPermit(s)(XIO.never), XIO.fork))
      yield* _(Fi.interrupt(fiber))
      return yield* _(S.available(s))
    })

    const result = await XIO.unsafeRunPromise(effect)
    expect(result).toEqual(1)
  })
})
