import { pipe } from "fp-ts/function"
import * as O from "fp-ts/Option"

import * as C from "../src/Cause"
import * as Ex from "../src/Exit"
import * as Fi from "../src/Fiber"
import * as F from "../src/Future"
import { assert } from "../src/internal/util"
import * as Ref from "../src/Ref"
import * as XIO from "../src/XIO"
import { test } from "./utils"

const [initial, update, failure] = ["value", "new value", "error"]

type State = "Active" | "Changed" | "Closed"

describe("Ref.Synchronized", () => {
  test("get", () =>
    pipe(
      Ref.makeSynchronized(initial),
      XIO.chain(Ref.get),
      XIO.chain((value) =>
        XIO.fromIO(() => {
          expect(value).toEqual(initial)
        }),
      ),
    ))

  test("getAndUpdate", () =>
    XIO.gen(function* (_) {
      const ref = yield* _(Ref.makeSynchronized(initial))
      const value1 = yield* _(
        pipe(
          ref,
          Ref.getAndUpdateE(() => XIO.succeed(update)),
        ),
      )
      const value2 = yield* _(Ref.get(ref))
      expect(value1).toEqual(initial)
      expect(value2).toEqual(update)
    }))

  test("getAndUpdate failure", () =>
    XIO.gen(function* (_) {
      const ref = yield* _(Ref.makeSynchronized(initial))
      const value = yield* _(
        pipe(
          ref,
          Ref.getAndUpdateE((_: string) => XIO.fail(failure)),
          XIO.result,
        ),
      )
      assert(Ex.isFailure(value))
      expect(pipe(value.cause, C.contains(C.fail(failure)))).toEqual(true)
    }))

  test("getAndUpdateSome", () =>
    XIO.gen(function* (_) {
      const ref = yield* _(Ref.makeSynchronized<State>("Active"))
      const value1 = yield* _(
        pipe(
          ref,
          Ref.getAndUpdateSomeE((s) =>
            s === "Closed" ? O.some(XIO.succeed("Active" as State)) : O.none,
          ),
        ),
      )
      const value2 = yield* _(Ref.get(ref))
      expect(value1).toEqual("Active")
      expect(value2).toEqual("Active")
    }))

  test("getAndUpdateSome twice", () =>
    XIO.gen(function* (_) {
      const ref = yield* _(Ref.makeSynchronized<State>("Active"))
      const value1 = yield* _(
        pipe(
          ref,
          Ref.getAndUpdateSomeE((s) =>
            s === "Active" ? O.some(XIO.succeed("Changed" as State)) : O.none,
          ),
        ),
      )
      const value2 = yield* _(
        pipe(
          ref,
          Ref.getAndUpdateSomeE((s) =>
            s === "Changed" ? O.some(XIO.succeed("Closed" as State)) : O.none,
          ),
        ),
      )
      const value3 = yield* _(Ref.get(ref))
      expect(value1).toEqual("Active")
      expect(value2).toEqual("Changed")
      expect(value3).toEqual("Closed")
    }))

  test("getAndUpdateSome with failure", () =>
    XIO.gen(function* (_) {
      const ref = yield* _(Ref.makeSynchronized<State>("Active"))
      const value = yield* _(
        pipe(
          ref,
          Ref.getAndUpdateSomeE((s) => (s === "Active" ? O.some(XIO.fail(failure)) : O.none)),
          XIO.result,
        ),
      )
      assert(Ex.isFailure(value))
      expect(pipe(value.cause, C.contains(C.fail(failure)))).toEqual(true)
    }))

  test("interrupt parent fiber and update", () =>
    XIO.gen(function* (_) {
      const future = yield* _(F.make<never, Ref.Synchronized<State>>())
      const latch = yield* _(F.make<never, void>())
      const makeAndWait = pipe(
        future,
        F.complete(Ref.makeSynchronized<State>("Active")),
        XIO.chain(() => F.await(latch)),
      )
      const fiber = yield* _(XIO.fork(makeAndWait))
      const ref = yield* _(F.await(future))
      yield* _(Fi.interrupt(fiber))
      const value = yield* _(
        pipe(
          ref,
          Ref.updateAndGetE(() => XIO.succeed("Closed")),
        ),
      )
      expect(value).toEqual("Closed")
    }))

  test("modify", () =>
    pipe(
      XIO.Do,
      XIO.bind("ref", () => Ref.makeSynchronized(initial)),
      XIO.bind("r", ({ ref }) =>
        pipe(
          ref,
          Ref.modifyE(() => XIO.succeed(["hello", update])),
        ),
      ),
      XIO.bind("value", ({ ref }) => pipe(ref, Ref.get)),
      XIO.chain(({ r, value }) =>
        XIO.fromIO(() => {
          expect(r).toEqual("hello")
          expect(value).toEqual(update)
        }),
      ),
    ))

  test("modify with failure", () =>
    pipe(
      XIO.Do,
      XIO.bind("ref", () => Ref.makeSynchronized(initial)),
      XIO.bind("r", ({ ref }) =>
        pipe(
          ref,
          Ref.modifyE(() => XIO.fail(failure)),
          XIO.result,
        ),
      ),
      XIO.chain(({ r }) =>
        XIO.fromIO(() => {
          assert(Ex.isFailure(r))
          expect(pipe(r.cause, C.contains(C.fail(failure)))).toEqual(true)
        }),
      ),
    ))

  test("modifySome", () =>
    pipe(
      XIO.Do,
      XIO.bind("ref", () => Ref.makeSynchronized(0)),
      XIO.bind("r", ({ ref }) =>
        pipe(
          ref,
          Ref.modifySomeE("no change", (n) =>
            n === 1 ? O.some(XIO.succeed(["change", 2])) : O.none,
          ),
        ),
      ),
      XIO.bind("value", ({ ref }) => Ref.get(ref)),
      XIO.bind("r1", ({ ref }) =>
        pipe(
          ref,
          Ref.modifySomeE("no change", (n) =>
            n === 0 ? O.some(XIO.succeed(["change", 3])) : O.none,
          ),
        ),
      ),
      XIO.bind("value1", ({ ref }) => Ref.get(ref)),
      XIO.chain(({ r, value, r1, value1 }) =>
        XIO.fromIO(() => {
          expect(r).toEqual("no change")
          expect(value).toEqual(0)
          expect(r1).toEqual("change")
          expect(value1).toEqual(3)
        }),
      ),
    ))

  test("modifySome with failure not triggered", () =>
    pipe(
      XIO.Do,
      XIO.bind("ref", () => Ref.makeSynchronized(0)),
      XIO.bind("r", ({ ref }) =>
        pipe(
          ref,
          Ref.modifySomeE("no change", (n) => (n === 1 ? O.some(XIO.fail(failure)) : O.none)),
        ),
      ),
      XIO.chain(({ r }) =>
        XIO.fromIO(() => {
          expect(r).toEqual("no change")
        }),
      ),
    ))

  test("modifySome with failure", () =>
    pipe(
      XIO.Do,
      XIO.bind("ref", () => Ref.makeSynchronized(0)),
      XIO.bind("r", ({ ref }) =>
        pipe(
          ref,
          Ref.modifySomeE("no change", (n) => (n === 0 ? O.some(XIO.fail(failure)) : O.none)),
          XIO.result,
        ),
      ),
      XIO.chain(({ r }) =>
        XIO.fromIO(() => {
          assert(Ex.isFailure(r))
          expect(pipe(r.cause, C.contains(C.fail(failure)))).toEqual(true)
        }),
      ),
    ))

  test("set", () =>
    pipe(
      XIO.Do,
      XIO.bind("ref", () => Ref.makeSynchronized(initial)),
      XIO.chainFirst(({ ref }) => pipe(ref, Ref.set(update))),
      XIO.bind("value", ({ ref }) => Ref.get(ref)),
      XIO.chain(({ value }) =>
        XIO.fromIO(() => {
          expect(value).toEqual(update)
        }),
      ),
    ))

  test("updateSomeAndGet", () =>
    pipe(
      XIO.Do,
      XIO.bind("ref", () => Ref.makeSynchronized(0)),
      XIO.bind("r", ({ ref }) =>
        pipe(
          ref,
          Ref.updateSomeAndGetE((n) => (n === 1 ? O.some(XIO.succeed(2)) : O.none)),
        ),
      ),
      XIO.bind("r1", ({ ref }) =>
        pipe(
          ref,
          Ref.updateSomeAndGetE((n) => (n === 0 ? O.some(XIO.succeed(3)) : O.none)),
        ),
      ),
      XIO.chain(({ r, r1 }) =>
        XIO.fromIO(() => {
          expect(r).toEqual(0)
          expect(r1).toEqual(3)
        }),
      ),
    ))

  test("updateSomeAndGet with failure", () =>
    pipe(
      XIO.Do,
      XIO.bind("ref", () => Ref.makeSynchronized(0)),
      XIO.bind("r", ({ ref }) =>
        pipe(
          ref,
          Ref.updateSomeAndGetE((n) => (n === 0 ? O.some(XIO.fail(failure)) : O.none)),
          XIO.result,
        ),
      ),
      XIO.chain(({ r }) =>
        XIO.fromIO(() => {
          assert(Ex.isFailure(r))
          expect(pipe(r.cause, C.contains(C.fail(failure)))).toEqual(true)
        }),
      ),
    ))
})
