import { identity, increment, pipe } from "fp-ts/function"

import * as Fi from "../src/Fiber"
import * as FR from "../src/FiberRef"
import * as F from "../src/Future"
import * as XIO from "../src/XIO"

const [initial, update, update1, update2] = ["initial", "update", "update1", "update2"]

const looseTimeAndCpu = pipe(
  XIO.sleep(0),
  XIO.chain(() => XIO.yieldNow),
  XIO.repeatN(100),
)

describe("FiberRef", () => {
  describe("Create a new FiberRef and check if:", () => {
    test.concurrent("`remove` restores the original value", async () => {
      const effect = XIO.gen(function* (_) {
        const fiberRef = yield* _(FR.make(initial))
        yield* _(pipe(fiberRef, FR.set(update)))
        yield* _(FR.remove(fiberRef))
        return yield* _(FR.get(fiberRef))
      })

      const result = await pipe(effect, XIO.scoped, XIO.unsafeRunPromise)
      expect(result).toEqual(initial)
    })

    test.concurrent("`get` returns the current value", async () => {
      const effect = XIO.gen(function* (_) {
        const fiberRef = yield* _(FR.make(initial))
        return yield* _(FR.get(fiberRef))
      })

      const result = await pipe(effect, XIO.scoped, XIO.unsafeRunPromise)
      expect(result).toEqual(initial)
    })

    test.concurrent("`get` returns the current value for a child", async () => {
      const effect = XIO.gen(function* (_) {
        const fiberRef = yield* _(FR.make(initial))
        const child = yield* _(pipe(FR.get(fiberRef), XIO.fork))
        return yield* _(Fi.join(child))
      })

      const result = await pipe(effect, XIO.scoped, XIO.unsafeRunPromise)
      expect(result).toEqual(initial)
    })

    test.concurrent("`locally` restores original value", async () => {
      const effect = XIO.gen(function* (_) {
        const fiberRef = yield* _(FR.make(initial))
        const local = yield* _(pipe(FR.get(fiberRef), FR.locally(fiberRef, update)))
        const value = yield* _(FR.get(fiberRef))
        return { local, value }
      })

      const { local, value } = await pipe(effect, XIO.scoped, XIO.unsafeRunPromise)
      expect(local).toEqual(update)
      expect(value).toEqual(initial)
    })

    test.concurrent("`locally` restores parent's value", async () => {
      const effect = XIO.gen(function* (_) {
        const fiberRef = yield* _(FR.make(initial))
        const child = yield* _(pipe(FR.get(fiberRef), FR.locally(fiberRef, update), XIO.fork))
        const local = yield* _(Fi.join(child))
        const value = yield* _(FR.get(fiberRef))
        return { local, value }
      })

      const { local, value } = await pipe(effect, XIO.scoped, XIO.unsafeRunPromise)
      expect(local).toEqual(update)
      expect(value).toEqual(initial)
    })

    test.concurrent("`locally` restores undefined value", async () => {
      const effect = XIO.gen(function* (_) {
        const child = yield* _(pipe(FR.make(initial), XIO.fork))
        const fiberRef = yield* _(pipe(child, Fi.await, XIO.chain(XIO.done)))
        const localValue = yield* _(pipe(FR.get(fiberRef), FR.locally(fiberRef, update)))
        const value = yield* _(FR.get(fiberRef))
        return { localValue, value }
      })

      const { localValue, value } = await pipe(effect, XIO.scoped, XIO.unsafeRunPromise)
      expect(localValue).toEqual(update)
      expect(value).toEqual(initial)
    })

    test.concurrent("`modify` changes value", async () => {
      const effect = XIO.gen(function* (_) {
        const fiberRef = yield* _(FR.make(initial))
        const value1 = yield* _(
          pipe(
            fiberRef,
            FR.modify(() => [1, update]),
          ),
        )
        const value2 = yield* _(FR.get(fiberRef))
        return { value1, value2 }
      })

      const { value1, value2 } = await pipe(effect, XIO.scoped, XIO.unsafeRunPromise)
      expect(value1).toEqual(1)
      expect(value2).toEqual(update)
    })

    test.concurrent("`set` updates the current value", async () => {
      const effect = XIO.gen(function* (_) {
        const fiberRef = yield* _(FR.make(initial))
        yield* _(pipe(fiberRef, FR.set(update)))
        return yield* _(FR.get(fiberRef))
      })

      const result = await pipe(effect, XIO.scoped, XIO.unsafeRunPromise)
      expect(result).toEqual(update)
    })

    test.concurrent("`set` by a child does not update parent value", async () => {
      const effect = XIO.gen(function* (_) {
        const fiberRef = yield* _(FR.make(initial))
        const future = yield* _(F.make<never, void>())
        yield* _(
          pipe(
            fiberRef,
            FR.set(update),
            XIO.chain(() => pipe(future, F.succeed<void>(undefined))),
            XIO.fork,
          ),
        )
        yield* _(F.await(future))
        return yield* _(FR.get(fiberRef))
      })

      const result = await pipe(effect, XIO.scoped, XIO.unsafeRunPromise)
      expect(result).toEqual(initial)
    })

    test.concurrent("value is inherited by join", async () => {
      const effect = XIO.gen(function* (_) {
        const fiberRef = yield* _(FR.make(initial))
        const child = yield* _(pipe(fiberRef, FR.set(update), XIO.fork))
        yield* _(Fi.join(child))
        return yield* _(FR.get(fiberRef))
      })

      const result = await pipe(effect, XIO.scoped, XIO.unsafeRunPromise)
      expect(result).toEqual(update)
    })

    test.concurrent("value is inherited after a simple race", async () => {
      const effect = XIO.gen(function* (_) {
        const fiberRef = yield* _(FR.make(initial))
        yield* _(pipe(fiberRef, FR.set(update1), XIO.race(pipe(fiberRef, FR.set(update2)))))
        return yield* _(FR.get(fiberRef))
      })

      const result = await pipe(effect, XIO.scoped, XIO.unsafeRunPromise)
      expect(result === update1 || result === update2).toEqual(true)
    })

    test.concurrent("value is inherited after a race with a bad winner", async () => {
      const effect = XIO.gen(function* (_) {
        const fiberRef = yield* _(FR.make(initial))
        const badWinner = pipe(
          fiberRef,
          FR.set(update1),
          XIO.chain(() => XIO.fail("oops")),
        )
        const goodLoser = pipe(
          fiberRef,
          FR.set(update2),
          XIO.chain(() => looseTimeAndCpu),
        )
        yield* _(pipe(badWinner, XIO.race(goodLoser)))
        return yield* _(FR.get(fiberRef))
      })

      const result = await pipe(effect, XIO.scoped, XIO.unsafeRunPromise)
      expect(result).toEqual(update2)
    })

    test.concurrent("value is not inherited after a race of losers", async () => {
      const effect = XIO.gen(function* (_) {
        const fiberRef = yield* _(FR.make(initial))
        const loser1 = pipe(
          fiberRef,
          FR.set(update1),
          XIO.chain(() => XIO.fail("oops1")),
        )
        const loser2 = pipe(
          fiberRef,
          FR.set(update2),
          XIO.chain(() => XIO.fail("oops2")),
        )
        yield* _(pipe(loser1, XIO.race(loser2), XIO.ignore))
        return yield* _(FR.get(fiberRef))
      })

      const result = await pipe(effect, XIO.scoped, XIO.unsafeRunPromise)
      expect(result).toEqual(initial)
    })

    test.concurrent("fork function is applied on fork - 1", async () => {
      const effect = XIO.gen(function* (_) {
        const fiberRef = yield* _(FR.make(0, increment))
        const child = yield* _(pipe(XIO.unit, XIO.fork))
        yield* _(pipe(child, Fi.join))
        return yield* _(FR.get(fiberRef))
      })

      const result = await pipe(effect, XIO.scoped, XIO.unsafeRunPromise)
      expect(result).toEqual(1)
    })

    test.concurrent("fork function is applied on fork - 2", async () => {
      const effect = XIO.gen(function* (_) {
        const fiberRef = yield* _(FR.make(0, increment))
        const child = yield* _(pipe(XIO.unit, XIO.fork, XIO.chain(Fi.join), XIO.fork))
        yield* _(Fi.join(child))
        return yield* _(FR.get(fiberRef))
      })

      const result = await pipe(effect, XIO.scoped, XIO.unsafeRunPromise)
      expect(result).toEqual(2)
    })

    test.concurrent("join function is applied on fork - 1", async () => {
      const effect = XIO.gen(function* (_) {
        const fiberRef = yield* _(FR.make(0, identity, Math.max))
        const child = yield* _(pipe(fiberRef, FR.update(increment), XIO.fork))
        yield* _(pipe(child, Fi.join))
        return yield* _(FR.get(fiberRef))
      })

      const result = await pipe(effect, XIO.scoped, XIO.unsafeRunPromise)
      expect(result).toEqual(1)
    })

    test.concurrent("join function is applied on fork - 2", async () => {
      const effect = XIO.gen(function* (_) {
        const fiberRef = yield* _(FR.make(0, identity, Math.max))
        const child = yield* _(pipe(fiberRef, FR.update(increment), XIO.fork))
        yield* _(
          pipe(
            fiberRef,
            FR.update((n) => n + 2),
          ),
        )
        yield* _(Fi.join(child))
        return yield* _(FR.get(fiberRef))
      })

      const result = await pipe(effect, XIO.scoped, XIO.unsafeRunPromise)
      expect(result).toEqual(2)
    })
  })
})
