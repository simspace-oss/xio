import type { Has } from "../src/Tag"

import * as HS from "@simspace/collections/HashSet"
import * as V from "@simspace/collections/Vector"
import * as E from "fp-ts/Either"
import { identity, pipe } from "fp-ts/function"
import * as O from "fp-ts/Option"
import * as RNEA from "fp-ts/ReadonlyNonEmptyArray"

import * as C from "../src/Cause"
import * as Ex from "../src/Exit"
import * as Fi from "../src/Fiber"
import * as F from "../src/Future"
import { assert } from "../src/internal/util"
import * as Ref from "../src/Ref"
import { tag } from "../src/Tag"
import * as XIO from "../src/XIO"
import * as _ from "./utils"

describe("XIO", () => {
  describe("bracket", () => {
    test.concurrent("happy path", async () => {
      const effect = XIO.gen(function* (_) {
        const release = yield* _(Ref.make(false))
        const result = yield* _(
          XIO.bracket(
            XIO.succeed(42),
            () => XIO.succeed(0),
            () => pipe(release, Ref.set(true)),
          ),
        )
        const released = yield* _(Ref.get(release))
        return { released, result }
      })

      const { released, result } = await XIO.unsafeRunPromise(effect)
      expect(released).toEqual(true)
      expect(result).toEqual(0)
    })

    test.concurrent("error handling", async () => {
      const releaseHalted = new Error("release halted")
      const effect = pipe(
        XIO.bracket(
          XIO.succeed(42),
          () => XIO.fail("use failed"),
          () => XIO.halt(releaseHalted),
        ),
        XIO.disconnect,
        XIO.result,
      )

      const exit = await XIO.unsafeRunPromise(effect)
      expect(
        pipe(
          exit,
          Ex.match(C.failures, () => V.empty()),
        ),
      ).toEqual(V.vector("use failed"))
      expect(
        pipe(
          exit,
          Ex.match(C.defects, () => V.empty),
        ),
      ).toEqual(V.vector(releaseHalted))
    })

    test.concurrent('"beast mode" error handling', async () => {
      const releaseHalted = new Error("release halted")
      const effect = XIO.gen(function* (_) {
        const released = yield* _(Ref.make(false))
        const exit = yield* _(
          pipe(
            XIO.bracket(
              XIO.succeed(42),
              () => {
                throw releaseHalted
              },
              () => pipe(released, Ref.set(true)),
            ),
            XIO.disconnect,
            XIO.result,
          ),
        )
        const isReleased = yield* _(Ref.get(released))
        return { isReleased, exit }
      })

      const { exit, isReleased } = await XIO.unsafeRunPromise(effect)
      expect(isReleased).toEqual(true)
      expect(
        pipe(
          exit,
          Ex.match(C.defects, () => V.empty()),
        ),
      ).toEqual(V.vector(releaseHalted))
    })
  })
  describe("foreach", () => {
    test.concurrent("returns the list of results", async () => {
      const list = RNEA.range(1, 100)
      const result = await pipe(XIO.foreach(list, XIO.succeed), XIO.unsafeRunPromise)
      expect(result).toEqual(V.from(list))
    })

    test.concurrent(
      "both evaluates effects and returns the list of results in the same order",
      async () => {
        const list = ["1", "2", "3"]
        const effect = XIO.gen(function* (_) {
          const ref = yield* _(Ref.make<ReadonlyArray<string>>([]))
          const result = yield* _(
            XIO.foreach(list, (s) =>
              pipe(
                ref,
                Ref.update((xs) => [...xs, s]),
                XIO.map(() => s),
              ),
            ),
          )
          const refResult = yield* _(Ref.get(ref))
          return { result, refResult }
        })
        const { result, refResult } = await XIO.unsafeRunPromise(effect)
        expect(result).toEqual(V.from(list))
        expect(refResult).toEqual(list)
      },
    )

    test.concurrent("fails if one of the effects fails", async () => {
      const list = ["1", "h", "3"]
      const parseOrFail = (s: string): XIO.XIO<unknown, string, number> =>
        XIO.defer(() => {
          const n = parseInt(s, 10)
          if (isNaN(n)) {
            return XIO.fail("not a number")
          }
          return XIO.succeed(n)
        })
      const res = await pipe(XIO.foreach(list, parseOrFail), XIO.swap, XIO.unsafeRunPromise)
      expect(res).toEqual("not a number")
    })
  })

  describe("foreachDiscard", () => {
    test.concurrent("runs effects in order", async () => {
      const as = [1, 2, 3, 4, 5]
      const effect = XIO.gen(function* (_) {
        const ref = yield* _(Ref.make<ReadonlyArray<number>>([]))
        yield* _(
          XIO.foreachDiscard(as, (n) =>
            pipe(
              ref,
              Ref.update((xs) => [...xs, n]),
            ),
          ),
        )
        return yield* _(Ref.get(ref))
      })
      const result = await XIO.unsafeRunPromise(effect)
      expect(result).toEqual(as)
    })
  })

  describe("foreachPar", () => {
    test.concurrent("runs a single task", async () => {
      const as = [2]
      const effect = XIO.foreachPar(as, (a) => XIO.succeed(a * 2))
      const result = await XIO.unsafeRunPromise(effect)
      expect(result).toEqual(V.vector(4))
    })

    test.concurrent("runs two tasks", async () => {
      const as = [2, 3]
      const effect = XIO.foreachPar(as, (a) => XIO.succeed(a * 2))
      const result = await XIO.unsafeRunPromise(effect)
      expect(result).toEqual(V.vector(4, 6))
    })

    test.concurrent("runs many tasks", async () => {
      const as = RNEA.range(1, 1000)
      const effect = XIO.foreachPar(as, (a) => XIO.succeed(a * 2))
      const result = await XIO.unsafeRunPromise(effect)
      expect(result).toEqual(V.from(as.map((n) => n * 2)))
    })

    test.concurrent("runs a task that fails", async () => {
      const as = RNEA.range(1, 10)
      const effect = pipe(
        XIO.foreachPar(as, (a) => (a === 5 ? XIO.fail("Boom!") : XIO.succeed(a * 2))),
        XIO.swap,
      )
      const result = await XIO.unsafeRunPromise(effect)
      expect(result).toEqual("Boom!")
    })

    test.concurrent("runs two failed tasks", async () => {
      const as = RNEA.range(1, 10)
      const effect = pipe(
        XIO.foreachPar(as, (a) =>
          a === 5 ? XIO.fail("Boom1!") : a === 8 ? XIO.fail("Boom2!") : XIO.succeed(a * 2),
        ),
        XIO.swap,
      )
      const result = await XIO.unsafeRunPromise(effect)
      expect(result).toContain("Boom")
    })

    test.concurrent("runs a task that halts", async () => {
      const as = RNEA.range(1, 10)
      const error = new Error("Boom!")
      const effect = pipe(
        XIO.foreachPar(as, (a) => (a === 5 ? XIO.halt(error) : XIO.succeed(a * 2))),
        XIO.result,
      )
      const result = await XIO.unsafeRunPromise(effect)
      expect(
        pipe(
          result,
          Ex.match(C.defects, () => V.empty()),
        ),
      ).toEqual(V.vector(error))
    })

    test.concurrent("runs a task that is interrupted", async () => {
      const as = RNEA.range(1, 10)
      const effect = pipe(
        XIO.foreachPar(as, (a) => (a === 5 ? XIO.interrupt : XIO.succeed(a * 2))),
        XIO.result,
      )
      const result = await XIO.unsafeRunPromise(effect)
      expect(
        pipe(
          result,
          Ex.match(C.isInterrupted, () => false),
        ),
      ).toEqual(true)
    })

    test.concurrent("returns results in the same order", async () => {
      const as = ["1", "2", "3"]
      const effect = XIO.foreachPar(as, (a) => XIO.fromIO(() => parseInt(a, 10)))
      const result = await XIO.unsafeRunPromise(effect)
      expect(result).toEqual(V.vector(1, 2, 3))
    })

    test.concurrent("runs effects concurrently", async () => {
      const effect = pipe(
        XIO.Do,
        XIO.bind("f", () => F.make<never, void>()),
        XIO.bind("fiber", ({ f }) =>
          pipe(
            XIO.foreachPar([XIO.never, pipe(f, F.succeed<void>(undefined))], (a) => a),
            XIO.fork,
          ),
        ),
        XIO.chainFirst(({ f }) => F.await(f)),
        XIO.chain(({ fiber }) => Fi.interrupt(fiber)),
        XIO.map(() => true),
      )
      const result = await XIO.unsafeRunPromise(effect)
      expect(result).toEqual(true)
    })

    test.concurrent("propagates error", async () => {
      const ints = [1, 2, 3, 4, 5, 6]
      const odds = XIO.foreachPar(ints, (n) => (n % 2 !== 0 ? XIO.succeed(n) : XIO.fail("not odd")))
      const result = await pipe(odds, XIO.swap, XIO.unsafeRunPromise)
      expect(result).toEqual("not odd")
    })

    test.concurrent("interrupts effects on first failure", async () => {
      const effect = XIO.gen(function* (_) {
        const ref = yield* _(Ref.make(false))
        const future = yield* _(F.make<never, void>())
        const actions = [
          XIO.never,
          XIO.succeed(1),
          XIO.fail("C"),
          pipe(
            F.await(future),
            XIO.chain(() => pipe(ref, Ref.set(true))),
          ),
        ]
        const e = yield* _(
          pipe(
            XIO.foreachPar(actions, (a) => a),
            XIO.swap,
          ),
        )
        const v = yield* _(Ref.get(ref))
        return { e, v }
      })
      const { e, v } = await XIO.unsafeRunPromise(effect)
      expect(e).toEqual("C")
      expect(v).toEqual(false)
    })

    test.concurrent("does not kill fiber when forked on the parent scope", async () => {
      const effect = XIO.gen(function* (_) {
        const ref = yield* _(Ref.make(0))
        const fibers = yield* _(
          XIO.foreachPar(RNEA.range(1, 100), () =>
            pipe(
              ref,
              Ref.update((n) => n + 1),
              XIO.fork,
            ),
          ),
        )
        yield* _(XIO.foreach(fibers, Fi.await))
        return yield* _(Ref.get(ref))
      })
      const result = await XIO.unsafeRunPromise(effect)
      expect(result).toEqual(100)
    })
  })

  describe("foreachC (bounded concurrency)", () => {
    test.concurrent("returns the list of results in the appropriate order", async () => {
      const list = [1, 2, 3]
      const res = await pipe(
        XIO.foreachPar(list, (x) => XIO.fromIO(() => x.toString())),
        XIO.withConcurrency(2),
        XIO.unsafeRunPromise,
      )
      expect(res).toEqual(V.vector("1", "2", "3"))
    })

    test.concurrent("works on large lists", async () => {
      const list = RNEA.range(1, 100000)
      const result = await pipe(
        XIO.foreachPar(list, XIO.succeed),
        XIO.withConcurrency(10),
        XIO.unsafeRunPromise,
      )
      expect(result).toEqual(V.from(list))
    })

    test.concurrent("runs effects concurrently", async () => {
      const effect = XIO.gen(function* (_) {
        const f = yield* _(F.make<never, void>())
        yield* _(
          pipe(
            XIO.foreachPar([XIO.never, pipe(f, F.succeed<void>(undefined))], identity),
            XIO.withConcurrency(2),
            XIO.fork,
          ),
        )
        yield* _(F.await(f))
        return true
      })
      const result = await XIO.unsafeRunPromise(effect)
      expect(result).toEqual(true)
    })

    test.concurrent("propagates error", async () => {
      const ints = [1, 2, 3, 4, 5, 6]
      const odds = pipe(
        XIO.foreachPar(ints, (n) => (n % 2 !== 0 ? XIO.succeed(n) : XIO.fail("not odd"))),
        XIO.withConcurrency(4),
      )
      const result = await pipe(odds, XIO.swap, XIO.unsafeRunPromise)
      expect(result).toEqual("not odd")
    })

    test.concurrent("interrupts effects on first failure", async () => {
      const effect = XIO.gen(function* (_) {
        const actions = [XIO.never, XIO.succeed(1), XIO.fail("C")]
        return yield* _(
          pipe(
            XIO.foreachPar(actions, (a) => a),
            XIO.withConcurrency(4),
            XIO.swap,
          ),
        )
      })
      const result = await XIO.unsafeRunPromise(effect)
      expect(result).toEqual("C")
    })
  })

  describe("foreachDiscardC (bounded concurrency)", () => {
    test.concurrent("runs all effects", async () => {
      const as = [1, 2, 3, 4, 5]
      const effect = XIO.gen(function* (_) {
        const ref = yield* _(Ref.make<ReadonlyArray<number>>([]))
        yield* _(
          pipe(
            XIO.foreachDiscardPar(as, (a) =>
              pipe(
                ref,
                Ref.update((ns) => [...ns, a]),
              ),
            ),
            XIO.withConcurrency(2),
          ),
        )
        return yield* _(Ref.get(ref))
      })
      const result = await XIO.unsafeRunPromise(effect)
      expect(result.length).toEqual(as.length)
      expect([...result].sort()).toEqual(as)
    })
  })

  describe("fork", () => {
    test.concurrent("propagates interruption", async () => {
      const result = await pipe(
        XIO.never,
        XIO.fork,
        XIO.chain(Fi.interrupt),
        XIO.chain(XIO.done),
        XIO.unsafeRunPromiseExit,
      )
      expect(Ex.isInterrupted(result)).toEqual(true)
    })
  })

  describe("onExit", () => {
    test.concurrent("ensures that a cleanup function runs when the effect succeeds", async () => {
      const effect = XIO.gen(function* (_) {
        const ref = yield* _(Ref.make(false))
        yield* _(
          pipe(
            XIO.unit,
            XIO.onExit(
              Ex.match(
                () => XIO.unit,
                () => pipe(ref, Ref.set(true)),
              ),
            ),
          ),
        )
        return yield* _(Ref.get(ref))
      })
      const result = await XIO.unsafeRunPromise(effect)
      expect(result).toEqual(true)
    })
  })

  describe("asynchronous correctness", () => {
    test.concurrent("simple async must return", async () => {
      const result = await pipe(
        XIO.async<unknown, never, number>((k) => k(XIO.succeed(42))),
        XIO.unsafeRunPromise,
      )
      expect(result).toEqual(42)
    })
    test.concurrent("async should not resume fiber twice after interruption", async () => {
      const effect = XIO.gen(function* (_) {
        const step = yield* _(F.make<never, void>())
        const unexpectedPlace = yield* _(Ref.make<ReadonlyArray<number>>([]))
        const fork = yield* _(
          pipe(
            XIO.async<unknown, never, void>((k) => {
              pipe(
                F.await(step),
                XIO.chain(() =>
                  XIO.fromIO(() => {
                    k(
                      pipe(
                        unexpectedPlace,
                        Ref.update((xs) => [1, ...xs]),
                      ),
                    )
                  }),
                ),
                XIO.unsafeRun,
              )
            }),
            XIO.ensuring(
              XIO.async<unknown, never, void>(() => {
                pipe(step, F.succeed<void>(undefined), XIO.unsafeRun)
                // never complete
              }),
            ),
            XIO.ensuring(
              pipe(
                unexpectedPlace,
                Ref.update((xs) => [2, ...xs]),
              ),
            ),
            XIO.forkDaemon,
          ),
        )
        const result = yield* _(pipe(fork, Fi.interrupt, XIO.timeout(1000)))
        const unexpected = yield* _(Ref.get(unexpectedPlace))
        return { result, unexpected }
      })
      const { result, unexpected } = await XIO.unsafeRunPromise(effect)
      expect(unexpected.length).toEqual(0)
      expect(result).toEqual(O.none)
    })
    test.concurrent(
      "asyncOption should not resume fiber twice after synchronous result",
      async () => {
        const effect = XIO.gen(function* (_) {
          const step = yield* _(F.make<never, void>())
          const unexpectedPlace = yield* _(Ref.make<ReadonlyArray<number>>([]))
          const fork = yield* _(
            pipe(
              XIO.asyncOption<unknown, never, void>((k) => {
                pipe(
                  F.await(step),
                  XIO.chain(() =>
                    XIO.fromIO(() =>
                      k(
                        pipe(
                          unexpectedPlace,
                          Ref.update((xs) => [1, ...xs]),
                        ),
                      ),
                    ),
                  ),
                  XIO.unsafeRun,
                )
                return O.some(XIO.unit)
              }),
              XIO.chain(() =>
                XIO.async<unknown, never, void>(() => {
                  pipe(step, F.succeed<void>(undefined), XIO.unsafeRun)
                  // never complete
                }),
              ),
              XIO.ensuring(
                pipe(
                  unexpectedPlace,
                  Ref.update((xs) => [2, ...xs]),
                ),
              ),
              XIO.uninterruptible,
              XIO.forkDaemon,
            ),
          )
          const result = yield* _(pipe(Fi.interrupt(fork), XIO.timeout(1000)))
          const unexpected = yield* _(Ref.get(unexpectedPlace))
          return { result, unexpected }
        })

        const { result, unexpected } = await XIO.unsafeRunPromise(effect)
        expect(unexpected.length).toEqual(0)
        expect(result).toEqual(O.none)
      },
    )
  })
  describe("concurrency correctness", () => {
    test.concurrent("shallow fork/join identity", async () => {
      const result = await pipe(XIO.succeed(42), XIO.fork, XIO.chain(Fi.join), XIO.unsafeRunPromise)
      expect(result).toEqual(42)
    })

    test.concurrent("asyncInterrupt runs cancel token on interrupt", async () => {
      const effect = XIO.gen(function* (_) {
        const inAsync = yield* _(F.make<never, void>())
        const release = yield* _(F.make<never, number>())
        const async = XIO.asyncInterrupt<unknown, never, never>(() => {
          pipe(inAsync, F.succeed<void>(undefined), XIO.unsafeRun)
          return E.left(pipe(release, F.succeed(42)))
        })
        const fiber = yield* _(XIO.fork(async))
        yield* _(F.await(inAsync))
        yield* _(Fi.interrupt(fiber))
        return yield* _(F.await(release))
      })

      const result = await XIO.unsafeRunPromise(effect)
      expect(result).toEqual(42)
    })

    test.concurrent("daemon fiber race interruption", async () => {
      const plus1 = (latch: F.Future<never, void>, finalizer: XIO.UIO<void>) =>
        pipe(
          latch,
          F.succeed<void>(undefined),
          XIO.chain(() => XIO.sleep(5000)),
          XIO.onInterrupt(finalizer),
        )

      const effect = XIO.gen(function* (_) {
        const interruptionRef = yield* _(Ref.make(0))
        const latch1Start = yield* _(F.make<never, void>())
        const latch2Start = yield* _(F.make<never, void>())
        const inc = pipe(
          interruptionRef,
          Ref.update((n) => n + 1),
        )
        const left = plus1(latch1Start, inc)
        const right = plus1(latch2Start, inc)
        const fiber = yield* _(pipe(left, XIO.race(right), XIO.fork))
        yield* _(
          pipe(
            F.await(latch1Start),
            XIO.chain(() => F.await(latch2Start)),
            XIO.chain(() => Fi.interrupt(fiber)),
          ),
        )
        return yield* _(Ref.get(interruptionRef))
      })

      const result = await XIO.unsafeRunPromise(effect)
      expect(result).toEqual(2)
    })

    test.concurrent("race in daemon is executed", async () => {
      const effect = XIO.gen(function* (_) {
        const latch1 = yield* _(F.make<never, void>())
        const latch2 = yield* _(F.make<never, void>())
        const f1 = yield* _(F.make<never, void>())
        const f2 = yield* _(F.make<never, void>())
        const loser1 = XIO.bracket(
          pipe(latch1, F.succeed<void>(undefined)),
          () => XIO.never,
          () => pipe(f1, F.succeed<void>(undefined)),
        )
        const loser2 = XIO.bracket(
          pipe(latch2, F.succeed<void>(undefined)),
          () => XIO.never,
          () => pipe(f2, F.succeed<void>(undefined)),
        )
        const fiber = yield* _(pipe(loser1, XIO.race(loser2), XIO.forkDaemon))
        yield* _(F.await(latch1))
        yield* _(F.await(latch2))
        yield* _(Fi.interrupt(fiber))
        const res1 = yield* _(F.await(f1))
        const res2 = yield* _(F.await(f2))
        return { res1, res2 }
      })

      const { res1, res2 } = await XIO.unsafeRunPromise(effect)
      expect(res1).toEqual(undefined)
      expect(res2).toEqual(undefined)
    })

    test.concurrent("race of fail with success", async () => {
      const result = await pipe(
        XIO.fail(42),
        XIO.race(XIO.succeed(24)),
        XIO.either,
        XIO.unsafeRunPromise,
      )
      expect(result).toEqual(E.right(24))
    })

    test.concurrent("race of halt with success", async () => {
      const result = await pipe(
        XIO.halt(new Error()),
        XIO.race(XIO.succeed(24)),
        XIO.unsafeRunPromise,
      )
      expect(result).toEqual(24)
    })

    test.concurrent("race of fail with fail", async () => {
      const result = await pipe(
        XIO.fail(42),
        XIO.race(XIO.fail(42)),
        XIO.either,
        XIO.unsafeRunPromise,
      )
      expect(result).toEqual(E.left(42))
    })

    test.concurrent("race of value and never", async () => {
      const result = await pipe(XIO.succeed(42), XIO.race(XIO.never), XIO.unsafeRunPromise)
      expect(result).toEqual(42)
    })

    //
    // TODO: This test passes, but it causes jest to never terminate,
    // since XIO.never never terminates unless interrupted, and this race
    // is in an uninterruptible region. Figure out how to test this case.
    //
    // test.concurrent("race in uninterruptible region", async () => {
    //   const result = await pipe(
    //     XIO.unit,
    //     XIO.race(XIO.never),
    //     XIO.uninterruptible,
    //     XIO.unsafeRunToPromise,
    //   )
    //   expect(result).toEqual(undefined)
    // })

    test.concurrent("race of two forks does not interrupt winner", async () => {
      const effect = XIO.gen(function* (_) {
        const ref = yield* _(Ref.make(0))
        const fibers = yield* _(Ref.make(HS.makeDefault<Fi.Fiber<unknown, unknown>>()))
        const latch = yield* _(F.make<never, void>())
        const effect = XIO.uninterruptibleMask((restore) =>
          pipe(
            F.await(latch),
            XIO.onInterrupt(
              pipe(
                ref,
                Ref.update((n) => n + 1),
              ),
            ),
            restore,
            XIO.fork,
            XIO.chainFirst((fiber: Fi.Fiber<unknown, unknown>) =>
              pipe(fibers, Ref.update(HS.add(fiber))),
            ),
          ),
        )
        const awaitAll = pipe(Ref.get(fibers), XIO.chain(Fi.awaitAll))
        yield* _(pipe(effect, XIO.race(effect)))
        return yield* _(
          pipe(
            latch,
            F.succeed<void>(undefined),
            XIO.chain(() => awaitAll),
            XIO.chain(() => Ref.get(ref)),
          ),
        )
      })

      const result = await XIO.unsafeRunPromise(effect)
      expect(result).toBeLessThanOrEqual(1)
    })

    test.concurrent("raceFirst interrupts loser on success", async () => {
      const effect = XIO.gen(function* (_) {
        const s = yield* _(F.make<never, void>())
        const effect = yield* _(F.make<never, number>())
        const winner = pipe(
          F.await(s),
          XIO.chain(() => XIO.unit),
        )
        const loser = XIO.bracket(
          pipe(s, F.succeed<void>(undefined)),
          () => XIO.never,
          () => pipe(effect, F.succeed(42)),
        )
        yield* _(pipe(winner, XIO.raceFirst(loser)))
        return yield* _(F.await(effect))
      })

      const result = await XIO.unsafeRunPromise(effect)
      expect(result).toEqual(42)
    })
  })

  describe("environment", () => {
    test.concurrent("provide is modular", async () => {
      const Tag = tag<number>("test_XIO_environment_provide_is_modular")

      const effect = XIO.gen(function* (_) {
        const v1 = yield* _(XIO.askService(Tag))
        const v2 = yield* _(pipe(XIO.askService(Tag), XIO.provideService(Tag, 2)))
        const v3 = yield* _(XIO.askService(Tag))
        return { v1, v2, v3 }
      })

      const { v1, v2, v3 } = await XIO.unsafeRunPromise(pipe(effect, XIO.provideService(Tag, 4)))
      expect(v1).toEqual(4)
      expect(v2).toEqual(2)
      expect(v3).toEqual(4)
    })

    test.concurrent("async can use environment", async () => {
      const Tag = tag<number>("test_XIO_environment_async_can_use_environment")

      const effect = XIO.async<Has<number>, never, number>((cb) => cb(XIO.askService(Tag)))

      const result = await pipe(effect, XIO.provideService(Tag, 10), XIO.unsafeRunPromise)
      expect(result).toEqual(10)
    })
  })

  describe("fromPromiseCatch", () => {
    const succeeding = () =>
      new Promise<number>((resolve) => {
        resolve(42)
      })

    const failing = () =>
      new Promise<number>((_, reject) => {
        reject()
      })

    _.test("succeeds if Promise resolves", () =>
      XIO.gen(function* (_) {
        const result = yield* _(XIO.fromPromiseCatch(succeeding, () => "failed"))
        expect(result).toEqual(42)
      }),
    )

    _.test("fails if Promise rejects", () =>
      XIO.gen(function* (_) {
        const result = yield* _(
          pipe(
            XIO.fromPromiseCatch(failing, () => "oops!"),
            XIO.swap,
          ),
        )
        expect(result).toEqual("oops!")
      }),
    )
  })

  describe("fromPromiseHalt", () => {
    const succeeding = () =>
      new Promise<number>((resolve) => {
        resolve(42)
      })

    const failing = () =>
      new Promise<number>((_, reject) => {
        reject("oops!")
      })

    _.test("succeeds if Promise resolves", () =>
      XIO.gen(function* (_) {
        const result = yield* _(XIO.fromPromiseHalt(succeeding))
        expect(result).toEqual(42)
      }),
    )

    _.test("halts if Promise rejects", () =>
      XIO.gen(function* (_) {
        const result = yield* _(pipe(XIO.fromPromiseHalt(failing), XIO.sandbox, XIO.swap))
        expect(pipe(result, C.contains(C.halt("oops!")))).toEqual(true)
      }),
    )
  })

  describe("unsafeRunSyncExit", () => {
    const syncEffect = pipe(
      XIO.succeed(1),
      XIO.chain((n) => XIO.succeed(n + 1)),
    )

    test.concurrent("returns Success<A> if the XIO was syncronous and succeeding", () => {
      const result = XIO.unsafeRunSyncExit(syncEffect)
      assert(Ex.isSuccess(result))
      expect(result.value).toEqual(2)
    })

    const asyncEffect = pipe(XIO.succeed(1), XIO.delay(0))

    test.concurrent(
      "returns Failure<Fail<FiberContext<E, A>>> if the XIO was asynchronous",
      async () => {
        const result = XIO.unsafeRunSyncExit(asyncEffect)
        assert(Ex.isFailure(result))
        const fiber = pipe(
          result.cause,
          C.findMap((cause) => {
            console.log(cause)
            if (C.isFailType(cause) && Fi.isFiberContext(cause.value)) {
              return O.some(cause.value)
            }
            return O.none
          }),
        )
        assert(O.isSome(fiber))
        const asyncResult = await XIO.unsafeRunPromise(fiber.value.await)
        assert(Ex.isSuccess(asyncResult))
        expect(asyncResult.value).toEqual(1)
      },
    )
  })
})
