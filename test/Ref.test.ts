import { flow, pipe } from "fp-ts/function"
import * as O from "fp-ts/Option"

import * as Ref from "../src/Ref"
import * as XIO from "../src/XIO"
import { test } from "./utils"

const [initial, update] = ["value", "new value"]

describe("Ref", () => {
  test("get", () =>
    pipe(
      Ref.make(initial),
      XIO.chain(Ref.get),
      XIO.map((s) => {
        expect(s).toEqual(initial)
      }),
    ))

  test("getAndSet", () =>
    XIO.gen(function* (_) {
      const ref = yield* _(Ref.make(initial))
      const value1 = yield* _(pipe(ref, Ref.getAndSet(update)))
      const value2 = yield* _(Ref.get(ref))
      expect(value1).toEqual(initial)
      expect(value2).toEqual(update)
    }))

  test("getAndUpdate", () =>
    XIO.gen(function* (_) {
      const ref = yield* _(Ref.make(initial))
      const value1 = yield* pipe(
        ref,
        Ref.getAndUpdate(() => update),
        _,
      )
      const value2 = yield* pipe(ref, Ref.get, _)
      expect(value1).toEqual(initial)
      expect(value2).toEqual(update)
    }))

  test("getAndUpdateSome", () =>
    XIO.gen(function* (_) {
      const ref = yield* _(Ref.make(initial))
      const value1 = yield* _(
        pipe(
          ref,
          Ref.getAndUpdateSome(
            flow(
              O.fromPredicate((s) => s === "not value"),
              O.map(() => "nah"),
            ),
          ),
        ),
      )
      const value2 = yield* _(Ref.get(ref))

      expect(value1).toEqual(initial)
      expect(value2).toEqual(initial)
    }))

  test("getAndUpdateSome twice", () =>
    XIO.gen(function* (_) {
      const ref = yield* _(Ref.make(initial))
      const value1 = yield* _(
        pipe(
          ref,
          Ref.getAndUpdateSome(
            flow(
              O.fromPredicate((s) => s === "value"),
              O.map((s) => `updated ${s}`),
            ),
          ),
        ),
      )
      const value2 = yield* _(
        pipe(
          ref,
          Ref.getAndUpdateSome(
            flow(
              O.fromPredicate((s) => s === "updated value"),
              O.map((s) => s + "!"),
            ),
          ),
        ),
      )
      const value3 = yield* _(Ref.get(ref))
      expect(value1).toEqual("value")
      expect(value2).toEqual("updated value")
      expect(value3).toEqual("updated value!")
    }))

  test("modify", () =>
    pipe(
      XIO.Do,
      XIO.bind("ref", () => Ref.make(initial)),
      XIO.bind("r", ({ ref }) =>
        pipe(
          ref,
          Ref.modify((_) => ["hello", update]),
        ),
      ),
      XIO.bind("value", ({ ref }) => Ref.get(ref)),
      XIO.chain(({ r, value }) =>
        XIO.fromIO(() => {
          expect(r).toEqual("hello")
          expect(value).toEqual(update)
        }),
      ),
    ))

  test("modifySome", () =>
    XIO.gen(function* (_) {
      const ref = yield* _(Ref.make(initial))
      const value = yield* _(
        pipe(
          ref,
          Ref.modifySome(
            "no update",
            flow(
              O.fromPredicate((s) => s !== "value"),
              O.map(() => ["not value", update]),
            ),
          ),
        ),
      )
      expect(value).toEqual("no update")
    }))

  test("modifySome twice", () =>
    pipe(
      XIO.Do,
      XIO.bind("ref", () => Ref.make(initial)),
      XIO.bind("value1", ({ ref }) =>
        pipe(
          ref,
          Ref.modifySome(
            "no update",
            flow(
              O.fromPredicate((s) => s === "value"),
              O.map((s) => ["updated", `updated ${s}`]),
            ),
          ),
        ),
      ),
      XIO.bind("value2", ({ ref }) =>
        pipe(
          ref,
          Ref.modifySome(
            "no update",
            flow(
              O.fromPredicate((s) => s === "updated value"),
              O.map((s) => ["again", s]),
            ),
          ),
        ),
      ),
      XIO.chain(({ value1, value2 }) =>
        XIO.fromIO(() => {
          expect(value1).toEqual("updated")
          expect(value2).toEqual("again")
        }),
      ),
    ))

  test("set", () =>
    XIO.gen(function* (_) {
      const ref = yield* _(Ref.make(initial))
      yield* _(Ref.set(update)(ref))
      const value = yield* _(Ref.get(ref))
      expect(value).toEqual(update)
    }))

  test("update", () =>
    XIO.gen(function* (_) {
      const ref = yield* _(Ref.make(initial))
      yield* _(
        pipe(
          ref,
          Ref.update(() => update),
        ),
      )
      const value = yield* _(Ref.get(ref))
      expect(value).toEqual(update)
    }))

  test("updateAndGet", () =>
    XIO.gen(function* (_) {
      const ref = yield* _(Ref.make(initial))
      const value = yield* _(
        pipe(
          ref,
          Ref.updateAndGet(() => update),
        ),
      )
      expect(value).toEqual(update)
    }))

  test("updateSome", () =>
    XIO.gen(function* (_) {
      const ref = yield* _(Ref.make(initial))
      yield* _(
        pipe(
          ref,
          Ref.updateSome(
            flow(
              O.fromPredicate((s) => s === ""),
              O.map(() => "empty"),
            ),
          ),
        ),
      )
      const value = yield* _(Ref.get(ref))
      expect(value).toEqual("value")
    }))

  test("updateSome twice", () =>
    XIO.gen(function* (_) {
      const ref = yield* _(Ref.make(initial))
      yield* _(
        pipe(
          ref,
          Ref.updateSome(
            flow(
              O.fromPredicate((s) => s === "value"),
              O.map((s) => `updated ${s}`),
            ),
          ),
        ),
      )
      const value1 = yield* _(Ref.get(ref))
      yield* _(
        pipe(
          ref,
          Ref.updateSome(
            flow(
              O.fromPredicate((s) => s === "updated value"),
              O.map((s) => s + "!"),
            ),
          ),
        ),
      )
      const value2 = yield* _(Ref.get(ref))
      expect(value1).toEqual("updated value")
      expect(value2).toEqual("updated value!")
    }))
  test("updateSomeAndGet", () =>
    XIO.gen(function* (_) {
      const ref = yield* _(Ref.make(initial))
      const value = yield* _(
        pipe(
          ref,
          Ref.updateSomeAndGet(
            flow(
              O.fromPredicate((s) => s === ""),
              O.map(() => "empty"),
            ),
          ),
        ),
      )
      expect(value).toEqual("value")
    }))

  test("updateSomeAndGet twice", () =>
    XIO.gen(function* (_) {
      const ref = yield* _(Ref.make(initial))
      const value1 = yield* _(
        pipe(
          ref,
          Ref.updateSomeAndGet(
            flow(
              O.fromPredicate((s) => s === "value"),
              O.map((s) => `updated ${s}`),
            ),
          ),
        ),
      )
      const value2 = yield* _(
        pipe(
          ref,
          Ref.updateSomeAndGet(
            flow(
              O.fromPredicate((s) => s === "updated value"),
              O.map((s) => s + "!"),
            ),
          ),
        ),
      )
      expect(value1).toEqual("updated value")
      expect(value2).toEqual("updated value!")
    }))
})
