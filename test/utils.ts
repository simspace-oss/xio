import { pipe } from "fp-ts/function"
import * as O from "fp-ts/Option"

import * as C from "../src/Cause"
import * as Ex from "../src/Exit"
import * as XIO from "../src/XIO"

export function test<E, A>(name: string, test: () => XIO.XIO<unknown, E, A>): void {
  globalThis.test.concurrent(name, async () => {
    const exit = await XIO.unsafeRunPromiseExit(test())
    if (Ex.isFailure(exit)) {
      throw pipe(
        exit.cause,
        C.findMap(O.fromPredicate(C.isHaltType)),
        O.map(({ value }) => value),
        O.getOrElseW(() => new Error("Assertion failed")),
      )
    }
  })
}
