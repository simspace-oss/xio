import * as Equ from "@simspace/collections/Equatable"
import * as V from "@simspace/collections/Vector"
import * as E from "fp-ts/Either"
import { pipe } from "fp-ts/function"
import * as O from "fp-ts/Option"

import * as C from "../src/Cause"
import * as Fi from "../src/Fiber"
import * as F from "../src/Future"
import { assert } from "../src/internal/util"
import * as Q from "../src/Queue"
import * as Ref from "../src/Ref"
import * as XIO from "../src/XIO"

describe("Queue", () => {
  test.concurrent("sequential offer and take", async () => {
    const effect = XIO.gen(function* (_) {
      const queue = yield* _(Q.bounded<number>(100))
      const o1 = yield* _(Q.offer(10)(queue))
      const v1 = yield* _(Q.take(queue))
      const o2 = yield* _(Q.offer(20)(queue))
      const v2 = yield* _(Q.take(queue))
      return { o1, v1, o2, v2 }
    })

    const { o1, v1, o2, v2 } = await XIO.unsafeRunPromise(effect)
    expect(v1).toEqual(10)
    expect(v2).toEqual(20)
    expect(o1).toEqual(true)
    expect(o2).toEqual(true)
  })

  test.concurrent("sequential take and offer", async () => {
    const effect = XIO.gen(function* (_) {
      const queue = yield* _(Q.bounded<string>(100))
      const f1 = yield* _(
        pipe(
          Q.take(queue),
          XIO.zipWith(Q.take(queue), (x, y) => x + y),
          XIO.fork,
        ),
      )
      yield* _(
        pipe(
          queue,
          Q.offer("hello "),
          XIO.chain(() => pipe(queue, Q.offer("world!"))),
        ),
      )
      return yield* _(Fi.join(f1))
    })

    const result = await XIO.unsafeRunPromise(effect)
    expect(result).toEqual("hello world!")
  })

  test.concurrent("concurrent takes and sequential offers", async () => {
    const values = V.range(0, 10)
    const effect = XIO.gen(function* (_) {
      const queue = yield* _(Q.bounded<number>(10))
      const fiber = yield* _(XIO.forkAll(V.replicate(10)(Q.take(queue))))
      yield* _(
        pipe(
          values,
          V.map((n) => Q.offer(n)(queue)),
          V.reduce(XIO.succeed(false), (b, a) =>
            pipe(
              b,
              XIO.chain(() => a),
            ),
          ),
        ),
      )
      return yield* _(Fi.join(fiber))
    })

    const result = await XIO.unsafeRunPromise(effect)
    expect(result).toEqual(values)
  })

  test.concurrent("concurrent offers and sequential takes", async () => {
    const values = V.range(0, 10)
    const effect = XIO.gen(function* (_) {
      const queue = yield* _(Q.bounded<number>(10))
      const fiber = yield* _(
        XIO.forkAll(
          pipe(
            values,
            V.map((n) => Q.offer(n)(queue)),
          ),
        ),
      )
      yield* _(waitForSize(queue, 10))
      const out = yield* _(Ref.make(V.empty<number>()))
      yield* _(
        pipe(
          Q.take(queue),
          XIO.chain((i) => pipe(out, Ref.update(V.append(i)))),
          XIO.repeatN(9),
        ),
      )
      const l = yield* _(Ref.get(out))
      yield* _(Fi.join(fiber))
      return l
    })

    const result = await XIO.unsafeRunPromise(effect)
    expect(result).toEqual(values)
  })

  test.concurrent("offers are suspended by back pressure", async () => {
    const effect = XIO.gen(function* (_) {
      const queue = yield* _(Q.bounded<number>(10))
      yield* _(pipe(queue, Q.offer(1), XIO.repeatN(9)))
      const refSuspended = yield* _(Ref.make(true))
      const f = yield* _(
        pipe(
          queue,
          Q.offer(2),
          XIO.chain(() => pipe(refSuspended, Ref.set(false))),
          XIO.fork,
        ),
      )
      yield* _(waitForSize(queue, 11))
      const isSuspended = yield* _(Ref.get(refSuspended))
      yield* _(Fi.interrupt(f))
      return isSuspended
    })

    const result = await XIO.unsafeRunPromise(effect)
    expect(result).toEqual(true)
  })

  test.concurrent("back pressured offers are retrieved", async () => {
    const values = V.range(0, 10)
    const effect = XIO.gen(function* (_) {
      const queue = yield* _(Q.bounded<number>(5))
      const f = yield* _(
        pipe(
          values,
          V.map((n) => Q.offer(n)(queue)),
          XIO.forkAll,
        ),
      )
      yield* _(waitForSize(queue, 10))
      const out = yield* _(Ref.make(V.empty<number>()))
      yield* _(
        pipe(
          Q.take(queue),
          XIO.chain((i) => pipe(out, Ref.update(V.append(i)))),
          XIO.repeatN(9),
        ),
      )
      const l = yield* _(Ref.get(out))
      yield* _(Fi.join(f))
      return l
    })

    const result = await XIO.unsafeRunPromise(effect)
    expect(result).toEqual(values)
  })

  test.concurrent("take interruption", async () => {
    const effect = XIO.gen(function* (_) {
      const queue = yield* _(Q.bounded<number>(100))
      const fiber = yield* _(pipe(Q.take(queue), XIO.fork))
      yield* _(waitForSize(queue, -1))
      yield* _(Fi.interrupt(fiber))
      return yield* _(Q.size(queue))
    })

    const result = await XIO.unsafeRunPromise(effect)
    expect(result).toEqual(0)
  })

  test.concurrent("offer interruption", async () => {
    const effect = XIO.gen(function* (_) {
      const queue = yield* _(Q.bounded<number>(2))
      yield* _(Q.offer(1)(queue))
      yield* _(Q.offer(1)(queue))
      const fiber = yield* _(XIO.fork(Q.offer(1)(queue)))
      yield* _(waitForSize(queue, 3))
      yield* _(Fi.interrupt(fiber))
      return yield* _(Q.size(queue))
    })

    const result = await XIO.unsafeRunPromise(effect)
    expect(result).toEqual(2)
  })

  test.concurrent("queue is ordered", async () => {
    const effect = XIO.gen(function* (_) {
      const queue = yield* _(Q.unbounded<number>())
      yield* _(Q.offer(1)(queue))
      yield* _(Q.offer(2)(queue))
      yield* _(Q.offer(3)(queue))
      const v1 = yield* _(Q.take(queue))
      const v2 = yield* _(Q.take(queue))
      const v3 = yield* _(Q.take(queue))
      return { v1, v2, v3 }
    })

    const { v1, v2, v3 } = await XIO.unsafeRunPromise(effect)
    expect(v1).toEqual(1)
    expect(v2).toEqual(2)
    expect(v3).toEqual(3)
  })

  test.concurrent("takeAll", async () => {
    const effect = XIO.gen(function* (_) {
      const queue = yield* _(Q.unbounded<number>())
      yield* _(Q.offer(1)(queue))
      yield* _(Q.offer(2)(queue))
      yield* _(Q.offer(3)(queue))
      return yield* _(Q.takeAll(queue))
    })

    const result = await XIO.unsafeRunPromise(effect)
    expect(result).toEqual(V.vector(1, 2, 3))
  })

  test.concurrent("takeAll with empty queue", async () => {
    const effect = XIO.gen(function* (_) {
      const queue = yield* _(Q.unbounded<number>())
      const c = yield* _(Q.takeAll(queue))
      yield* _(Q.offer(1)(queue))
      yield* _(Q.take(queue))
      const v = yield* _(Q.takeAll(queue))
      return { c, v }
    })

    const { c, v } = await XIO.unsafeRunPromise(effect)
    expect(c).toEqual(V.empty())
    expect(v).toEqual(V.empty())
  })

  test.concurrent("takeAll does not return more than the queue size", async () => {
    const values = V.vector(1, 2, 3, 4)
    const effect = XIO.gen(function* (_) {
      const queue = yield* _(Q.bounded<number>(4))
      yield* _(
        pipe(
          values,
          V.map((n) => Q.offer(n)(queue)),
          V.reduce(XIO.succeed(false), (b, a) =>
            pipe(
              b,
              XIO.chain(() => a),
            ),
          ),
        ),
      )
      yield* _(XIO.fork(Q.offer(5)(queue)))
      yield* _(waitForSize(queue, 5))
      const v = yield* _(Q.takeAll(queue))
      const c = yield* _(Q.take(queue))
      return { v, c }
    })

    const { c, v } = await XIO.unsafeRunPromise(effect)
    expect(v).toEqual(values)
    expect(c).toEqual(5)
  })

  test.concurrent("takeUpTo", async () => {
    const effect = XIO.gen(function* (_) {
      const queue = yield* _(Q.bounded<number>(100))
      yield* _(Q.offer(10)(queue))
      yield* _(Q.offer(20)(queue))
      return yield* _(Q.takeUpTo(2)(queue))
    })

    const result = await XIO.unsafeRunPromise(effect)
    expect(result).toEqual(V.vector(10, 20))
  })

  test.concurrent("takeUpTo with empty queue", async () => {
    const effect = XIO.gen(function* (_) {
      const queue = yield* _(Q.bounded<number>(100))
      return yield* _(Q.takeUpTo(2)(queue))
    })

    const result = await XIO.unsafeRunPromise(effect)
    expect(result.length).toEqual(0)
  })

  test.concurrent("takeUpTo with empty queue, with max higher than queue size", async () => {
    const effect = XIO.gen(function* (_) {
      const queue = yield* _(Q.bounded<number>(100))
      return yield* _(Q.takeUpTo(101)(queue))
    })

    const result = await XIO.unsafeRunPromise(effect)
    expect(result.length).toEqual(0)
  })

  test.concurrent("takeUpTo with remaining items", async () => {
    const effect = XIO.gen(function* (_) {
      const queue = yield* _(Q.bounded<number>(100))
      yield* _(Q.offer(10)(queue))
      yield* _(Q.offer(20)(queue))
      yield* _(Q.offer(30)(queue))
      yield* _(Q.offer(40)(queue))
      return yield* _(Q.takeUpTo(2)(queue))
    })

    const result = await XIO.unsafeRunPromise(effect)
    expect(result).toEqual(V.vector(10, 20))
  })

  test.concurrent("takeUpTo with not enough items", async () => {
    const effect = XIO.gen(function* (_) {
      const queue = yield* _(Q.bounded<number>(100))
      yield* _(Q.offer(10)(queue))
      yield* _(Q.offer(20)(queue))
      yield* _(Q.offer(30)(queue))
      yield* _(Q.offer(40)(queue))
      return yield* _(Q.takeUpTo(10)(queue))
    })

    const result = await XIO.unsafeRunPromise(effect)
    expect(result).toEqual(V.vector(10, 20, 30, 40))
  })

  test.concurrent("takeUpTo(0)", async () => {
    const effect = XIO.gen(function* (_) {
      const queue = yield* _(Q.bounded<number>(100))
      yield* _(Q.offer(10)(queue))
      yield* _(Q.offer(20)(queue))
      yield* _(Q.offer(30)(queue))
      yield* _(Q.offer(40)(queue))
      return yield* _(Q.takeUpTo(0)(queue))
    })

    const result = await XIO.unsafeRunPromise(effect)
    expect(result.length).toEqual(0)
  })

  test.concurrent("takeUpTo(-1)", async () => {
    const effect = XIO.gen(function* (_) {
      const queue = yield* _(Q.bounded<number>(100))
      yield* _(Q.offer(10)(queue))
      return yield* _(Q.takeUpTo(-1)(queue))
    })

    const result = await XIO.unsafeRunPromise(effect)
    expect(result.length).toEqual(0)
  })

  test.concurrent("takeUpTo(MAX_SAFE_INTEGER)", async () => {
    const effect = XIO.gen(function* (_) {
      const queue = yield* _(Q.bounded<number>(100))
      yield* _(Q.offer(10)(queue))
      return yield* _(Q.takeUpTo(Number.MAX_SAFE_INTEGER)(queue))
    })

    const result = await XIO.unsafeRunPromise(effect)
    expect(result).toEqual(V.vector(10))
  })

  test.concurrent("multiple takeUpTo", async () => {
    const effect = XIO.gen(function* (_) {
      const queue = yield* _(Q.bounded<number>(100))
      yield* _(Q.offer(10)(queue))
      yield* _(Q.offer(20)(queue))
      const vec1 = yield* _(Q.takeUpTo(2)(queue))
      yield* _(Q.offer(30)(queue))
      yield* _(Q.offer(40)(queue))
      const vec2 = yield* _(Q.takeUpTo(2)(queue))
      return { vec1, vec2 }
    })

    const { vec1, vec2 } = await XIO.unsafeRunPromise(effect)
    expect(vec1).toEqual(V.vector(10, 20))
    expect(vec2).toEqual(V.vector(30, 40))
  })

  test.concurrent("consecutive takeUpTo", async () => {
    const effect = XIO.gen(function* (_) {
      const queue = yield* _(Q.bounded<number>(100))
      yield* _(Q.offer(10)(queue))
      yield* _(Q.offer(20)(queue))
      yield* _(Q.offer(30)(queue))
      yield* _(Q.offer(40)(queue))
      const vec1 = yield* _(Q.takeUpTo(2)(queue))
      const vec2 = yield* _(Q.takeUpTo(2)(queue))
      return { vec1, vec2 }
    })

    const { vec1, vec2 } = await XIO.unsafeRunPromise(effect)
    expect(vec1).toEqual(V.vector(10, 20))
    expect(vec2).toEqual(V.vector(30, 40))
  })

  test.concurrent("takeUpTo does not return back-pressured offers", async () => {
    const values = V.vector(1, 2, 3, 4)
    const effect = XIO.gen(function* (_) {
      const queue = yield* _(Q.bounded<number>(4))
      yield* _(
        pipe(
          values,
          V.map((n) => Q.offer(n)(queue)),
          V.reduce(XIO.succeed(false), (b, a) =>
            pipe(
              b,
              XIO.chain(() => a),
            ),
          ),
        ),
      )
      const fiber = yield* _(XIO.fork(Q.offer(5)(queue)))
      yield* _(waitForSize(queue, 5))
      const c = yield* _(Q.takeUpTo(5)(queue))
      yield* _(Fi.interrupt(fiber))
      return c
    })

    const result = await XIO.unsafeRunPromise(effect)
    expect(result).toEqual(V.vector(1, 2, 3, 4))
  })

  test.concurrent("offerAll with takeAll", async () => {
    const orders = V.range(0, 10)
    const effect = XIO.gen(function* (_) {
      const queue = yield* _(Q.bounded<number>(10))
      yield* _(Q.offerAll(orders)(queue))
      yield* _(waitForSize(queue, 10))
      return yield* _(Q.takeAll(queue))
    })

    const result = await XIO.unsafeRunPromise(effect)
    expect(result).toEqual(orders)
  })

  test.concurrent("offerAll with takeAll and back-pressure", async () => {
    const orders = V.range(0, 3)
    const effect = XIO.gen(function* (_) {
      const queue = yield* _(Q.bounded<number>(2))
      const fiber = yield* _(XIO.fork(Q.offerAll(orders)(queue)))
      const size = yield* _(waitForSize(queue, 3))
      const c = yield* _(Q.takeAll(queue))
      yield* _(Fi.interrupt(fiber))
      return { c, size }
    })

    const { c, size } = await XIO.unsafeRunPromise(effect)
    expect(size).toEqual(3)
    expect(c).toEqual(V.vector(0, 1))
  })

  test.concurrent("offerAll with takeAll and back-pressure + interruption", async () => {
    const orders1 = V.range(0, 2)
    const orders2 = V.range(2, 4)
    const effect = XIO.gen(function* (_) {
      const queue = yield* _(Q.bounded<number>(2))
      yield* _(Q.offerAll(orders1)(queue))
      const fiber = yield* _(XIO.fork(Q.offerAll(orders2)(queue)))
      yield* _(waitForSize(queue, 4))
      yield* _(Fi.interrupt(fiber))
      const l1 = yield* _(Q.takeAll(queue))
      const l2 = yield* _(Q.takeAll(queue))
      return { l1, l2 }
    })

    const { l1, l2 } = await XIO.unsafeRunPromise(effect)
    expect(l1).toEqual(orders1)
    expect(l2).toEqual(V.empty())
  })

  test.concurrent("offerAll with takeAll and back-pressure, check ordering", async () => {
    const effect = XIO.gen(function* (_) {
      const queue = yield* _(Q.bounded<number>(64))
      const fiber = yield* _(XIO.fork(Q.offerAll(V.range(0, 128))(queue)))
      yield* _(waitForSize(queue, 128))
      const l = yield* _(Q.takeAll(queue))
      yield* _(Fi.interrupt(fiber))
      return l
    })

    const result = await XIO.unsafeRunPromise(effect)
    expect(result).toEqual(V.range(0, 64))
  })

  test.concurrent("offerAll with pending takers", async () => {
    const orders = V.range(0, 100)
    const effect = XIO.gen(function* (_) {
      const queue = yield* _(Q.bounded<number>(50))
      const takers = yield* _(XIO.forkAll(V.replicate(100)(Q.take(queue))))
      yield* _(waitForSize(queue, -100))
      yield* _(Q.offerAll(orders)(queue))
      const l = yield* _(Fi.join(takers))
      const s = yield* _(Q.size(queue))
      return { l, s }
    })

    const { l, s } = await XIO.unsafeRunPromise(effect)
    expect(l).toEqual(orders)
    expect(s).toEqual(0)
  })

  test.concurrent("offerAll with pending takers, check ordering", async () => {
    const orders = V.range(0, 128)
    const effect = XIO.gen(function* (_) {
      const queue = yield* _(Q.bounded<number>(256))
      const takers = yield* _(XIO.forkAll(V.replicate(64)(Q.take(queue))))
      yield* _(waitForSize(queue, -64))
      yield* _(Q.offerAll(orders)(queue))
      const l = yield* _(Fi.join(takers))
      const s = yield* _(Q.size(queue))
      return { l, s }
    })

    const { l, s } = await XIO.unsafeRunPromise(effect)
    expect(l).toEqual(pipe(orders, V.take(64)))
    expect(s).toEqual(64)
  })

  test.concurrent("offerAll with pending takers, check ordering of taker resolution", async () => {
    const values = V.range(0, 100)
    const effect = XIO.gen(function* (_) {
      const queue = yield* _(Q.bounded<number>(200))
      const takers = yield* _(XIO.forkAll(V.replicate(100)(Q.take(queue))))
      yield* _(waitForSize(queue, -100))
      const fiber = yield* _(XIO.forkAll(V.replicate(100)(Q.take(queue))))
      yield* _(waitForSize(queue, -200))
      yield* _(Q.offerAll(values)(queue))
      const l = yield* _(Fi.join(takers))
      const s = yield* _(Q.size(queue))
      yield* _(Fi.interrupt(fiber))
      return { l, s }
    })

    const { l, s } = await XIO.unsafeRunPromise(effect)
    expect(l).toEqual(values)
    expect(s).toEqual(-100)
  })

  test.concurrent("offerAll with take and back-pressure", async () => {
    const values = V.range(0, 3)
    const effect = XIO.gen(function* (_) {
      const queue = yield* _(Q.bounded<number>(2))
      yield* _(XIO.fork(Q.offerAll(values)(queue)))
      yield* _(waitForSize(queue, 3))
      const v1 = yield* _(Q.take(queue))
      const v2 = yield* _(Q.take(queue))
      const v3 = yield* _(Q.take(queue))
      return { v1, v2, v3 }
    })

    const { v1, v2, v3 } = await XIO.unsafeRunPromise(effect)
    expect(v1).toEqual(0)
    expect(v2).toEqual(1)
    expect(v3).toEqual(2)
  })

  test.concurrent("multiple offerAll with back-pressure", async () => {
    const values = V.range(0, 3)
    const values2 = V.range(3, 5)
    const effect = XIO.gen(function* (_) {
      const queue = yield* _(Q.bounded<number>(2))
      yield* _(XIO.fork(Q.offerAll(values)(queue)))
      yield* _(waitForSize(queue, 3))
      yield* _(XIO.fork(Q.offerAll(values2)(queue)))
      yield* _(waitForSize(queue, 5))
      const v1 = yield* _(Q.take(queue))
      const v2 = yield* _(Q.take(queue))
      const v3 = yield* _(Q.take(queue))
      const v4 = yield* _(Q.take(queue))
      const v5 = yield* _(Q.take(queue))
      return { v1, v2, v3, v4, v5 }
    })

    const { v1, v2, v3, v4, v5 } = await XIO.unsafeRunPromise(effect)
    expect(v1).toEqual(0)
    expect(v2).toEqual(1)
    expect(v3).toEqual(2)
    expect(v4).toEqual(3)
    expect(v5).toEqual(4)
  })

  test.concurrent("combination of offer, offerAll, take, takeAll", async () => {
    const values = V.range(3, 36)
    const effect = XIO.gen(function* (_) {
      const queue = yield* _(Q.bounded<number>(32))
      yield* _(Q.offer(1)(queue))
      yield* _(Q.offer(2)(queue))
      yield* _(XIO.fork(Q.offerAll(values)(queue)))
      yield* _(waitForSize(queue, 35))
      const v = yield* _(Q.takeAll(queue))
      const v1 = yield* _(Q.take(queue))
      const v2 = yield* _(Q.take(queue))
      const v3 = yield* _(Q.take(queue))
      return { v, v1, v2, v3 }
    })

    const { v, v1, v2, v3 } = await XIO.unsafeRunPromise(effect)
    expect(v).toEqual(V.range(1, 33))
    expect(v1).toEqual(33)
    expect(v2).toEqual(34)
    expect(v3).toEqual(35)
  })

  test.concurrent("shutdown with take fiber", async () => {
    const effect = XIO.gen(function* (_) {
      const selfId = yield* _(XIO.fiberId)
      const queue = yield* _(Q.bounded<number>(3))
      const f = yield* _(XIO.fork(Q.take(queue)))
      yield* _(waitForSize(queue, -1))
      yield* _(Q.shutdown(queue))
      const result = yield* _(pipe(f, Fi.join, XIO.sandbox, XIO.either))
      return { selfId, result }
    })

    const { selfId, result } = await XIO.unsafeRunPromise(effect)
    assert(E.isLeft(result))
    expect(Equ.strictEqualsUnknown(result.left, C.interrupt(selfId))).toEqual(true)
  })

  test.concurrent("shutdown with offer fiber", async () => {
    const effect = XIO.gen(function* (_) {
      const selfId = yield* _(XIO.fiberId)
      const queue = yield* _(Q.bounded<number>(2))
      const f = yield* _(XIO.fork(Q.take(queue)))
      yield* _(waitForSize(queue, -1))
      yield* _(Q.shutdown(queue))
      const result = yield* _(pipe(f, Fi.join, XIO.sandbox, XIO.either))
      return { selfId, result }
    })

    const { selfId, result } = await XIO.unsafeRunPromise(effect)
    assert(E.isLeft(result))
    expect(Equ.strictEqualsUnknown(result.left, C.interrupt(selfId))).toEqual(true)
  })

  test.concurrent("shutdown with offer", async () => {
    const effect = XIO.gen(function* (_) {
      const selfId = yield* _(XIO.fiberId)
      const queue = yield* _(Q.bounded<number>(1))
      yield* _(Q.shutdown(queue))
      const result = yield* _(pipe(queue, Q.offer(1), XIO.sandbox, XIO.either))
      return { selfId, result }
    })

    const { selfId, result } = await XIO.unsafeRunPromise(effect)
    assert(E.isLeft(result))
    expect(Equ.strictEqualsUnknown(result.left, C.interrupt(selfId))).toEqual(true)
  })

  test.concurrent("shutdown with take", async () => {
    const effect = XIO.gen(function* (_) {
      const selfId = yield* _(XIO.fiberId)
      const queue = yield* _(Q.bounded<number>(1))
      yield* _(Q.shutdown(queue))
      const result = yield* _(pipe(queue, Q.take, XIO.sandbox, XIO.either))
      return { selfId, result }
    })

    const { selfId, result } = await XIO.unsafeRunPromise(effect)
    assert(E.isLeft(result))
    expect(Equ.strictEqualsUnknown(result.left, C.interrupt(selfId))).toEqual(true)
  })

  test.concurrent("shutdown with takeAll", async () => {
    const effect = XIO.gen(function* (_) {
      const selfId = yield* _(XIO.fiberId)
      const queue = yield* _(Q.bounded<number>(1))
      yield* _(Q.shutdown(queue))
      const result = yield* _(pipe(queue, Q.takeAll, XIO.sandbox, XIO.either))
      return { selfId, result }
    })

    const { selfId, result } = await XIO.unsafeRunPromise(effect)
    assert(E.isLeft(result))
    expect(Equ.strictEqualsUnknown(result.left, C.interrupt(selfId))).toEqual(true)
  })

  test.concurrent("shutdown with takeUpTo", async () => {
    const effect = XIO.gen(function* (_) {
      const selfId = yield* _(XIO.fiberId)
      const queue = yield* _(Q.bounded<number>(1))
      yield* _(Q.shutdown(queue))
      const result = yield* _(pipe(queue, Q.takeUpTo(1), XIO.sandbox, XIO.either))
      return { selfId, result }
    })

    const { selfId, result } = await XIO.unsafeRunPromise(effect)
    assert(E.isLeft(result))
    expect(Equ.strictEqualsUnknown(result.left, C.interrupt(selfId))).toEqual(true)
  })

  test.concurrent("shutdown with size", async () => {
    const effect = XIO.gen(function* (_) {
      const selfId = yield* _(XIO.fiberId)
      const queue = yield* _(Q.bounded<number>(1))
      yield* _(Q.shutdown(queue))
      const result = yield* _(pipe(queue, Q.size, XIO.sandbox, XIO.either))
      return { selfId, result }
    })

    const { selfId, result } = await XIO.unsafeRunPromise(effect)
    assert(E.isLeft(result))
    expect(Equ.strictEqualsUnknown(result.left, C.interrupt(selfId))).toEqual(true)
  })

  test.concurrent("back-pressured offer completes after take", async () => {
    const effect = XIO.gen(function* (_) {
      const queue = yield* _(Q.bounded<number>(2))
      yield* _(Q.offerAll(V.vector(1, 2))(queue))
      const f = yield* _(XIO.fork(Q.offer(3)(queue)))
      yield* _(waitForSize(queue, 3))
      const v1 = yield* _(Q.take(queue))
      const v2 = yield* _(Q.take(queue))
      yield* _(Fi.join(f))
      return { v1, v2 }
    })

    const { v1, v2 } = await XIO.unsafeRunPromise(effect)
    expect(v1).toEqual(1)
    expect(v2).toEqual(2)
  })

  test.concurrent("back-pressured offer completes after takeAll", async () => {
    const effect = XIO.gen(function* (_) {
      const queue = yield* _(Q.bounded<number>(2))
      yield* _(Q.offerAll(V.vector(1, 2))(queue))
      const f = yield* _(XIO.fork(Q.offer(3)(queue)))
      yield* _(waitForSize(queue, 3))
      const v1 = yield* _(Q.takeAll(queue))
      yield* _(Fi.join(f))
      return v1
    })

    const result = await XIO.unsafeRunPromise(effect)
    expect(result).toEqual(V.vector(1, 2))
  })

  test.concurrent("back-pressured offer completes after takeUpTo", async () => {
    const effect = XIO.gen(function* (_) {
      const queue = yield* _(Q.bounded<number>(2))
      yield* _(Q.offerAll(V.vector(1, 2))(queue))
      const f = yield* _(XIO.fork(Q.offer(3)(queue)))
      yield* _(waitForSize(queue, 3))
      const v1 = yield* _(Q.takeUpTo(2)(queue))
      yield* _(Fi.join(f))
      return v1
    })

    const result = await XIO.unsafeRunPromise(effect)
    expect(result).toEqual(V.vector(1, 2))
  })

  test.concurrent("back-pressured offerAll completes after takeAll", async () => {
    const effect = XIO.gen(function* (_) {
      const queue = yield* _(Q.bounded<number>(2))
      yield* _(Q.offerAll(V.vector(1, 2))(queue))
      const f = yield* _(pipe(queue, Q.offerAll(V.vector(3, 4, 5)), XIO.fork))
      yield* _(waitForSize(queue, 5))
      const v1 = yield* _(Q.takeAll(queue))
      const v2 = yield* _(Q.takeAll(queue))
      const v3 = yield* _(Q.takeAll(queue))
      yield* _(Fi.join(f))
      return { v1, v2, v3 }
    })

    const { v1, v2, v3 } = await XIO.unsafeRunPromise(effect)
    expect(v1).toEqual(V.vector(1, 2))
    expect(v2).toEqual(V.vector(3, 4))
    expect(v3).toEqual(V.vector(5))
  })

  test.concurrent("sliding strategy with offer", async () => {
    const effect = pipe(
      XIO.Do,
      XIO.bind("queue", () => Q.sliding<number>(2)),
      XIO.chainFirst(({ queue }) => Q.offer(1)(queue)),
      XIO.bind("v1", ({ queue }) => Q.offer(2)(queue)),
      XIO.bind("v2", ({ queue }) => Q.offer(3)(queue)),
      XIO.bind("l", ({ queue }) => Q.takeAll(queue)),
      XIO.map(({ v1, v2, l }) => ({ v1, v2, l })),
    )

    const { v1, v2, l } = await XIO.unsafeRunPromise(effect)
    expect(v1).toEqual(true)
    expect(v2).toEqual(true)
    expect(l).toEqual(V.vector(2, 3))
  })

  test.concurrent("sliding strategy with offerAll", async () => {
    const effect = XIO.gen(function* (_) {
      const queue = yield* _(Q.sliding<number>(2))
      const v = yield* _(Q.offerAll(V.vector(1, 2, 3))(queue))
      const size = yield* _(Q.size(queue))
      return { v, size }
    })

    const { v, size } = await XIO.unsafeRunPromise(effect)
    expect(v).toEqual(true)
    expect(size).toEqual(2)
  })

  test.concurrent("sliding strategy with enough capacity", async () => {
    const effect = pipe(
      XIO.Do,
      XIO.bind("queue", () => Q.sliding<number>(100)),
      XIO.chainFirst(({ queue }) => Q.offer(1)(queue)),
      XIO.chainFirst(({ queue }) => Q.offer(2)(queue)),
      XIO.chainFirst(({ queue }) => Q.offer(3)(queue)),
      XIO.chain(({ queue }) => Q.takeAll(queue)),
    )

    const result = await XIO.unsafeRunPromise(effect)
    expect(result).toEqual(V.vector(1, 2, 3))
  })

  test.concurrent("sliding strategy with offerAll and takeAll", async () => {
    const effect = XIO.gen(function* (_) {
      const queue = yield* _(Q.sliding<number>(2))
      yield* _(Q.offerAll(V.vector(1, 2, 3, 4, 5, 6))(queue))
      return yield* _(Q.takeAll(queue))
    })

    const result = await XIO.unsafeRunPromise(effect)
    expect(result).toEqual(V.vector(5, 6))
  })

  test.concurrent("awaitShutdown", async () => {
    const effect = pipe(
      XIO.Do,
      XIO.bind("queue", () => Q.bounded<number>(3)),
      XIO.bind("f", () => F.make<never, boolean>()),
      XIO.chainFirst(({ queue, f }) =>
        pipe(
          queue,
          Q.awaitShutdown,
          XIO.chain(() => pipe(f, F.succeed(true))),
          XIO.fork,
        ),
      ),
      XIO.chainFirst(({ queue }) => Q.shutdown(queue)),
      XIO.chain(({ f }) => F.await(f)),
    )

    const result = await XIO.unsafeRunPromise(effect)
    expect(result).toEqual(true)
  })

  test.concurrent("multiple awaitShutdown", async () => {
    const effect = XIO.gen(function* (_) {
      const queue = yield* _(Q.bounded<number>(3))
      const f1 = yield* _(F.make<never, boolean>())
      const f2 = yield* _(F.make<never, boolean>())
      yield* _(
        pipe(
          queue,
          Q.awaitShutdown,
          XIO.chain(() => pipe(f1, F.succeed(true))),
          XIO.fork,
        ),
      )
      yield* _(
        pipe(
          queue,
          Q.awaitShutdown,
          XIO.chain(() => pipe(f2, F.succeed(true))),
          XIO.fork,
        ),
      )
      yield* _(Q.shutdown(queue))
      const res1 = yield* _(F.await(f1))
      const res2 = yield* _(F.await(f2))
      return { res1, res2 }
    })

    const { res1, res2 } = await XIO.unsafeRunPromise(effect)
    expect(res1).toEqual(true)
    expect(res2).toEqual(true)
  })

  test.concurrent("awaitShutdown when queue is already shutdown", async () => {
    const effect = pipe(
      XIO.Do,
      XIO.bind("queue", () => Q.bounded<number>(3)),
      XIO.chainFirst(({ queue }) => Q.shutdown(queue)),
      XIO.bind("f", () => F.make<never, boolean>()),
      XIO.chainFirst(({ queue, f }) =>
        pipe(
          queue,
          Q.awaitShutdown,
          XIO.chain(() => pipe(f, F.succeed(true))),
          XIO.fork,
        ),
      ),
      XIO.chain(({ f }) => F.await(f)),
    )

    const res = await XIO.unsafeRunPromise(effect)
    expect(res).toEqual(true)
  })

  test.concurrent("dropping strategy with offerAll", async () => {
    const values = V.range(1, 6)
    const effect = XIO.gen(function* (_) {
      const queue = yield* _(Q.dropping<number>(4))
      const v1 = yield* _(Q.offerAll(values)(queue))
      const l = yield* _(Q.takeAll(queue))
      return { v1, l }
    })

    const { v1, l } = await XIO.unsafeRunPromise(effect)
    expect(v1).toEqual(false)
    expect(l).toEqual(V.vector(1, 2, 3, 4))
  })

  test.concurrent("dropping strategy with pending taker", async () => {
    const values = V.range(1, 5)
    const effect = pipe(
      XIO.Do,
      XIO.bind("queue", () => Q.dropping<number>(2)),
      XIO.bind("f", ({ queue }) => XIO.fork(Q.take(queue))),
      XIO.chainFirst(({ queue }) => waitForSize(queue, -1)),
      XIO.bind("oa", ({ queue }) => Q.offerAll(values)(queue)),
      XIO.bind("j", ({ f }) => Fi.join(f)),
      XIO.map(({ j, oa }) => ({ j, oa })),
    )

    const { j, oa } = await XIO.unsafeRunPromise(effect)
    expect(j).toEqual(1)
    expect(oa).toEqual(false)
  })

  test.concurrent("sliding strategy with pending taker", async () => {
    const values = V.range(1, 5)
    const effect = pipe(
      XIO.Do,
      XIO.bind("queue", () => Q.sliding<number>(2)),
      XIO.bind("f", ({ queue }) => XIO.fork(Q.take(queue))),
      XIO.chainFirst(({ queue }) => waitForSize(queue, -1)),
      XIO.bind("oa", ({ queue }) => Q.offerAll(values)(queue)),
      XIO.bind("t", ({ queue }) => Q.take(queue)),
      XIO.map(({ t, oa }) => ({ t, oa })),
    )

    const { t, oa } = await XIO.unsafeRunPromise(effect)
    expect(t).toEqual(3)
    expect(oa).toEqual(true)
  })

  test.concurrent("poll on empty queue", async () => {
    const effect = pipe(Q.bounded<number>(5), XIO.chain(Q.poll))

    const result = await XIO.unsafeRunPromise(effect)
    expect(result).toEqual(O.none)
  })

  test.concurrent("poll on queue just emptied", async () => {
    const effect = XIO.gen(function* (_) {
      const queue = yield* _(Q.bounded<number>(5))
      yield* _(Q.offerAll(V.range(1, 5))(queue))
      yield* _(Q.takeAll(queue))
      return yield* _(Q.poll(queue))
    })

    const result = await XIO.unsafeRunPromise(effect)
    expect(result).toEqual(O.none)
  })

  test.concurrent("multiple polls", async () => {
    const effect = XIO.gen(function* (_) {
      const queue = yield* _(Q.bounded<number>(5))
      yield* _(Q.offerAll(V.range(1, 3))(queue))
      const t1 = yield* _(Q.poll(queue))
      const t2 = yield* _(Q.poll(queue))
      const t3 = yield* _(Q.poll(queue))
      const t4 = yield* _(Q.poll(queue))
      return { t1, t2, t3, t4 }
    })

    const { t1, t2, t3, t4 } = await XIO.unsafeRunPromise(effect)
    expect(t1).toEqual(O.some(1))
    expect(t2).toEqual(O.some(2))
    expect(t3).toEqual(O.none)
    expect(t4).toEqual(O.none)
  })

  test.concurrent("shutdown race condition with offer", async () => {
    const effect = pipe(
      XIO.Do,
      XIO.bind("queue", () => Q.bounded<number>(2)),
      XIO.bind("f", ({ queue }) => pipe(queue, Q.offer(1), XIO.forever, XIO.fork)),
      XIO.chainFirst(({ queue }) => Q.shutdown(queue)),
      XIO.chain(({ f }) => Fi.await(f)),
    )
    await XIO.unsafeRunPromise(effect)
  })

  test.concurrent("shutdown race condition with take", async () => {
    const effect = pipe(
      XIO.Do,
      XIO.bind("queue", () => Q.bounded<number>(2)),
      XIO.chainFirst(({ queue }) => Q.offer(1)(queue)),
      XIO.chainFirst(({ queue }) => Q.offer(1)(queue)),
      XIO.bind("f", ({ queue }) => pipe(queue, Q.take, XIO.forever, XIO.fork)),
      XIO.chainFirst(({ queue }) => Q.shutdown(queue)),
      XIO.chain(({ f }) => Fi.await(f)),
    )
    await XIO.unsafeRunPromise(effect)
  })
})

function waitForValue<T>(ref: XIO.UIO<T>, value: T): XIO.UIO<T> {
  return pipe(
    ref,
    XIO.chainFirst(() => XIO.sleep(10)),
    XIO.repeatUntil((_) => _ === value),
  )
}

function waitForSize<A>(queue: Q.Queue<A>, size: number): XIO.UIO<number> {
  return waitForValue(Q.size(queue), size)
}
